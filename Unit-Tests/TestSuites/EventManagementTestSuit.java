/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class EventManagementTestSuit extends BaseClass
{

    static TestMarshall instance;

    public EventManagementTestSuit()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //S4_IncidentManagement_Beta
    @Test
    public void EventManagement() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\Event Management regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void EventManagement_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\Event Management regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1-Capture Incident - Main Scenario
    @Test
    public void FR1_Capture_An_Event_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario5() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_AlternateScenario6() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Alternate scenario 6.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_OptionalScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Optional scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_An_Event_OptionalScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR1-Capture An Event - Optional scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_02_Capture_A_Grievance_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-02 Capture a grievance Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_02_Capture_A_Grievance_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-02 Capture a grievance Optional  Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_02_Capture_A_Grievance_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-02 Capture a grievance Alternate  Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_03_Capture_a_High_Potential_Hazard_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-03 Capture a high potential Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_03_Capture_a_High_Potential_Hazard_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-03 Capture a high potential Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_03_Capture_a_High_Potential_Hazard_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-03 Capture a high potential Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_03_Capture_a_High_Potential_Hazard_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-03 Capture a high potential Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_03_Capture_a_High_Potential_Hazard_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-03 Capture a high potential Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_04_CaptureLowPotentialHazard_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-04 Capture a low potential hazard  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_04_CaptureLowPotentialHazard_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_04_CaptureLowPotentialHazard_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_05_Close_Low_Potential_Hazard_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-05 Close low potential hazard - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_05_Close_Low_Potential_Hazard_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_01_05_Close_Low_Potential_Hazard_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 01-05 Close low potential hazard - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR2-Capture Non-Compliance Intervention - Main Scenario

    @Test
    public void FR2_Capture_NonComplianceIntervention_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR2-Capture Non-Compliance Intervention - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_Capture_Agency_And_Mechanism_Details_For_The_Event_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR3-Capture Agency and Mechanism details for the event  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_03_02_Capturing_Agency_and_Mechanism_Fields_For_a_Grievance_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 03-02 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_03_02_Capturing_Agency_and_Mechanism_Fields_For_a_Grievance_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 03-02 - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_03_02_Capturing_Agency_and_Mechanism_Fields_For_a_Grievance_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 03-02 - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_03_02_Capturing_Agency_and_Mechanism_Fields_For_a_Grievance_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 03-02 - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_Capture_Event_Actions_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR4-Capture Event Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5 Capture Event Consequence Safety Injury Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR5-Capture Event Consequence  Safety Injury - Alternate scenario 1

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - Alternate scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - Alternate scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - Alternate scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - Alternate scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Alternate_Scenario5() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - Alternate scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Event_Consequence_Safety_Injury_Optional_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR5-Capture Event Consequence  Safety Injury - optional scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_05_02_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 05-02 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario5() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Alternate_Scenario6() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Alternate Scenario 6.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Event_Consequence_Environmental_Impact_Optional_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR6- Capture Event Consequence – Environmental Impact - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_02_Capture_Files_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06 02 - Capture Files Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_02_Capture_Files_Alternater_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06 02 - Capture Files Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_02_Capture_Files_Alternater_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06 02 - Capture Files Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_02_Capture_Files_Alternater_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06 02 - Capture Files Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_03_Capture_Classification_Verification_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06-03 Capture E-Classification Verification  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_06_03_Capture_Classification_Verification_Optional_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 06-03 Capture E-Classification Verification  Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Event_Consequence_Legal_Regulatory_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR7- Capture Event Consequence – Legal and Regulatory  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Event_Consequence_Legal_Regulatory_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR7- Capture Event Consequence – Legal and Regulatory  - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Event_Consequence_Legal_Regulatory_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR7- Capture Event Consequence – Legal and Regulatory  - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Event_Consequence_Legal_Regulatory_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR7- Capture Event Consequence – Legal and Regulatory  - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Event_Consequence_Legal_Regulatory_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR7- Capture Event Consequence – Legal and Regulatory  - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //UC EVE 07-02 Capturing Legal and Regulatory Fines- Main Scenario

    @Test

    public void UC_EVE_07_02_Capturing_Legal_and_Regulatory_Fines_Main_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 07-02 Capturing Legal and Regulatory Fines- Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_07_02_Capturing_Legal_and_Regulatory_Fines_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 07-02 Capturing Legal and Regulatory Fines- Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_07_02_Capturing_Legal_and_Regulatory_Fines_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 07-02 Capturing Legal and Regulatory Fines- Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test

    public void UC_EVE_07_02_Capturing_Legal_and_Regulatory_Fines_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 07-02 Capturing Legal and Regulatory Fines- Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Event_Consequence_Material_Alternate_Scenario5() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR8 Capture Event Consequence Material - Alternate Scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Event_Consequence_Occupationa_Health_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR9- Capture Event Consequence – Occupational Health - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Event_Consequence_Occupationa_Health_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR9- Capture Event Consequence Occupational Health - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Event_Consequence_Occupationa_Health_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR9- Capture Event Consequence Occupational Health - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Event_Consequence_Occupationa_Health_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR9- Capture Event Consequence Occupational Health - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Event_Consequence_Occupationa_Health_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR9- Capture Event Consequence Occupational Health - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_09_02_Capture_Event_Consequence_Occupationa_Health_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 09 - Capture Event Consequence – Occupational Health - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_09_02_Capture_Event_Consequence_Occupationa_Health_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 09 - Capture Event Consequence – Occupational Health - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Capture_Event_Consequence_Reputation_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR10- Capture Event Consequence – Reputation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Capture_Event_Consequence_Reputation_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR10- Capture Event Consequence – Reputation - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Capture_Event_Consequence_Reputation_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR10- Capture Event Consequence – Reputation - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Capture_Event_Consequence_Reputation_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR10- Capture Event Consequence – Reputation - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Capture_Event_Consequence_Reputation_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR10- Capture Event Consequence – Reputation - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Event_Consequence_Social_Communities_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR11- Capture Event Consequence – Social Communities - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Event_Consequence_Social_Communities_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR11- Capture Event Consequence – Social Communities - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Event_Consequence_Social_Communities_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR11- Capture Event Consequence – Social Communities - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Event_Consequence_Social_Communities_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR11- Capture Event Consequence – Social Communities - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Event_Consequence_Social_Communities_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR11- Capture Event Consequence – Social Communities - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Capture_Event_Consequence_Workplace_Exposure_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR12- Capture Event Consequence – Workplace Exposure  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Capture_Event_Consequence_Workplace_Exposure_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR12- Capture Event Consequence – Workplace Exposure  - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Capture_Event_Consequence_Workplace_Exposure_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR12- Capture Event Consequence – Workplace Exposure  - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Capture_Event_Consequence_Workplace_Exposure_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR12- Capture Event Consequence – Workplace Exposure  - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Capture_Event_Consequence_Workplace_Exposure_Alternate_Scenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR12- Capture Event Consequence – Workplace Exposure  - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR13_Declare_Event_Ready_for_Investigation_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR13- Declare Event Ready for Investigation  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR13_Declare_Event_Ready_for_Investigation_Alternate_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR13- Declare Event Ready for Investigation  - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR14_Capture_Investigation_Team_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR14-Capture Investigation Team  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR14_Capture_Investigation_Team_Alternate_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR14-Capture Investigation Team  - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR15_Capture_Evidence_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR15-Capture Evidence   - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR15_Capture_Evidence_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR15-Capture Evidence   - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR15_Capture_Evidence_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR15-Capture Evidence   - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR15_Capture_Evidence_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR15-Capture Evidence   - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR16_Capture_Investigation_Detail_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR16-Capture Investigation Detail  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR16_Capture_Investigation_Detail_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR16-Capture Investigation Detail  - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR16_Capture_Investigation_Detail_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR16-Capture Investigation Detail  - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR18_Capture_Why_Analysis_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR18-Capture Why Analysis - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_EVE_18_02_Capture_Actions_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC EVE 18-02 Capture Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR19_Capture_Control_Analysis_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR19-Capture Control Analysis - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR19_Capture_Control_Analysis_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR19-Capture Control Analysis - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR19_Capture_Control_Analysis_Optional_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR19-Capture Control Analysis - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR20_Capture_Behaviour_Analysis_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR20-Capture Behaviour Analysis   - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR21_Capture_Change_Analysis  - Main Scenario
    @Test
    public void FR21_Capture_Change_Analysis_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR21-Capture Change Analysis  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR22_Declare_Event_Ready_For_Summary_and_Approval_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR22- Declare Event Ready for Summary and Approval - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR23_Capture_Summary_and_Approval_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR23- Capture Summary and Approval - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR23- Capture Summary and Approval - Alternate Scenario

    @Test
    public void FR23_Capture_Summary_and_Approval_Alternate_Scenario_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR23- Capture Summary and Approval - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR24_Declare_The_Event_Complete_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR24 - Declare the Event Complete  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR24_Declare_The_Event_Complete_Alternate_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR24 - Declare the Event Complete - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR25_Validate_Investigation_Information  - Main Scenario

    @Test
    public void FR25_Validate_Investigation_Information_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR25 – Validate Investigation Information  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR25 – Validate Investigation Information  -Alternate Scenario

    @Test
    public void FR25_Validate_Investigation_Information_Alternate_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR25 – Validate Investigation Information  -Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR26_Move_Event_to_Appropriate_Administrator_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR26 – Move Event to Appropriate Administrator  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR26_Move_Event_to_Appropriate_Administrator_Alternate_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR26 – Move Event to Appropriate Administrator  - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR27_Edit_Event_Management_Record_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\FR27-Edit Event Management Record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
     @Test
    public void UC_EVE_27_02_Delete_Event_Management_Record_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Event Managemant\\UC_EVE_27_02_Delete_Event_Management_Record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
