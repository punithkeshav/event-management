/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.EventManagement;

/**
 *
 * @author SMABE
 */
public class EventManagemant_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }
    
        public static String EntityexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";

    }

    public static String linkToADocument()
    {
        return "//div[@id='control_36C93CF0-9F8F-4661-A131-810C96569C49']//b[@original-title='Link to a document']";
    }

    public static String SupportingDocumentsTab()
    {
        return "//li[@id='tab_98124058-DEC1-41A5-82BF-7B07BA53107C']//div[text()='Supporting Documents']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String BusinessUnitDropDownOption2(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    
    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }
    
      public static String Check_box(String text)
    {
        return "//a[contains(text(),'"+text+"')]/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "']";
        //div[contains(@class,'transition visible')]//a[contains(text(),'Environmental Impact')]
    }

    public static String LinkToSite()
    {
        return "//div[@id='control_198375A9-1B33-46D1-A93F-180F33F21D25']";
    }

    public static String LinkToSiteDropDown()
    {
        return "//div[@id='control_3544E0CE-09D7-408E-8CB9-EC387D9B3675']//li";
    }

    public static String LinkToProjects()
    {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']";
    }

    public static String LinkToProjectsDropDown()
    {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']//li";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Saving...']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String EventManagemantTab()
    {
        return "//label[text()='Event Management']";
    }

    public static String AddButton()
    {
        return "//div[contains(@class,'active form transition visible')]//div[text()='Add']";
    }

    public static String ProcessFlowButton()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String TypeofEventDropDown()
    {
        return "//div[@id='control_9C6B5F1B-AF54-4B1D-BF12-0C076D64A87F']";
    }

    public static String businessUnitDropDown()
    {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]/..//i)[1]";
    }
    
    
     public static String WasEquipmentInvolvedOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]/..//i)[1]";
    }

    public static String EventTitle()
    {
        return "//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input";
    }

    public static String EventDescription()
    {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String FunctionalLocationDropDown()
    {
        return "//div[@id='control_C492928D-54AF-49E5-B173-A427BC1A2BE5']";
    }

    public static String SpecificLocation()
    {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input";
    }

    public static String PinToMap()
    {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']";
    }

    public static String DateOfEvent()
    {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input";
    }

    public static String DateReported()
    {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input";
    }

    public static String TimeOfEvent()
    {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input";
    }

    public static String TimeReported()
    {
        return "//div[@id='control_B623B7E6-DB94-4A72-933F-4847D48066D5']//input";
    }

    public static String ImmediateActionTaken()
    {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea";
    }

    public static String ReportedByDropDown()
    {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//li";
    }

    public static String ResponsibleSupervisorDropDown()
    {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']//li";
    }

    public static String ValidatorDropDown()
    {
        return "//div[@id='control_783CFD1D-ACE7-4A2B-B0CE-7B6883272992']//li";
    }

    public static String ReportedByCheckBox()
    {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String KeyPersonInvolved()
    {
        return "//div[@id='control_1D3E03CD-D399-4FEB-8EEE-839857F408EA']";
    }

    public static String ExternalPartiesInvolved()
    {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']";
    }

    public static String WasEquipmentInvolved()
    {
        return "//div[@id='control_811EB188-F2C5-4BD7-A01C-85A65C04F355']";
    }

    public static String WasEquipmentInvolvedDropDownCheckBox(String data)
    {
        return "(//a[text()='" + data + "']/..//a//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String StoppageDueToDropDownCheckBox(String data)
    {
        return "(//a[text()='" + data + "']/..//a//i[@class='jstree-icon jstree-checkbox'])";
    }

    public static String WasEquipmentInvolvedDropDown()
    {
        return "//div[@id='control_69BDF7EA-72B5-420E-B8CF-86D5DB6DF657']//ul";
    }

    public static String WasEquipmentInvolvedSelectAll()
    {
        return "//div[@id='control_69BDF7EA-72B5-420E-B8CF-86D5DB6DF657']//b[@class='select3-all']";
    }

    public static String KeyPersonInvolvedDropDown()
    {
        return "//div[@id='control_75D58974-F17E-49F1-9A75-ACC9F66F161B']//ul";
    }

    public static String KeyPersonInvolvedSelectAll()
    {
        return "//div[@id='control_75D58974-F17E-49F1-9A75-ACC9F66F161B']//b[@class='select3-all']";
    }

    public static String ExternalPartiesInvolvedSelectAll()
    {
        return "//div[@id='control_12497702-FCCB-47A2-9267-BE9A324421EC']//b[@class='select3-all']";
    }

    public static String ExternalPartiesInvolvedDropDown()
    {
        return "//div[@id='control_12497702-FCCB-47A2-9267-BE9A324421EC']//ul";

    }

    public static String UploadImage()
    {
        return "//div[@id='control_E39283CD-AF7A-4775-B1D7-8CE676E49D65']//b[@original-title='Upload an image']";
    }

    public static String Button_Save()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String recordSaved_popup_2()
    {
        return "//div[@class='ui floating icon message transition visible']//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String DVTPic()
    {
        return "DVT.PNG";
    }

    public static String mapZoominPic()
    {
        return "ZoomIn.PNG";
    }

    public static String mapZoomOutPic()
    {
        return "ZoomOut.PNG";
    }

    public static String mapPic()
    {
        return "map1.PNG";
    }

    public static String imagePic1()
    {
        return "image1.PNG";
    }

    public static String fileNamePic1()
    {
        return "FileName.PNG";
    }

    public static String fullReportPic1()
    {
        return "entertext.PNG";
    }

    public static String linkToADocument_2()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String UrlInput_TextArea()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextArea()
    {
        return "//input[@id='urlTitle']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Verification_Additional_Detail_Tab()
    {
        return "//div[text()='2.Verification & Additional Detail']";
    }

    public static String WhatHappenDropDown()
    {

        return "//div[@id='control_430B3444-0F67-42BB-A9CA-C17E14AE7448']//ul";
    }

    public static String HowDidItHappenDropDown()
    {
        return "//div[@id='control_58A81103-5F88-4AAF-8783-54D3BACD3403']//ul";
    }

    public static String ComplaintAnonymousDropDown()
    {
        return "//div[@id='control_E1D4CAF6-9C41-4F91-A2F0-2B606FEA59F4']//ul";
    }

    public static String PotentialConsequencesDropDown()
    {
        return "//div[@id='control_1397EF94-CA3A-477F-A6EF-F8C59398559A']//ul";
    }

    public static String PotentialConsequencesSelectAll()
    {
        return "//div[@id='control_1397EF94-CA3A-477F-A6EF-F8C59398559A']//b[@class='select3-all']";
    }

    public static String WhatHappenedToCauseTheHazardDropDown()
    {
        return "//div[@id='control_7D3026FE-8849-4A8D-87D9-0444E8203766']//ul";
    }

    public static String HazardTypeDropDown()
    {
        return "//div[@id='control_A5E685BD-32AF-448F-86B9-213D8095D0C4']//ul";
    }

    public static String RiskControlledDropDown()
    {
        return "//div[@id='control_C7EBA116-4565-4CF0-93DC-33B828686A59']//ul";
    }

    public static String WhatWasDoneToControlTheHazard()
    {
        return "//div[@id='control_1DCE3D07-28C8-4E56-BC03-41C3780E5278']//textarea";
    }

    public static String InterimDropDown()
    {
        return "//div[@id='control_F9FB9C47-6255-4615-BE88-4813D5F46BF2']//ul";
    }

    public static String OtherDescription()
    {

        return "//div[@id='control_00545A60-F93C-46B0-973E-62C056E887C4']//textarea";
    }

    public static String PotentialConsequencesDropDownOptin(String data)
    {
        return "(//a[text()='" + data + "']/..//i)[1]";
    }

    public static String PotentialConsequencesDropDownOptin2(String data)
    {
        return "(//a[contains(text(),'" + data + "')])//i[@class='jstree-icon jstree-checkbox']";
              //(//a[contains(text(),'3. Legal Moderate')]/..//i)[2]
    }

    public static String PotentialConsequencesDropDownClose()
    {
        return "//div[@id='control_1397EF94-CA3A-477F-A6EF-F8C59398559A']//b[@class='select3-down drop_click']";
    }

    public static String IssueNonComplianceInterventionTab()
    {
        return "//div[text()='Issue Non-Compliance Intervention'][contains(@control_color,'system')]";
    }

    public static String ButtonAdd()
    {
        return "//div[@id='control_91797873-DC3C-4C9C-B894-EE3357611828']//div[@id='btnAddNew']";
    }

    public static String ReportedByDropDown2()
    {
        return "//div[@id='control_7CC7802D-F601-479A-A368-EA7E27A2B502']//ul";
    }

    public static String InterventionUpliftmentDropDown()
    {
        return "//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']//ul";
    }

    public static String InterventionIssuedDropDown()
    {
        return "//div[@id='control_8A50209C-3EDB-40C6-A225-6EB5DA2AE0DF']//ul";
    }

    public static String DateOfNonCompliance()
    {
        return "//div[@id='control_E81D5D30-D0CF-4B4D-A45B-9D6A2759166A']//input";
    }

    public static String ReportedDateOfNonCompliance()
    {
        return "//div[@id='control_817B1336-983B-4C72-BAD5-304A992D37A1']//input";
    }

    public static String StoppageReferenceNumber()
    {
        return "//div[@id='control_CB772C7C-ABD8-4B0D-8618-F20ECC6A3F58']//input";
    }

    public static String Description()
    {
        return "//div[@id='control_BFA70286-6BD9-4063-ABA8-82FE45B96A6A']//textarea";
    }

    public static String StoppageOutcomeDropDown()
    {
        return "//div[@id='control_AB93D552-65AF-4AB1-AB2B-612D4C2F78DF']//ul";
    }

    public static String StoppageAffectedAreasDropDown()
    {
        return "//div[@id='control_5E1C5AC3-208F-453B-B154-0F6CF3EECD25']//ul";
    }

    public static String StoppageDueToDropDown()
    {
        return "//div[@id='control_F3E89F54-2B95-4D1F-9AB4-25A3A4B0BEBC']//ul";
    }

    public static String StopClassificationDropDown()
    {
        return "//div[@id='control_0870BCBF-A0B7-40B2-A18F-D088477B890C']//ul";
    }

    public static String WorkStoppageDropDown()
    {
        return "//div[@id='control_89EDFE37-2C85-45CD-9C73-338374164EF0']//ul";
    }

    public static String ClearanceClassificationdDropDown()
    {
        return "//div[@id='control_C6C29054-080D-4972-A794-811062B59AF2']//ul";
    }

    public static String StoppageInitiatedByDropDown()
    {
        return "//div[@id='control_FDBF77F4-AEAF-4F79-B2E2-7E4DB11A1DE5']//ul";
    }

    public static String TypeOfNonComplianceInterventionDropDown()
    {
        return "//div[@id='control_C92CA60D-7027-4F48-B9B4-60ABFDA93A62']//ul";
    }

    public static String Button_Save2()
    {
        return "//div[@id='btnSave_form_841EA22F-EE35-4590-AAD8-A4F93728F900']";
    }

    public static String Declaration_checkbox()
    {
        return "//div[@id='control_9F76042E-B404-4133-95F9-3297459D6C00']//div[@class='c-chk']//div";
    }

    public static String SaveAndClose()
    {
        return "//div[@id='control_7255070F-0A3C-4F8E-83BE-D938BFAA6A52']";
    }

    public static String VerificationAdditionalTab()
    {
        return "//div[text()='2.Verification & Additional Detail']";
    }

    public static String ProcessFlowButton2()
    {
        return "//div[@id='btnProcessFlow_form_841EA22F-EE35-4590-AAD8-A4F93728F900']";
    }

    public static String SubmittedByContractorDropDown()
    {
        return "//div[@id='control_926266E0-8F08-4820-8038-FB3C6230138B']//ul";
    }

    public static String SubmittedByDropDown()
    {
        return "//div[@id='control_F0414EFF-7917-4FD9-97FF-11E3AE87A73D']//ul";
    }

    public static String CategoryOfStakeholderInvolvedDropDown()
    {
        return "//div[@id='control_AFE5075E-9050-4CBA-87DE-854CA9EFD8D6']//ul";
    }

    public static String SubmittedViaDropDown()
    {
        return "//div[@id='control_31B74098-6B0E-43EC-B312-9830EB2ACD43']//ul";
    }

    public static String Action_Tab()
    {
        return "//div[text()='Actions']";
    }

    public static String ButtonAddActions()
    {
        return "//div[@id='control_45F45C6F-AA39-494A-A64D-5EA19914C37C']//div[@id='btnAddNew']";
    }

    public static String ProcessFlowActions()
    {
        return "//div[@id='btnProcessFlow_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String TypeOfActionDropDown()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ActionsResponsiblePersonDropDown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String ActionsAgencyDropDown()
    {
        return "//div[@id='control_5B580F56-394D-4695-8AB2-C2CB9AAE9EB9']//ul";
    }

    public static String Action_due_date()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String ActionsButton_Save()
    {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])[2]";
    }

    public static String EntityDropDown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String businessUnitOption_1(String data)
    {
        return "(//div[contains(@class,'transition visible')]//a[contains(text(),'" + data + "')]/../..//i)[1]";
    }

    public static String EventConsequenceAdd()
    {
        return "//div[@id='control_924216FF-AACE-4859-8E96-ADD62F03DA48']//div[@id='btnAddNew']";
    }

    public static String EventConsequenceProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_49E9FBC7-348B-410B-AFFD-F2D62E4EC134']";
    }

    public static String ConsequenceTypeDropDown()
    {
        return "//div[@id='control_5089B937-E116-45E6-9D9F-FD4839F061A5']//ul";
    }

    public static String ActualConsequenceTypeDropDown()
    {
        return "//div[@id='control_4E43DC8C-10EF-4D8A-A4B1-D52AF056D4ED']//ul";
    }

    public static String PotentialConsequenceTypeDropDown()
    {
        return "//div[@id='control_E3DC78CD-D3F2-4A06-B04C-562BE2B1672F']//ul";
    }

    public static String Consequenc_Button_Save()
    {
        return "//div[@id='btnSave_form_49E9FBC7-348B-410B-AFFD-F2D62E4EC134']";

    }

    public static String WasTreatmentProvidedDropDown()
    {
        return "//div[@id='control_70296474-83C8-42FC-8F57-08D33B9125B7']//ul";
    }

    public static String HospitalizedDropDown()
    {
        return "//div[@id='control_A84E0CD4-E104-4ECF-BF30-E3ED0C3690BD']//ul";
    }

    public static String TreatmentProvidedDescription()
    {
        return "//div[@id='control_41F7788B-8978-425B-BBDF-8EBBAFB416A3']//textarea";
    }

    public static String ConsecutiveDaysWorked()
    {
        return "//div[@id='control_C537E7D3-C9AA-474C-BF93-22AFE59B4B45']//input";
    }

    public static String ShiftDurationDropDown()
    {
        return "//div[@id='control_0E48E9A1-9C86-4E96-8ED6-367834A5DFE8']//ul";
    }

    public static String CrewDropDown()
    {
        return "//div[@id='control_3E7BA6B2-E951-43BA-B60F-0CC3E35DEDE4']//ul";
    }

    public static String InjuryDescription()
    {
        return "//div[@id='control_3814641E-44CD-4B49-BE18-8EAD9F877594']//textarea";
    }

    public static String FullNameDropDown()
    {
        return "//div[@id='control_FE661C41-E3A2-4EF6-988C-BE234F313C36']//ul";
        
    }

    public static String HoursIntoShift()
    {
        return "(//div[@id='control_F97DE49B-94CB-4227-A230-40C2CA7B8381']//input)[1]";
    }

    public static String EstimatedDaysLostFrom()
    {
        return "//div[@id='control_38F6A74A-0040-48B9-B620-355EAB9EFC82']//input";
    }

    public static String EstimatedRestrictedDaysLost()
    {
        return "//div[@id='control_CC37EA57-509A-407B-9287-26FFED17B449']//input";
    }

    public static String EstimatedDaysLost()
    {
        return "//div[@id='control_20CC5F37-88D1-4967-899D-811EE16C28BA']//input";
    }

    public static String RealRestrictedDaysLost()
    {
        return "//div[@id='control_4A492D0E-E48B-445D-8D37-67A237E3BDF8']//input";
    }

    public static String EstimatedRestrictedDaysLostFrom()
    {
        return "//div[@id='control_F16D8430-E65B-45EF-BE04-989D74CAF7D0']//input";
    }

    public static String RealDaysLostFrom()
    {
        return "//div[@id='control_7EF18E88-88C3-4E5D-9055-B11C71DB6C8C']//input";
    }

    public static String EstimatedDaysLostTo()
    {
        return "//div[@id='control_F667DB80-9CD6-4FB8-AF49-86D6302211E0']//input";
    }

    public static String RealDaysLostTo()
    {
        return "//div[@id='control_AA568A84-CAC0-4A33-A328-35CFDC599B59']//input";
    }

    public static String EstimatedRestrictedDaysLostTo()
    {
        return "//div[@id='control_6640B98D-86C0-4075-96C8-066067B21DB2']//input";
    }

    public static String RealRestrictedDaysLostTo()
    {
        return "//div[@id='control_44669D26-8E4B-4857-B4A2-1A6322CFF749']//input";
    }

    public static String RealDaysLost()
    {
        return "//div[@id='control_78E53F32-00D5-420F-B5C4-9878EA2CBC19']//input";
    }

    public static String InjuryAddButton()
    {
        return "//div[@id='control_528B40A2-36E4-4BD6-AE10-0F6093964FF8']//div[@id='btnAddNew']";
    }

    public static String InjuryBodyPart()
    {
        return "//div[@id='control_60D8D891-6B6D-4A42-B4AE-E99AFDB57C2A']//li";
    }

    public static String NatureOfInjuryDropDown()
    {
        return "//div[@id='control_C3FE7B53-A8F5-4D79-82FF-68354BAFC3A6']//li";
    }

    public static String OffSiteImpactDropDown()
    {
        return "//div[@id='control_03E3A2DE-CC73-4FC9-9030-18C825F25209']//li";
    }

    public static String WasaFineIssuedDropDown()
    {
        return "//div[@id='control_EF233109-C29F-4FD2-B85D-C9534572DF79']//li";
    }

    public static String RadioactiveTypeDropDown()
    {
        return "//div[@id='control_484BA062-3C01-4497-B5E1-9CC46030B414']//li";
    }

    public static String EnvironmentalHazardDropDown()
    {
        return "//div[@id='control_1C860AF4-25F8-466D-A9CB-6E0CE213D25D']//li";
    }

    public static String EnvironmentalHazardSelectAll()
    {
        return "//div[@id='control_1C860AF4-25F8-466D-A9CB-6E0CE213D25D']//b[@original-title='Select all']";
    }

    public static String PrimaryMediaImpactedDropDown()
    {
        return "//div[@id='control_C80FC512-49E2-40CD-AED8-9B9934763C1F']//li";
    }

    public static String Accountable_Person_Verification_DropDown()
    {

        return "//div[@id='control_15D0DCA2-D2F2-44D0-8E39-F07767540F34']//ul";
    }

    public static String Describe_scale_of_impact()
    {
        return "//div[@id='control_5608941D-3F34-4AB6-B1C3-FCDA98D637EC']//textarea";
    }

    public static String Describe_severity()
    {
        return "//div[@id='control_F3248954-05B7-48A9-ACDF-5476214B3194']//textarea";
    }

    public static String View_Environmental_Consequence()
    {
        return "//div[text()='View Environmental Consequence Level']";
    }

    public static String EventConsequenceFinesAdd()
    {
        return "//div[text()='Event Consequence Fines']/.././/div[@id='btnAddNew']";
    }

    public static String EventConsequenceFinesProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_80EC488A-F06B-431B-AA84-CCCF00555184']";
    }

    public static String EventConsequenceFinesDateOfFine()
    {
        return "//div[@id='control_C1194930-984E-4ED9-B65D-4981238AAF2F']//input";
    }

    public static String EventConsequenceFinesCurrency()
    {
        return "//div[@id='control_9B4564B3-882C-4D9A-8D90-309256430DE7']//ul";
    }

    public static String EventConsequenceFinesValueOfFine()
    {
        return "//div[@id='control_3037E1D3-EB8B-4A33-8BBC-BF27CE9B48AB']//input";
    }

    public static String EventConsequenceFinesContested()
    {
        return "//div[@id='control_86823C14-144B-474E-ACC8-EBED913C4D99']//ul";
    }

    public static String EventConsequenceFinesWasTheFineClosed()
    {
        return "//div[@id='control_E4E5290D-B389-441F-B77E-40B56C1B36E4']//ul";
    }

    public static String EventConsequenceFinesWasTheFineClosedDate()
    {
        return "//div[@id='control_752876EB-EF7E-4C2E-8633-8CDC5ACB77B7']//input";
    }

    public static String DateContested()
    {
        return "//div[@id='control_84C787A3-798F-42BC-9133-4A547842FEFA']//input";
    }

    public static String ContestedDetails()
    {
        return "//div[@id='control_B0536B00-3617-4C93-9BE7-0BF7489D7A9C']//textarea";
    }

    public static String Date_Of_Fine_Paid()
    {
        return "//div[@id='control_19E17AD7-0C42-4B22-B28C-0CFD0D369505']//input";
    }

    public static String Date_Of_Fine_Paid_Value()
    {
        return "//div[@id='control_AAD327FF-365D-4768-BD5A-B58D79352A3F']//input";
    }

    public static String EventConsequenceFineDescription()
    {
        return "//div[@id='control_4F23B57C-75A8-4B5A-A33D-6FB571329DCF']//textarea";
    }

    public static String Consequenc_Fine_Button_Save()
    {
        return "//div[@id='btnSave_form_80EC488A-F06B-431B-AA84-CCCF00555184']";
    }

    public static String WasFinePaidDropDoww()
    {
        return "//div[@id='control_DAA34FCE-3125-4FCA-B676-53BD4E09F990']//ul";
    }

    public static String Illustration_Description()
    {
        return "//div[@id='control_A4C95A56-137D-4D65-A043-D460F13F876D']//textarea";
    }

    public static String EventUploadImage()
    {
        return "//div[@id='control_CD51A180-66BD-4DDE-89C9-C07B265E2921']//div[@class='imagecontrol-droparea imagecontrol-empty']";
    }

    public static String VerifyConsequenceAdd()
    {
        return "//div[@id='control_4F00CEB5-B6B7-4073-ADA3-B3C14C4C33D0']//div[@id='btnAddNew']";
    }

    public static String VerifyConsequenceProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_6BE1D53C-2CF0-4D5F-BE5D-8207B6786859']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@id='control_7DA44F9C-5EE0-45CC-AA64-2C6729929C32']//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String VerifyActualConsequenceTypeDropDown()
    {
        return "//div[@id='control_73AB359F-CD8D-4C45-874B-099FC4086C32']//ul";
    }

    public static String VerifyPotentialConsequenceTypeDropDown()
    {
        return "//div[@id='control_E5F99C95-26A1-42E4-91C8-73FAC7774947']//ul";
    }

    public static String ReasonForUpdate()
    {
        return "//div[@id='control_5FEDC9D1-D110-4671-9940-BF458BEEF7CF']//textarea";
    }

    public static String Verify_Button_Save()
    {
        return"//div[@id='btnSave_form_6BE1D53C-2CF0-4D5F-BE5D-8207B6786859']";
    }
    
    public static String iframeName()
    {
        return "ifrMain";
    }

    public static String UpdateClassifications()
    {
       return "//div[@id='control_7975DC56-769F-4857-A0D2-6924C97EDEC2']";
    }

    public static String PropertyInvolvedDropDown()
    {
        return"//div[@id='control_80895A60-B844-4CFA-A665-A2F10EE5C6CD']//ul";
    }

    public static String PropertyOwnerDropDown()
    {
       return "//div[@id='control_D0E1CE06-4DAD-4B94-A504-171D6F5D483F']//ul";
    }

    public static String SpecifyOtherPropertyInvolved()
    {
       return "//div[@id='control_02DD4E79-830B-4256-BB53-F7332710297C']//textarea";
    }

    public static String ContractorCompanyNameDropDown()
    {
        return"//div[@id='control_2E828D32-D46E-4708-BED5-B1593FC884A3']//ul";
    }

    public static String EventUploadImage2()
    {
        return"//div[@id='control_CD51A180-66BD-4DDE-89C9-C07B265E2921']";
    }

    public static String Consequenc_Button_Save_And_Close_DropDown()
    {
       return "//div[@id='btnSave_form_49E9FBC7-348B-410B-AFFD-F2D62E4EC134']//div[@class='options toggle']";
    }

    public static String Consequenc_Button_Save_And_Close()
    {
      return  "//div[@id='btnSave_form_49E9FBC7-348B-410B-AFFD-F2D62E4EC134']//div[text()='Save and close']";
    }

    public static String OccupationalHealthAdd()
    {
       return "//div[@id='control_D5D03B59-C850-42AC-99E4-61AE291AA744']//div[@id='btnAddNew']";
    }

    public static String OccupationalHealthProcessFlow()
    {
      return  "//div[@id='btnProcessFlow_form_1323A32C-A6AA-49D4-817F-A02B134B8476']";
    }

    public static String OccupationalHealthFullNameDropDown()
    {
       return "//div[@id='control_7725D7BA-D651-4EE9-A3F1-C63F36112AA7']//ul";
    }

    public static String IllnessDiseaseTypeDropDown()
    {
       return "//div[@id='control_B6D0C630-45E9-442C-900A-30BC11958EB9']//ul";
    }

    public static String IllnessDescription()
    {
        return"//div[@id='control_40787EF3-8D8B-4EED-87F0-104FE762054A']//textarea";
    }

    public static String WasMedicalTreatmentProvidedDropDown()
    {
       return "//div[@id='control_64671BA5-FDD5-4073-9067-50E1B396A085']//ul";
    }

    public static String TreatmentProvidedDescription_2()
    {
       return "//div[@id='control_693221D2-EC07-4185-B7EF-89DC2AD2D49F']//textarea";
    }

    public static String HospitalizedDropDown_2()
    {
       return "//div[@id='control_2D9B9152-A357-42C1-BA48-48B03FF642AB']//ul";
    }

    public static String Occupational_Button_Save()
    {
        return"//div[@id='btnSave_form_1323A32C-A6AA-49D4-817F-A02B134B8476']";
    }

    public static String ExposureHazardDropDown()
    {
       return "//div[@id='control_5D74ACA0-EE6B-44B5-9C25-B27E7EEDDADD']//ul";
    }

    public static String SEGDropDown()
    {
        return"//div[@id='control_A161A96C-DDDD-4FA4-AFFC-124AEBD9891A']//ul";
    }

    public static String MonitoringDropDown()
    {
        return"//div[@id='control_A17FCDC8-7B7B-45B4-9502-0DEB3CEBE8FE']//ul";
    }

    public static String OELExceedanceDropDown()
    {
       return "//div[@id='control_ABAAA501-42DF-4996-B882-102FF016D280']//ul";
    }

    public static String CheckBox()
    {
       return "//div[@id='control_8AC963E8-5EB7-492E-8371-DFAB7FE3B5AD']//div[@class='c-chk']";
    }

    public static String DeclarationComments()
    {
        return"//div[@id='control_280FC061-EC2C-4A95-916F-6D47A94C4758']//textarea";
    }

    public static String LeadInvestigator()
    {
       return "//div[@id='control_97D0FB6B-ABA3-47F8-A4C6-8E0DB2C5E71A']//ul";
    }

    public static String EventInvestigationTab()
    {
       return "//div[text()='3.Event Investigation']";
    }

    public static String InvestigationDueDate()
    {
        return"//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String RevisedDate()
    {
        return"//div[@id='control_E3152B03-4ACD-40A4-BB6A-303CEB6EF37D']//input";
    }

    public static String ReasonForRevisedDate()
    {
       return "//div[@id='control_6CE5BA67-C735-4599-B551-3943DA46A169']//textarea";
    }

    public static String InvestigationAddButton()
    {
        return"//div[@id='control_3006D33A-81F0-4C4F-A7F7-3289CCEEA80E']//div[@id='btnAddNew']";
    }

    public static String InvestigationFullNameDropDown()
    {
       return "//div[@id='control_E0535A21-FBC8-4F90-B767-B9C02D121842']//ul";
    }

    public static String Experience_Role_DropDown()
    {
       return "//div[@id='control_A1BF9E59-A685-4589-A3B6-9519333C11D1']//ul";
    }

    public static String InvestigationTeamDropDown()
    {
        return"//span[text()='Investigation Team']";
    }

    public static String EvidenceAddButton()
    {
        return"//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@id='btnAddNew']";
    }

    public static String EvidenceDropDown()
    {
       return "//span[text()='Evidence']";
    }

    public static String EvidenceProcessButton()
    {
       return "//div[@id='btnProcessFlow_form_755C4AF1-11D8-42D8-87D4-647F5E4CD9E1']";
    }

    public static String EvidenceTypeDropDown()
    {
        return"//div[@id='control_AA4AF927-2DBF-4D95-9313-C2AE18E7C854']//ul";
    }

    public static String EvidenceName()
    {
        return"(//div[@id='control_96582EB8-B3CF-49C2-A0B2-86E1502C7918']//input)[1]";
    }

    public static String DateOfEvidenceCollection()
    {
        return"(//div[@id='control_F410822B-3AEF-449D-87F3-C17277F9D8AC']//input)[1]";
    }

    public static String EvidenceButton_Save()
    {
        return"//div[@id='btnSave_form_755C4AF1-11D8-42D8-87D4-647F5E4CD9E1']";
    }

    public static String SensitiveInformation()
    {
        return"//div[@id='control_3F5CD186-6B11-4EC6-9ADF-3F319493B362']//div[@class='c-chk']";
    }

    public static String EvidenceDescription()
    {
        return"//div[@id='control_F6FD8C0D-ECBA-4546-80C0-19E86BE85307']//textarea";
    }

    public static String StatementTaken()
    {
       return "//div[@id='control_7A1C0D8E-2CEF-4638-98A0-7F4A52B8F9FD']//textarea";
    }

    public static String StatementGivenBy()
    {
       return "//div[@id='control_56FB8823-9DE9-45F6-88DD-A284B4DA9B04']//ul";
    }

    public static String StatementTakenBy()
    {
       return "//div[@id='control_543F9ED5-5990-43CD-9E43-84F6588331A5']//ul";
    }

    public static String TypeofStatement()
    {
        return"//div[@id='control_0E008782-E24E-4F02-88FC-608D92431A1C']//ul";
    }

    public static String InvestigationDetailTab()
    {
        return"//div[text()='Investigation Detail']";
    }

    public static String AnalysisToolDropDown()
    {
        return"(//div[@id='control_8DBB27FB-F48F-4889-816A-E7464863BB15']//span//b)[1]";
    }

    public static String AnalysisToolPanel(String data)
    {
       return "//span[contains(text(),'"+data+"')]";
    }

    public static String AnalysisWhyAdd()
    {
       return "//div[@id='control_B7C0CE7B-98BE-4FB0-8D3B-AC374D28DE95']//div[@id='btnAddNew']";
    }

    public static String WhyTextArea()
    {
       return "(//div[@id='control_B7C0CE7B-98BE-4FB0-8D3B-AC374D28DE95']//textarea)[1]";
    }

    public static String WhyAnalysisButton()
    {
        return"//div[@id='control_A1B4E814-0353-40FC-8B9E-B5DCC3032970']";
    }

    public static String WhyAnalysisSaveButton()
    {
       return "//div[@id='btnSave']";
    }

    public static String ControlAnalysisPanel()
    {
        return"//span[text()='Control Analysis']";
    }

    public static String ControlAnalysisAdd()
    {
       return "//div[@id='control_FFC3A55F-7020-4548-929F-DAB50544E408']//div[@id='btnAddNew']";
    }

    public static String ControlTextArea()
    {
       return "(//div[@id='control_6F1CC1B0-4605-49B6-AF26-ACED673C704A']//input)[1]";
    }

    public static String ControlAnalysis()
    {
        return"//div[@id='control_3DC7FD7E-2E4E-4AAE-9374-AD5434B5DAB8']//ul";
    }

    public static String WhyDidTheControlFailTextArea()
    {
        return"//div[@id='control_88A1C989-E6DD-4FEA-B252-BF7DAFCCEFFC']//textarea";
    }

    public static String HowDidTheControlPerformTextArea()
    {
       return "//div[@id='control_599A83BE-64A4-4FAD-B12E-E8EC37E1F20F']//textarea";
    }

    public static String BehaviourAnalysisAdd()
    {
        return"//div[@id='control_622E463A-0600-45F1-B010-E323B8F66FED']//div[@id='btnAddNew']";
    }

    public static String ActivatorTextArea()
    {
       return "//div[@id='control_09BA7AEE-CACC-4260-BEE6-872034CB7A80']//textarea";
    }

    public static String BehaviourTextArea()
    {
        return"//div[@id='control_59B9EA7E-7FCD-4F9E-8CAE-A74553C79C23']//textarea";
    }

    public static String ConsequenceTextArea()
    {
        return"//div[@id='control_6773DBAD-76F3-4032-A8EE-90728025478A']//textarea";
    }

    public static String PositiveOrNegative()
    {
        return"//div[@id='control_69F90C91-1E7D-49EF-8553-59C0127D5443']//ul";
    }

    public static String CertainOrUncertainDropDown()
    {
        return"//div[@id='control_971A4750-EFB6-46A2-98C8-316704B28A23']//ul";
    }

    public static String ImmediateOrLaterDropDown()
    {
       return "//div[@id='control_9DFF1B7A-E3EA-4763-8656-6BFBC545E11B']//ul";
    }

    public static String BehaviourClassificationDropDown()
    {
        return"//div[@id='control_FDBE89A5-0147-4A80-9056-9331048BA18D']//ul";
    }

    public static String ChangeAnalysisAdd()
    {
       return "//div[@id='control_B5252362-C473-4879-8E45-8D41709F1A9B']//div[@id='btnAddNew']";
    }

    public static String NormalPracticeTextArea()
    {
        return"//div[@id='control_12834AA1-A592-4F70-93FC-4F7EDCBE9294']//textarea";
    }

    public static String SituationPracticeAtTimeOfEventTextArea()
    {
        return"//div[@id='control_CE784424-E0D5-4310-84A8-780D774F6CFA']//textarea";
    }

    public static String GapTextArea()
    {
       return "//div[@id='control_4BA16FF1-E123-494B-B9BA-4F5ABA142F28']//textarea";
    }

    public static String ImpactOfDifferenceArea()
    {
        return"//div[@id='control_9A66C7E2-7AA8-48E4-8714-58C061CE02F0']//textarea";
    }

    public static String DeclarationCheckbox()
    {
       return "//div[@id='control_3EFDAEFC-376C-4DBB-B48A-825E2E3BEABC']//div[@class='c-chk']";
    }

    public static String Summary_and_ApprovalTab()
    {
       return "//div[text()='Summary and Approval']";
    }

    public static String DetailedEventDescriptionTextArea()
    {
       return "//div[@id='control_EA446423-174B-433D-9C20-6211AF8D550D']//textarea";
    }

    public static String UploadImage_2()
    {
        return"//div[@id='control_E3D7A6FE-35F7-456B-B35C-5A2EDB015896']";
    }

    public static String ConclusionTextArea()
    {
        return"//div[@id='control_46CC5397-E89F-406E-ACC0-6AC270D0E371']//textarea";
    }

    public static String LearningsToBeharedTextArea()
    {
       return "//div[@id='control_0711AFDC-7A24-45F4-9A7D-4B27F4104D49']//textarea";
    }

    public static String LongTermTextArea()
    {
        return"//div[@id='control_E11FB03E-D70A-4AF1-A1F7-46104B6B48A9']//textarea";
    }

    public static String ShortTermTextArea()
    {
        return"//div[@id='control_CE0C145E-C092-41CD-87C6-74FA2C9720AE']//textarea";
    }

    public static String ImmediateActionsTextArea()
    {
        return"//div[@id='control_F878FCD3-51B0-4E93-9AB9-1F296F3CF0F1']//textarea";
    }

    public static String OrganizationalFactorsTextArea()
    {
       return "//div[@id='control_845E76F4-5F65-41B5-B251-6A3A44E6B3E3']//textarea";
    }

    public static String WorkplaceFactorsTextArea()
    {
       return "//div[@id='control_9709EC60-7BD4-47F1-8653-2A9CD56F4F2A']//textarea";
    }

    public static String IndividualFactorsTextArea()
    {
       return "//div[@id='control_E81A3859-7604-4466-9652-EA3F66149F75']//textarea";
    }

    public static String DeclarationCheckbox_2()
    {
        return"//div[@id='control_1F2CA953-B448-4A17-B1D0-383EB46D1349']//div[@class='c-chk']";
    }

    public static String InvestigationCommentsTextArea()
    {
       return "//div[@id='control_8D1AE953-65A5-4E95-A6D6-3237EA4A48B1']//textarea";
    }

    public static String EventValidationTab()
    {
       return "(//div[text()='4.Event Validation'])[1]";
        
    }

    public static String ReasonForRejectionTextArea()
    {
       return "//div[@id='control_E6C075F6-629D-4E1B-8F99-20A2D6290672']//textarea";
    }

    public static String ValidateDropDown()
    {
        return"//div[@id='control_11DDC00C-D79C-4DB7-BC97-99B8E1780772']//ul";
    }
      public static String measurementsPanel()
    {
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']";
    }

    public static String ContainsTextBox()
    {
        return"(//input[@class='txt border'])[1]";
    }
    public static String Record(String string)
    {
        return"//span[text()='"+string+"']";
    }
    public static String ValidationCommentsTextArea()
    {
        return"//div[@id='control_7724A885-8A47-4DAA-A13F-36D69C9F68F5']//textarea";
    }

     public static String CloseCurrentModule2()
    {
      return  "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }
     public static String CloseCurrentModule()
    {
        return"(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String MoveThisEventToTheAppropriatedministrator()
    {
        return"//div[@id='control_BF097E08-571F-40A6-8B6A-1BCEF1DC831F']//div[@class='c-chk']";
    }

    public static String DeleteButton()
    {
        return"//div[@id='btnDelete_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String Button_Save_And_Close_DropDown()
    {
       return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='options toggle']";
    }

    public static String Button_Save_And_Close()
    {
        return"//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[text()='Save and close']";
    }

    public static String OtherSubmissions()
    {
       return "//div[@id='control_42866563-9ABD-4E96-917F-3A42CB6F1B5B']//textarea";
    }

    public static String OtherStakeholders()
    {
       return "//div[@id='control_308DDB3F-9133-4CB3-AAC3-DFB10898D601']//textarea";
    }

    public static String FunctionalLocationexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Specific_location()
    {
        return"//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input";
    }

    public static String TeamNameDropDown()
    {
       return "//div[@id='control_EA09D66F-E141-426A-BECA-FAEB78B6D803']//ul";
    }

    public static String WasEquipmentInvolvedCheckBox(String data)
    {
       return "//a[contains(text(),'"+data+"')]/../..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String UploadImage1()
    {
        return "//div[@id='control_E39283CD-AF7A-4775-B1D7-8CE676E49D65']";
    }

    public static String Accountable_person_for_verification(String data)
    {
        return"(//li[@title='1 Administrator']//a[text()='"+data+"'])[3]";
    }

    public static String recordSaved_popup3()
    {
        return"(//div[contains(@class,'form transition visible active')]//div[@id='txtHeader'][contains(text(),'Record saved')])[1]";
    }

    public static String WhyTitleTextArea()
    {
       return "(//div[@id='control_520640B0-0730-4F01-9D34-D185B8CCDA27']//input)[1]";
    }

    public static String ControlDropDown()
    {
        return"//div[@id='control_17E5F29A-060D-4A83-8D0B-05E492BA6797']//ul";
    }

    public static String controldropdownoption(String data)
    {
        return"(//a[text()='"+data+"'])[1]";
    }

    public static String DeclarationCheckbox_1()
    {
       return "(//div[@id='control_B41CCC6C-9D63-4E09-B067-592FE483D3BA']/..//div[@class='c-chk'])[1]";
    }

    public static String ActivitiesDropDown()
    {
       return "//div[@id='control_A545A6BF-2235-4998-86DD-4246FD93E79A']//ul";
    }
}
