/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author fnell
 */
public class GmailPageObject extends BaseClass
{
   
        public static String IsometrixURL()
    {
        // Use ENUM
        return "https://mail.google.com/";
    }
        
    public static String GmailURL()
    {
        // Use ENUM
        return "https://mail.google.com/";
    }
    public static String GmailRegistration(){
        return "https://accounts.google.com/signup";
    }
    
    public static String emailTextBoxXpath()
    {
        return "//input[@type='email']";
    }
    
    public static String emailOrPhoneTextBoxXpath()
    {
        return "//input[@id='identifierId']";
    }
    
    public static String nextButtonXpath()
    {
        return "//input[@id='next']";
    }
    
    public static String newNextButtonXpath()
    {
        return "//span[text()='Next']";
    }
    
    public static String passwordTextBoxXpath()
    {
        return "//input[@id='Passwd']";
    }
    
    public static String newPasswordTextBoxXpath()
    {
        return "//input[@type='password']";
    }
    
    
    public static String signInButtonXpath()
    {
        return "//input[@id='signIn']";
    }
    
    
    public static String composeButtonXpath()
    {
        return "//div[text()='COMPOSE']";
    }
    
    
    public static String recipientsTextBoxXpath()
    {
        return "//textarea[@aria-label='To']";
    }
    
    public static String subjectTextBoxXpath()
    {
        return "//input[@name='subjectbox']";
    }
    
    
    public static String messageBodyTextBoxXpath()
    {
        return "//div[@aria-label='Message Body']";
    }
    
    
    public static String sendButtonXpath()
    {
        return "//div[text()='Send']";
    }

    public static String sendConfirmationMessageXpath()
    {
        return "//div[contains(text(),'Your message has been sent')]";
    }
    
    
    public static String inboxItemSelectCheckboxGenericXpath(String subjectLineText)
    {
        return "//span/b[contains(text(),'" + subjectLineText + "')]/../../../../../..//div[@role='checkbox']";
    }
    
    public static String deleteButtonXpath()
    {
        return "//div[@data-tooltip='Delete']";
    }
     public static String first_name() {
        return "//input[@name='firstName']";
    }
    public static String last_Name() {
        return "//input[@name='lastName']";
    }
    public static String createAccount_Link() {
        return "(//a[contains(text(),'Create an account')])[4]";
    }

    public static String password() {
        return "//input[@name='Passwd']";
    }

    public static String emailAddress() {
        return "//input[@name='Username']";
    }

    public static String confirm_Password() {
        return "//input[@name='ConfirmPasswd']";
    }

    public static String next_Button() {
        return "//span[contains(text(),'Next')]";
    }

    public static String PhoneNumber() {
        return "//input[@id='phoneNumberId']";
    }

    public static String code() {
        return "//input[@id='code']";
    }

    public static String verify() {
        return "//span[contains(text(),'Verify')]";
    }

    public static String Gender() {
        return "//select[@id='gender']";
    }

    public static String Gender_Select(String data) {
        return "//select[@id='gender']//option[contains(text(),'" + data + "')]";
    }

    public static String day() {
        return "//input[@id='day']";
    }

    public static String month_Select(String data) {
        return "//select[@id='month']//option[contains(text(),'" + data + "')]";
    }

    public static String Month() {
        return "//select[@id='month']";
    }

    public static String year() {
        return "//input[@id='year']";
    }

    public static String Yes_Button() {
        return "//span[contains(text(),'Yes, I’m in')]";
    }

    public static String moreOptions_Button() {
        return "//div[contains(text(),'More options')]";
    }

    public static String Agree_Button() {
        return "//span[contains(text(),'I agree')]";
    }
    public static String home(){
        return "(//div[contains(text(),'Home')])[2]";
    }
}