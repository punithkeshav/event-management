/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5 - Capture Controls - Main Scenario",
        createNewBrowserInstance = false
)
public class FR5_Capture_Controls_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Capture_Controls_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Controls())
        {
            return narrator.testFailed("Capture Controls  Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Controls");
    }

    public boolean Controls()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ControlsAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ControlsAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click  add button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Controls_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Controls_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
        {
            error = "Failed to wait for Control source drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
        {
            error = "Failed to click Control source dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Control source  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Control source")))
        {
            error = "Failed to enter Control source option :" + getData("Control source");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Control source"))))
        {
            error = "Failed to wait for Control source drop down option : " + getData("Control source");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Control source"))))
        {
            error = "Failed to click Control source drop down option : " + getData("Control source");
            return false;
        }

        narrator.stepPassed("Control source :" + getData("Control source"));

        //Add new  control
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Control_Layer_3()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Control_Layer_3(), getData("Control source")))
            {
                error = "Failed to enter Control source option :" + getData("Control source");
                return false;
            }
            narrator.stepPassedWithScreenShot("Control source :" + getData("Control source"));

        }

        //Select from layer 2 controls
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Bowtie_Control_Layer_DropDown()))
        {

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Bowtie_Control_Layer_DropDown()))
            {
                error = "Failed to click Bowtie Control (Layer 2) drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Hazard classification  text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Bowtie Control (Layer 2)")))
            {
                error = "Failed to wait for Select from layer 2 controls option :" + getData("Bowtie Control (Layer 2)");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Bowtie Control (Layer 2)"))))
            {
                error = "Failed to wait for Bowtie Control (Layer 2) drop down option : " + getData("Bowtie Control (Layer 2)");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Bowtie Control (Layer 2)"))))
            {
                error = "Failed to click Bowtie Control (Layer 2)  drop down option : " + getData("Bowtie Control (Layer 2)");
                return false;
            }

            narrator.stepPassedWithScreenShot("Bowtie Control (Layer 2) :" + getData("Bowtie Control (Layer 2)"));

        }

        //Select from layer 3 controls
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Select_from_layer_3_DropDown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Select_from_layer_3_DropDown()))
            {
                error = "Failed to click Bowtie Control (Layer 2) drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Hazard classification  text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Layer 3 control")))
            {
                error = "Failed to wait for Layer 3 control option :" + getData("Layer 3 control");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Layer 3 control"))))
            {
                error = "Failed to wait for Layer 3 control drop down option : " + getData("Layer 3 control");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Layer 3 control"))))
            {
                error = "Failed to click Layer 3 control drop down option : " + getData("Layer 3 control");
                return false;
            }

            narrator.stepPassedWithScreenShot("Layer 3 control :" + getData("Layer 3 control"));

        }
        
        //Go / no-go control
         if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Go_No_Go_ControlDropDown()))
        {
            error = "Failed to wait for Go / no-go control drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Go_No_Go_ControlDropDown()))
        {
            error = "Failed to click Go / no-go control dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Go / no-go control  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Go / no-go control")))
        {
            error = "Failed to wait for Go / no-go control option :" + getData("Go / no-go control");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Go_no_go_controlOption(getData("Go / no-go control"))))
        {
            error = "Failed to wait for Go / no-go control drop down option : " + getData("Go / no-go control");
            return false;
        }
        
        
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Go_no_go_controlOption(getData("Go / no-go control"))))
        {
            error = "Failed to click  Go / no-go control drop down option : " + getData("Go / no-go control");
            return false;
        }

        narrator.stepPassedWithScreenShot("Go / no-go control :" + getData("Go / no-go control"));
        
        

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Controls_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Controls_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }
}
