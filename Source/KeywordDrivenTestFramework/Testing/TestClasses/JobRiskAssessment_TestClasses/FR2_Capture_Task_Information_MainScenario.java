/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Task Information - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Task_Information_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Task_Information_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!TaskInformation())
        {
            return narrator.testFailed("Section B - JRA / PTO Task Information Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Task Information");
    }

    public boolean TaskInformation()
    {
        //Navigate to Section B - JRA / PTO Task Information
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.SectionB_Tab()))
        {
            error = "Failed to wait for Section B - JRA / PTO Task Information";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.SectionB_Tab()))
        {
            error = "Failed to click on Section B - JRA / PTO Task Information";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Section B - JRA / PTO Task Information tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskInformationAdd()))
        {
            error = "Failed to wait for Task Information add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TaskInformationAdd()))
        {
            error = "Failed to click on Task Information add button.";
            return false;
        }

        narrator.stepPassed("Successfully click Task Information add button.");
        
         //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskInformation_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TaskInformation_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Order()))
        {
            error = "Failed to wait for Order text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Order(), getData("Order")))
        {
            error = "Failed to enter Order :" + getData("Order");
            return false;
        }
        narrator.stepPassed("Order :" + getData("Order"));

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskActivityStepDescription()))
        {
            error = "Failed to wait for Task / activity step description text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TaskActivityStepDescription(), getData("Task / activity step description")))
        {
            error = "Failed to enter Task / activity step description :" + getData("Task / activity step description");
            return false;
        }

        narrator.stepPassed("Task / activity step description :" + getData("Task / activity step description"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }
}
