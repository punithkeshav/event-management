/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6-Capture Actions - Main Scenario",
        createNewBrowserInstance = false
)
public class FR6_Capture_Actions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Capture_Actions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Actions())
        {
            return narrator.testFailed("Capture Actions Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Actions");
    }

    public boolean Actions()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_Tab()))
        {
            error = "Failed to wait for Actions Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_Tab()))
        {
            error = "Failed to click on Actions Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions Tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionsAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActionsAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click  add button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of action  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Type of action")))
        {
            error = "Failed to enter Type of action option :" + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Type of action"))))
        {
            error = "Failed to click Type of action drop down option : " + getData("Type of action");
            return false;
        }

        narrator.stepPassed("Type of action :" + getData("Type of action"));

        //Add new  control
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.ActionDescription(), getData("Action description")))
        {
            error = "Failed to enter Action description  :" + getData("Action description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Action description  :" + getData("Action description"));

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionsEntityDropDown()))
        {
            error = "Failed to wait for Entity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActionsEntityDropDown()))
        {
            error = "Failed to click 'Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Business unit")))
        {
            error = "Failed to wait for Entity option :" + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text3(getData("Business unit"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.EntityexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.EntityexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ResponsiblePersonDropDown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ResponsiblePersonDropDown()))
            {
                error = "Failed to click  Responsible person drop down.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Related Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click  Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person  :" + getData("Responsible person"));

        //Date
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Action_due_date()))
        {
            error = "Failed to wait for Action due date  text box.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Action_due_date(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" + startDate);

        

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }
}
