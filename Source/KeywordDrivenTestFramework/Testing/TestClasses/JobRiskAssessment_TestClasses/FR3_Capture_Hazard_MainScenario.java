/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR3 - Capture Hazard - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Capture_Hazard_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Hazard_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureHazard ())
        {
            return narrator.testFailed("Capture Hazard  Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Hazard");
    }

    public boolean CaptureHazard()
    {
       

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.HazardAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.HazardAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click Task Information add button.");
        
         //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Hazard_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Hazard_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Hazard()))
        {
            error = "Failed to wait for Hazard text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Hazard(), getData("Hazard")))
        {
            error = "Failed to enter Hazard :" + getData("Hazard");
            return false;
        }
        narrator.stepPassed("Hazard :" + getData("Hazard"));

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.HazardclassificationDropDown()))
        {
            error = "Failed to wait for Hazard classification drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.HazardclassificationDropDown()))
        {
            error = "Failed to click 'Hazard classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Hazard classification  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Hazard classification")))
        {
            error = "Failed to wait for Hazard classification option :" + getData("Hazard classification");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Hazard classification"))))
        {
            error = "Failed to wait for Hazard classification drop down option : " + getData("Hazard classification");
            return false;
        }
        
        
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Hazard classification"))))
        {
            error = "Failed to click  Hazard classification drop down option : " + getData("Hazard classification");
            return false;
        }

        narrator.stepPassed("Hazard classification :" + getData("Hazard classification"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Hazard_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Hazard_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }
}
