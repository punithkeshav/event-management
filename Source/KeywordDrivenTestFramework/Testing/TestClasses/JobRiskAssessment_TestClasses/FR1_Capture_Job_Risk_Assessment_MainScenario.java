/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Job Risk Assessment - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Job_Risk_Assessment_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Job_Risk_Assessment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToJobRiskAssessment())
        {
            return narrator.testFailed("Navigate To Job Risk Assessment Failed due :" + error);
        }
        if (!CaptureJobRiskAssessment())
        {
            return narrator.testFailed("Capture Job Risk Assessment Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Job Risk Assessment");
    }

    public boolean NavigateToJobRiskAssessment()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobRiskAssessmentTab()))
        {
            error = "Failed to wait for Job Risk Assessment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JobRiskAssessmentTab()))
        {
            error = "Failed to click on Job Risk Assessment tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Job Risk Assessment tab.");

        pause(4000);
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureJobRiskAssessment()
    {
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Active / inactive
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActiveInactiveDropDown()))
        {
            error = "Failed to wait for Active / inactive dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActiveInactiveDropDown()))
        {
            error = "Failed to click Active / inactive dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Active / inactive text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Active / inactive"))))
        {
            error = "Failed to wait for Active / inactive drop down option : " + getData("Active / inactive");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Active / inactive"))))
        {
            error = "Failed to click Active / inactive drop down option : " + getData("Active / inactive");
            return false;
        }

        narrator.stepPassedWithScreenShot("Active / inactive option  :" + getData("Active / inactive"));

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.EntityDropDown()))
        {
            error = "Failed to wait for Entity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.EntityDropDown()))
        {
            error = "Failed to click 'Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Business unit")))
        {
            error = "Failed to wait for Entity option :" + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Business unit"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.EntityexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.EntityexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Functional location
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to wait for Functional location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to click  Functional location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional location text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text3(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.FunctionalLocationexpandButton()))
        {
            error = "Failed to wait to expand Functional location";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.FunctionalLocationexpandButton()))
        {
            error = "Failed to expand Functional location";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to wait for :" + getData("Functional location 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Functional location  option"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Functional location  option"))))
        {
            error = "Failed to click  Functional location drop down option : " + getData("Functional location  option");
            return false;
        }

        //Process / activity
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ProcessActivityDropDown()))
        {
            error = "Failed to wait for Process / activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ProcessActivityDropDown()))
        {
            error = "Failed to click Process / activity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Process / activity Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Process / activity"))))
        {
            error = "Failed to wait for Process / activity drop down option : " + getData("Process / activity");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Process / activity"))))
        {
            error = "Failed to click Process / activity drop down option : " + getData("Process / activity");
            return false;
        }

        narrator.stepPassedWithScreenShot("Process / activity :" + getData("Process / activity"));

        //Job / task title
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobTaskTitle()))
        {
            error = "Failed to enter Job / task title :" + getData("Introduction");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.JobTaskTitle(), getData("Job / task title")))
        {
            error = "Failed to enter Job / task title :" + getData("Job / task title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Job / task title :" + getData("Job / task title"));

        //Related SAP task
//          if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.RelatedSAPTaskDropDown()))
//        {
//            error = "Failed to wait for Related SAP task dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.RelatedSAPTaskDropDown()))
//        {
//            error = "Failed to click 'Related SAP task dropdown.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Related SAP task Unit text box.";
//            return false;
//        }
        //JRA reference number
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JRAReferenceNumber()))
        {
            error = "Failed to enter JRA reference number :" + getData("JRA reference number");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.JRAReferenceNumber(), getData("JRA reference number")))
        {
            error = "Failed to enter JRA reference number  :" + getData("JRA reference number");
            return false;
        }
        narrator.stepPassedWithScreenShot("JRA reference number :" + getData("JRA reference number"));

        //Job / task scope and objectives
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobTaskScopeAndObjectives()))
        {
            error = "Failed to for Job / task scope and objectives text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.JobTaskScopeAndObjectives(), getData("Job / task title")))
        {
            error = "Failed to enter  Job / task scope and objectives :" + getData("Job / task title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Job / task scope and objectives :" + getData("Job / task title"));

        //JRA team leader
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JRATeamLeaderDropDown()))
        {
            error = "Failed to wait for JRA team leader drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JRATeamLeaderDropDown()))
        {
            error = "Failed to click JRA team leader drop down ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for JRA team leader text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch(), getData("JRA team leader")))
        {
            error = "Failed to enter  JRA team leader :" + getData("JRA team leader");
            return false;
        }
          if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("JRA team leader"))))
        {
            error = "Failed to wait for JRA team leader drop down option : " + getData("JRA team leader");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("JRA team leader"))))
        {
            error = "Failed to click JRA team leader drop down option : " + getData("JRA team leader");
            return false;
        }
        narrator.stepPassedWithScreenShot("JRA team leader :" + getData("JRA team leader"));

        //JRA recorded by
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JRARecordedByDropDown()))
        {
            error = "Failed to wait for JRA recorded by drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JRARecordedByDropDown()))
        {
            error = "Failed to click JRA recorded by drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for JRA recorded by text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch(), getData("JRA team leader")))
        {
            error = "Failed to enter  JRA recorded by :" + getData("JRA team leader");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text3(getData("JRA team leader"))))
        {
            error = "Failed to wait for JRA recorded by drop down option : " + getData("JRA team leader");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text3(getData("JRA team leader"))))
        {
            error = "Failed to click JRA recorded by drop down option : " + getData("JRA team leader");
            return false;
        }
        narrator.stepPassedWithScreenShot("JRA recorded by :" + getData("JRA team leader"));

        //Date JRA conducted
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.DateJRAConducted()))
        {
            error = "Failed to wait for Date JRA conducted";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.DateJRAConducted(), startDate))
        {
            error = "Failed to enter Date JRA conducted :" + getData("Job / task title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Date JRA conducted :" + startDate);

        //Is there an existing procedure for the task to be assessed?
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ExistingProcedureDropDown()))
        {
            error = "Failed to wait for Is there an existing procedure drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ExistingProcedureDropDown()))
        {
            error = "Failed to click Is there an existing procedure drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Is there an existing procedure text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Is there an existing procedure")))
        {
            error = "Failed to enter  Is there an existing procedure :" + getData("Is there an existing procedure");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Is there an existing procedure"))))
        {
            error = "Failed to wait for Is there an existing procedure drop down option : " + getData("Is there an existing procedure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Is there an existing procedure"))))
        {
            error = "Failed to click Is there an existing procedure drop down option : " + getData("Is there an existing procedure");
            return false;
        }
        narrator.stepPassedWithScreenShot("Is there an existing procedure :" + getData("Is there an existing procedure"));

        //List equipment / tools required for task
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ListEquipment()))
        {
            error = "Failed to enter List equipment / tools required for task :" + getData("List equipment / tools required for task");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.ListEquipment(), getData("List equipment / tools required for task")))
        {
            error = "Failed to enter List equipment / tools required for task :" + getData("List equipment / tools required for task");
            return false;
        }
        narrator.stepPassedWithScreenShot("Job / task title :" + getData("Job / task title"));

        //Do the task activities impact on other people / work?
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActivitiesImpactDropDown()))
        {
            error = "Failed to wait for Do the task activities impact on other people / work? drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActivitiesImpactDropDown()))
        {
            error = "Failed to click Do the task activities impact on other people / work? drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Do the task activities impact on other people / work? text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Do the task activities impact on other people / work")))
        {
            error = "Failed to enter  Do the task activities impact on other people / work? :" + getData("Do the task activities impact on other people / work");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text3(getData("Do the task activities impact on other people / work"))))
        {
            error = "Failed to wait for Do the task activities impact on other people / work? drop down option : " + getData("Do the task activities impact on other people / work");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text3(getData("Do the task activities impact on other people / work"))))
        {
            error = "Failed to click Do the task activities impact on other people / work? drop down option : " + getData("Do the task activities impact on other people / work");
            return false;
        }
        narrator.stepPassedWithScreenShot("Do the task activities impact on other people / work? :" + getData("Do the task activities impact on other people / work"));

        //Team Name
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TeamNameDropDown()))
        {
            error = "Failed to wait for Team Name drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TeamNameDropDown()))
        {
            error = "Failed to click Team Name drop down :";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Team Name text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Team Name")))
        {
            error = "Failed to enter  Team Name :" + getData("Team Name");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Team Name"))))
        {
            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Team Name"))))
        {
            error = "Failed to click Team Name drop down option : " + getData("Team Name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Team Name :" + getData("Team Name"));

        
        
        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    public boolean UploadDocument()
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
////        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.linkADoc_Add_button()))
//        {
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.linkADoc_Add_button()))
//        {
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
