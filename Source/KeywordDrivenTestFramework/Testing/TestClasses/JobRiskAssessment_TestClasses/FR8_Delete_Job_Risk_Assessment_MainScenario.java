/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;

/**
 *
 * @author SMabe
 */

@KeywordAnnotation(
        Keyword = "FR8-Delete Job Risk Assessment Main Scenario",
        createNewBrowserInstance = false
)
public class FR8_Delete_Job_Risk_Assessment_MainScenario extends BaseClass
{

    String error = "";

    public FR8_Delete_Job_Risk_Assessment_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteJobRiskAssessment())
        {
            return narrator.testFailed("Failed To Delete Job Risk Assessment :" + error);
        }

        return narrator.finalizeTest("Successfully Deleted Job Risk Assessment");
    }
    
    
    public boolean DeleteJobRiskAssessment() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the current module close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        pause(3000);

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        pause(5000);

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.JobRiskAssessment_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Job / task title
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.JobTaskTitle()))
        {
            error = "Failed to enter Job / task title";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }


        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }
        narrator.stepPassedWithScreenShot("");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);
        return true;
        
    }
}
