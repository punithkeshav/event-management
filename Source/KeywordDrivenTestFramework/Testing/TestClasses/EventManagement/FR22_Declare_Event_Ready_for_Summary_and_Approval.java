/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR22- Declare Event Ready for Summary and Approval",
        createNewBrowserInstance = false
)
public class FR22_Declare_Event_Ready_for_Summary_and_Approval extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR22_Declare_Event_Ready_for_Summary_and_Approval()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Declare Event Ready for Summary and Approval Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Declare Event Ready for Summary and Approval");
    }

    public boolean CaptureDetails()
    {

        if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.DeclarationCheckbox_1()))
        {
            error = "Failed to scroll to check box";
            return false;
        }
        //declaration checkbox 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DeclarationCheckbox_1()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DeclarationCheckbox()))
            {
                error = "Failed to wait for declaration checkbox";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.DeclarationCheckbox_1()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.DeclarationCheckbox()))
            {
                error = "Failed to click declaration checkbox";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Declaration Checkbox Clicked");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Summary_and_ApprovalTab()))
        {
            error = "Failed to wait for Summary and Approval";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.Summary_and_ApprovalTab()))
        {
            error = "Failed to scroll to Summary and Approval";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Summary_and_ApprovalTab()))
        {
            error = "Failed to scroll to Summary and Approval";
            return false;
        }

        narrator.stepPassedWithScreenShot("Summary and Approval");

        //Detailed event description
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DetailedEventDescriptionTextArea()))
        {
            error = "Failed to wait Detailed event description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DetailedEventDescriptionTextArea(), getData("Detailed event description")))
        {
            error = "Failed to enter Detailed event description";
            return false;
        }
        narrator.stepPassedWithScreenShot("Detailed event description :" + getData("Detailed event description"));

        if (getData("UploadImage").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.UploadImage_2()))
            {
                error = "Failed to wait for Upload Image";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.UploadImage_2()))
            {
                error = "Failed to click Upload Image";
                return false;
            }
            pause(7000);
            String ImageUpload = System.getProperty("user.dir") + getData("Image Path");
            //Path path = Paths.get("C:","IsoMetrixModules","Event Management","images");
            String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images\\" + EventManagemant_PageObjects.DVTPic();
            String path = new File(pathofImages).getAbsolutePath();
            System.out.println("path " + pathofImages);

            if (!sikuliDriverUtility.EnterText(EventManagemant_PageObjects.fileNamePic1(), path))
            {
                error = "Failed to click image 1";
                return false;
            }
            pause(3000);
            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
            pause(5000);
            narrator.stepPassedWithScreenShot("Image uploaded");
        }

        //Individual factors
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.IndividualFactorsTextArea()))
        {
            error = "Failed to wait Individual factors";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.IndividualFactorsTextArea(), getData("Individual factors")))
        {
            error = "Failed to enter Individual factors";
            return false;
        }
        narrator.stepPassedWithScreenShot("Individual factors :" + getData("Individual factors"));

        //Workplace factors
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WorkplaceFactorsTextArea()))
        {
            error = "Failed to wait Workplace factors";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.WorkplaceFactorsTextArea(), getData("Workplace factors")))
        {
            error = "Failed to enter Workplace factors";
            return false;
        }
        narrator.stepPassedWithScreenShot("Workplace factors :" + getData("Workplace factors"));

        //Organizational factors
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OrganizationalFactorsTextArea()))
        {
            error = "Failed to wait Organizational factors";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.OrganizationalFactorsTextArea(), getData("Organizational factors")))
        {
            error = "Failed to enter Organizational factors";
            return false;
        }
        narrator.stepPassedWithScreenShot("Organizational factors :" + getData("Organizational factors"));

        //Immediate actions
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ImmediateActionsTextArea()))
        {
            error = "Failed to wait Immediate actions";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ImmediateActionsTextArea(), getData("Immediate actions")))
        {
            error = "Failed to enter Immediate actions";
            return false;
        }
        narrator.stepPassedWithScreenShot("Immediate actions :" + getData("Immediate actions"));

        //Short term (0-3 months)
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ShortTermTextArea()))
        {
            error = "Failed to wait Short term (0-3 months)";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ShortTermTextArea(), getData("Short term (0-3 months)")))
        {
            error = "Failed to enter Short term (0-3 months)";
            return false;
        }
        narrator.stepPassedWithScreenShot("Short term (0-3 months) :" + getData("Short term (0-3 months)"));

        //Long term (3-6 months)
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LongTermTextArea()))
        {
            error = "Failed to wait Long term (3-6 months)";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.LongTermTextArea(), getData("Long term (3-6 months)")))
        {
            error = "Failed to enter Long term (3-6 months)";
            return false;
        }
        narrator.stepPassedWithScreenShot("Long term (3-6 months) :" + getData("Long term (3-6 months)"));

        //Learnings to be shared with the SHE community
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LearningsToBeharedTextArea()))
        {
            error = "Failed to wait Gap";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.LearningsToBeharedTextArea(), getData("Learnings to be shared with the SHE community")))
        {
            error = "Failed to enter Gap";
            return false;
        }
        narrator.stepPassedWithScreenShot("Learnings to be shared with the SHE community :" + getData("Learnings to be shared with the SHE community"));

        //Conclusion
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConclusionTextArea()))
        {
            error = "Failed to wait Conclusion";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ConclusionTextArea(), getData("Conclusion")))
        {
            error = "Failed to enter Conclusion";
            return false;
        }
        narrator.stepPassedWithScreenShot("Conclusion :" + getData("Conclusion"));

        if (getData("tick check box").equalsIgnoreCase("True"))
        {
            //check box

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DeclarationCheckbox_2()))
            {
                error = "Failed to wait for declaration checkbox";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.DeclarationCheckbox_2()))
            {
                error = "Failed to click declaration checkbox";
                return false;
            }

            narrator.stepPassedWithScreenShot("Declaration Checkbox Clicked");

            //Investigation comments
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationCommentsTextArea()))
            {
                error = "Failed to wait Investigation comments";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.InvestigationCommentsTextArea(), getData("Investigation comments")))
            {
                error = "Failed to enter Investigation comments";
                return false;
            }
            narrator.stepPassedWithScreenShot("Investigation comments :" + getData("Investigation comments"));

        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }
}
