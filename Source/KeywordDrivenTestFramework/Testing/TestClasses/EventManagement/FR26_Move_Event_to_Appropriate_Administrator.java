/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR26 Move Event to Appropriate Administrator",
        createNewBrowserInstance = false
)
public class FR26_Move_Event_to_Appropriate_Administrator extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR26_Move_Event_to_Appropriate_Administrator()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Move Event to Appropriate Administrator  Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Move Event to Appropriate Administrator");
    }

    public boolean CaptureDetails()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.MoveThisEventToTheAppropriatedministrator()))
        {
            error = "Failed to wait for Move this event to the appropriate administrator";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.MoveThisEventToTheAppropriatedministrator()))
        {
            error = "Failed to scroll to Move this event to the appropriate administrator";
            return false;
        }

        if (getData("Click").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.MoveThisEventToTheAppropriatedministrator()))
            {
                error = "Failed to click Move this event to the appropriate administrator";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully Clicked Move this event to the appropriate administrator");

            //save
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            String saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            String record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");

        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Move this event to the appropriate administrator");

        return true;
    }
}
