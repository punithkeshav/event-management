/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR19-Capture Control Analysis",
        createNewBrowserInstance = false
)
public class FR19_Capture_Control_Analysis extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR19_Capture_Control_Analysis()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Why Analysis Due To :" + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!WhyAnalysisActions())
            {
                return narrator.testFailed("Failed To Click Why Analysis Due To :" + error);
            }
        }

        return narrator.finalizeTest("Successfully Captured Why Analysis");
    }

    public boolean CaptureDetails()
    {

        //Event Investigation Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to wait for Event Investigation Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to click Event Investigation Tab ";
            return false;
        }

        //Event Investigation Detail Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationDetailTab()))
        {
            error = "Failed to wait for Investigation Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InvestigationDetailTab()))
        {
            error = "Failed to click Event Investigation Detail";
            return false;
        }

        narrator.stepPassedWithScreenShot("Event Investigation Detail");

        //Tool Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ControlAnalysisPanel()))
        {
            error = "Failed to wait for Control Analysis panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ControlAnalysisPanel()))
        {
            error = "Failed to click Control Analysis panel";
            return false;
        }

        narrator.stepPassedWithScreenShot("Control Analysis panel");

        //Add Button 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ControlAnalysisAdd()))
        {
            error = "Failed to wait Control Analysis  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ControlAnalysisAdd()))
        {
            error = "Failed to click Control Analysis Why  Button";
            return false;
        }

        // Control
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ControlDropDown()))
        {
            error = "Failed to wait for control drop down";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ControlDropDown()))
        {
            error = "Failed to click for control drop down";
            return false;
        }
         
          if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Control")))
        {
            error = "Failed to wait for control  option :" + getData("Control");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.controldropdownoption(getData("Control"))))
        {
            error = "Failed to wait for control  drop down option : " + getData("Control");
            return false;
        }
        
          if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.controldropdownoption(getData("Control"))))
        {
            error = "Failed to click control  drop down option : " + getData("Control");
            return false;
        }

        narrator.stepPassedWithScreenShot("Control" + getData("Control"));

        //Control analysis
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ControlAnalysis()))
        {
            error = "Failed to wait Control Analysis";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ControlAnalysis()))
        {
            error = "Failed to click Control Analysis";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Control analysis"))))
        {
            error = "Failed to wait for Control analysis option:" + getData("Control analysis");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Control analysis"))))
        {
            error = "Failed to click  Control analysis option:" + getData("Control analysis");
            return false;
        }

        narrator.stepPassedWithScreenShot("Control analysis : " + getData("Control analysis"));

        //How did the control perform?
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HowDidTheControlPerformTextArea()))
        {
            error = "Failed to wait for How did the control perform";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.HowDidTheControlPerformTextArea(), getData("How did the control perform")))
        {
            error = "Failed to enter How did the control perform";
            return false;
        }
        narrator.stepPassedWithScreenShot("How did the control perform :" + getData("How did the control perform"));

        //Why did the control fail Why was it absent
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyDidTheControlFailTextArea()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.WhyDidTheControlFailTextArea(), getData("Why did the control fail Why was it absent")))
            {
                error = "Failed to enter Why did the control fail Why was it absent";
                return false;
            }
            narrator.stepPassedWithScreenShot("Why did the control fail Why was it absent :" + getData("Why did the control fail Why was it absent"));
        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }

    public boolean WhyAnalysisActions()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyAnalysisButton()))
        {
            error = "Failed to wait for Why Analysis Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WhyAnalysisButton()))
        {
            error = "Failed to click Why Analysis Button";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyAnalysisSaveButton()))
        {
            error = "Failed to wait for Why Analysis Save Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Why Analysis Button");
        return true;
    }

}
