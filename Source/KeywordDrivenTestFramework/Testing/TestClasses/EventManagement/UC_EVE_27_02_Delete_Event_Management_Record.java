/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC EVE_27 02 Delete Event Management Record",
        createNewBrowserInstance = false
)
public class UC_EVE_27_02_Delete_Event_Management_Record extends BaseClass
{

    String error = "";

    public UC_EVE_27_02_Delete_Event_Management_Record()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteRecord())
        {
            return narrator.testFailed("Failed To Delete Record Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Deleted Record");
    }

    public boolean DeleteRecord() throws InterruptedException
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }
        pause(4000);

   

        return true;
    }

}
