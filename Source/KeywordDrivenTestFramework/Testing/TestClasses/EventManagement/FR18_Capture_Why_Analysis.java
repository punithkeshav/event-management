/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR18-Capture Why Analysis",
        createNewBrowserInstance = false
)
public class FR18_Capture_Why_Analysis extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR18_Capture_Why_Analysis()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Why Analysis Due To :" + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!WhyAnalysisActions())
            {
                return narrator.testFailed("Failed To Click Why Analysis Due To :" + error);
            }
        }

        return narrator.finalizeTest("Successfully Captured Why Analysis");
    }

    public boolean CaptureDetails()
    {

        //Event Investigation Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to wait for Event Investigation Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to click Event Investigation Tab ";
            return false;
        }

        //Event Investigation Detail Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationDetailTab()))
        {
            error = "Failed to wait for Investigation Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InvestigationDetailTab()))
        {
            error = "Failed to click Event Investigation Detail";
            return false;
        }

        narrator.stepPassedWithScreenShot("Event Investigation Detail");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.AnalysisToolDropDown()))
        {
            error = "Failed to wait for Analysis tool drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.AnalysisToolDropDown()))
        {
            error = "Failed to click Analysis tool drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Analysis tool drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }
        String option = getData("Analysis tool option");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin2(option)))
        {
            error = "Failed to wait for Analysis tool option drop down option : " + option;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin2(option)))
        {
            error = "Failed to click Analysis tool option drop down option : " + option;
            return false;
        }

        narrator.stepPassedWithScreenShot("Analysis tool option  :" + option);

        //Tool Panel
        String[] panel = option.split(" ");
        //Tool Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.AnalysisToolPanel(panel[0])))
        {
            error = "Failed to wait for Analysis tool panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.AnalysisToolPanel(panel[0])))
        {
            error = "Failed to click Analysis tool panel";
            return false;
        }

        narrator.stepPassedWithScreenShot("Analysis tool panel");

        //Add Button 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.AnalysisWhyAdd()))
        {
            error = "Failed to wait Analysis Why Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.AnalysisWhyAdd()))
        {
            error = "Failed to click Analysis Why Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyTextArea()))
        {
            error = "Failed to wait for Analysis Why";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.WhyTextArea(), getData("Why")))
        {
            error = "Failed to enter Analysis Why";
            return false;
        }
        narrator.stepPassedWithScreenShot("Why :" + getData("Why"));

        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyTitleTextArea()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.WhyTitleTextArea(), getData("Why")))
            {
                error = "Failed to enter Why title";
                return false;
            }
            narrator.stepPassedWithScreenShot("Why title :" + getData("Why"));
        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }

    public boolean WhyAnalysisActions()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyAnalysisButton()))
        {
            error = "Failed to wait for Why Analysis Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WhyAnalysisButton()))
        {
            error = "Failed to click Why Analysis Button";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhyAnalysisSaveButton()))
        {
            error = "Failed to wait for Why Analysis Save Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Why Analysis Button");
        return true;
    }

}
