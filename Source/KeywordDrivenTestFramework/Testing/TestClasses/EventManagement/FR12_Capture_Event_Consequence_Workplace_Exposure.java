/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR12- Capture Event Consequence – Workplace Exposure",
        createNewBrowserInstance = false
)

public class FR12_Capture_Event_Consequence_Workplace_Exposure extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR12_Capture_Event_Consequence_Workplace_Exposure()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequence Workplace ExposureDue To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Event Consequence Workplace Exposure");
    }

    public boolean EventConsequence()
    {

        // Exposure hazard	
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ExposureHazardDropDown()))
        {
            error = "Failed to wait for Exposure hazard Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ExposureHazardDropDown()))
        {
            error = "Failed to click Exposure hazard Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 1"))))
        {
            error = "Failed to wait for  Exposure hazard" + getData("Actual Consequenc 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 1"))))
        {
            error = "Failed to wait for Exposure hazard Option drop down";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 2"))))
        {
            error = "Failed to wait for  Exposure hazard" + getData("Actual Consequenc 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 2"))))
        {
            error = "Failed to wait for Exposure hazard Option drop down";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 3"))))
        {
            error = "Failed to wait for  Exposure hazard" + getData("Actual Consequenc 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Exposure hazard 3"))))
        {
            error = "Failed to wait for Exposure hazard Option drop down";
            return false;
        }
        
        
        
  
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Exposure hazard option"))))
        {
            error = "Failed to wait for  Exposure hazarddrop down option : " + getData("Exposure hazard option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Exposure hazard option"))))
        {
            error = "Failed to wait for  Exposure hazard drop down option : " + getData("Exposure hazard option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Exposure hazard option :" + getData("Exposure hazard option"));
        
        //SEG/HEG
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SEGDropDown()))
        {
            error = "Failed to wait fo SEGDropDown Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SEGDropDown()))
        {
            error = "Failed to click SEG Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("SEG/HEG"))))
        {
            error = "Failed to wait for SEG option:" + getData("SEG/HEG");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("SEG/HEG"))))
        {
            error = "Failed to click SEG/HEG option:" + getData("SEG/HEG");
            return false;
        }

        narrator.stepPassedWithScreenShot("SEG/HEG : " + getData("SEG/HEG"));

        //Monitoring
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.MonitoringDropDown()))
        {
            error = "Failed to wait fo Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.MonitoringDropDown()))
        {
            error = "Failed to click Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Monitoring"))))
        {
            error = "Failed to wait for Monitoring option:" + getData("Monitoring");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Monitoring"))))
        {
            error = "Failed to click Monitoring option:" + getData("Monitoring");
            return false;
        }

        narrator.stepPassedWithScreenShot("Monitoring : " + getData("Monitoring"));

        //OEL exceedance
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OELExceedanceDropDown()))
        {
            error = "Failed to wait fo OEL exceedance Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.OELExceedanceDropDown()))
        {
            error = "Failed to click OEL exceedance Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("OEL exceedance"))))
        {
            error = "Failed to wait for OEL exceedance option:" + getData("OEL exceedance");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("OEL exceedance"))))
        {
            error = "Failed to click  OEL exceedance  option:" + getData("OEL exceedance");
            return false;
        }

        narrator.stepPassedWithScreenShot("OEL exceedance : " + getData("OEL exceedance"));
        
        
         if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Illustration_Description()))
            {

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Illustration_Description(), getData("Description")))
                {
                    error = "Failed to enter Illustration Description :" + getData("Description");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventUploadImage()))
                {
                    error = "Failed to wait for Upload Image";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventUploadImage()))
                {
                    error = "Failed to click Upload Image";
                    return false;
                }
                pause(7000);

                String ImageUpload = System.getProperty("user.dir") + getData("Image Path");
                String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images\\" + EventManagemant_PageObjects.DVTPic();
                String path = new File(pathofImages).getAbsolutePath();
                System.out.println("path " + pathofImages);

                if (!sikuliDriverUtility.EnterText(EventManagemant_PageObjects.fileNamePic1(), path))
                {
                    error = "Failed to click image 1";
                    return false;
                }

                pause(3000);
                if (!SeleniumDriverInstance.pressEnter())
                {
                    error = "Failed to press enter";
                    return false;
                }
                pause(5000);
                narrator.stepPassedWithScreenShot("Image uploaded");         
            }

    

        if (getData("Save and close").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close_DropDown()))
            {
                error = "Failed to wait for drop down button save and close drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close_DropDown()))
            {
                error = "Failed to click button save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close()))
            {
                error = "Failed to wait for save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close()))
            {
                error = "Failed to click  save and close";
                return false;
            }

            narrator.stepPassedWithScreenShot("Clicked save and close");

            pause(5000);
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
            {
                error = "Failed to wait EventConsequence  Add Button";
                return false;
            }

        } else
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            //SeleniumDriverInstance.pause(4000);
            String saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            String record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");
        }

        return true;
    }

}
