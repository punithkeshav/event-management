/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC EVE 06 02 - Capture Files Main Scenario",
        createNewBrowserInstance = false
)
public class UC_EVE_06_02_CaptureFines extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public UC_EVE_06_02_CaptureFines()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Fines Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Fines");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesAdd()))
        {
            error = "Failed to wait Event Consequence Fines Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceFinesAdd()))
        {
            error = "Failed to click Event Consequence Fines Add Button";
            return false;
        }
        pause(4000);
        //Process Flow 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesProcessFlow()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesProcessFlow(), 6))
            {
                error = "Failed to wait Event Consequence Fines Process Flow";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceFinesProcessFlow()))
        {
            error = "Failed to click Event Consequence Fines Process Flow";
            return false;
        }

        //Date of fine
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesDateOfFine()))
        {
            error = "Failed to wait for Event Consequence Fines Date of fine";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventConsequenceFinesDateOfFine(), startDate))
        {
            error = "Failed to enter Event Consequence Fines Date of fine";
            return false;
        }
        //Fine description
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventConsequenceFineDescription(), getData("Fine description")))
        {
            error = "Failed to enter Event Consequence Fines Fine description :" + getData("Fine description");
            return false;
        }
        narrator.stepPassed("Fine description :" + getData("Fine description"));

        //Currency
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesCurrency()))
        {
            error = "Failed to wait Event Consequence Fines Currency Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceFinesCurrency()))
        {
            error = "Failed to click Event Consequence Fines Currency Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Currency"))))
        {
            error = "Failed to wait for Currency :" + getData("Currency");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Currency"))))
        {
            error = "Failed to wait for Currency drop down option";
            return false;
        }
        narrator.stepPassed("Currency :" + getData("Currency"));

        //Value of fine
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventConsequenceFinesValueOfFine(), getData("Value of fine")))
        {
            error = "Failed to enter Event Consequence Fines Value of fine :" + getData("Value of fine");
            return false;
        }
        narrator.stepPassed("Value of fine  :" + getData("Value of fine"));
        //Contested
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesContested()))
        {
            error = "Failed to wait Event Consequence Fines Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceFinesContested()))
        {
            error = "Failed to click Event Consequence Fines Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Contested"))))
        {
            error = "Failed to wait for Contested :" + getData("Contested");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Contested"))))
        {
            error = "Failed to wait for Contested drop down option";
            return false;
        }
        narrator.stepPassed("Contested :" + getData("Contested"));

        if (getData("Contested").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DateContested()))
            {
                error = "Failed to wait Date Contested";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DateContested(), startDate))
            {
                error = "Failed to enter Date Contested";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ContestedDetails(), getData("Fine Contested Details")))
            {
                error = "Failed to enter Fine Contested Details";
                return false;
            }
        }

        //Was fine paid
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasFinePaidDropDoww()))
        {
            error = "Failed to wait Event Consequence Fines Was fine paid drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasFinePaidDropDoww()))
        {
            error = "Failed to click Event Consequence Fines Was fine paid drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was fine paid"))))
        {
            error = "Failed to wait for Was fine paid :" + getData("Was fine paid");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was fine paid"))))
        {
            error = "Failed to wait for Was fine paid drop down option";
            return false;
        }
        narrator.stepPassed("Was fine paid :" + getData("Was fine paid"));

        if (getData("Was fine paid").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Date_Of_Fine_Paid()))
            {
                error = "Failed to wait Date of fine paid";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Date_Of_Fine_Paid(), startDate))
            {
                error = "Failed to enter Date of fine paid";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Date_Of_Fine_Paid_Value(), getData("Final value of fine paid")))
            {
                error = "Failed to enter fine date of fine paid value";
                return false;
            }
        }

        //Was the fine closed
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesWasTheFineClosed()))
        {
            error = "Failed to wait Event Consequence Fines Was the fine closed drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceFinesWasTheFineClosed()))
        {
            error = "Failed to click Event Consequence Fines Was the fine closed drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was the fine closed"))))
        {
            error = "Failed to wait for Was the fine closed :" + getData("Was the fine closed");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was the fine closed"))))
        {
            error = "Failed to wait for Contested drop down option";
            return false;
        }
        narrator.stepPassed("Was the fine closed :" + getData("Was the fine closed"));

        if (getData("Was the fine closed").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceFinesWasTheFineClosedDate()))
            {
                error = "Failed to wait Event Consequence Fines Was the fine closed drop down";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventConsequenceFinesWasTheFineClosedDate(), startDate))
            {
                error = "Failed to enter Fines Closed Date";
                return false;
            }
        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Fine_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Fine_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }
        
        pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup3()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup3());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }

}
