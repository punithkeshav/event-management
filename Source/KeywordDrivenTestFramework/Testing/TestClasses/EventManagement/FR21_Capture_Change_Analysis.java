/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR21-Capture Change Analysis",
        createNewBrowserInstance = false
)
public class FR21_Capture_Change_Analysis extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR21_Capture_Change_Analysis()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Change Analysis Due To :" + error);
        }
   
        return narrator.finalizeTest("Successfully Captured Change Analysis");
    }

    public boolean CaptureDetails()
    {

        //Add Button 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ChangeAnalysisAdd()))
        {
            error = "Failed to wait Change  Analysis  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ChangeAnalysisAdd()))
        {
            error = "Failed to click Change  Analysis Why  Button";
            return false;
        }

        // Normal practice
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.NormalPracticeTextArea()))
        {
            error = "Failed to wait for Normal practice";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.NormalPracticeTextArea(), getData("Normal practice")))
        {
            error = "Failed to enter Normal practice";
            return false;
        }
        narrator.stepPassedWithScreenShot("Normal practice" + getData("Normal practice"));

        //Situation / practice at time of event
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SituationPracticeAtTimeOfEventTextArea()))
        {
            error = "Failed to wait Situation / practice at time of event";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.SituationPracticeAtTimeOfEventTextArea(), getData("Situation practice at time of event")))
        {
            error = "Failed to enter Situation / practice at time of event";
            return false;
        }
        narrator.stepPassedWithScreenShot("Situation practice at time of event :" + getData("Situation practice at time of event"));

        // Gap
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.GapTextArea()))
        {
            error = "Failed to wait Gap";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.GapTextArea(), getData("Gap")))
        {
            error = "Failed to enter Gap";
            return false;
        }
        narrator.stepPassedWithScreenShot("Gap :" + getData("Gap"));

        //Impact of difference
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ImpactOfDifferenceArea()))
        {
            error = "Failed to wait Impact of difference";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ImpactOfDifferenceArea(), getData("Impact of difference")))
        {
            error = "Failed to enter Impact of difference";
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact of difference :" + getData("Impact of difference"));
        

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }
}
