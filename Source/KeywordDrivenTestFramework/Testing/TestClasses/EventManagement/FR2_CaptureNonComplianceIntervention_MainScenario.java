
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Non Compliance Intervention Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureNonComplianceIntervention_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR2_CaptureNonComplianceIntervention_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Intervention())
        {
            return narrator.testFailed("Failed To Capture Non-Compliance Intervention Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Non-Compliance Intervention");
    }

    public boolean Capture_An_Intervention()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerificationAdditionalTab()))
        {
            error = "Failed to wait for Verification & Additional Detail Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.VerificationAdditionalTab()))
        {
            error = "Failed to click Verification & Additional Detail Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Verification & Additional Detail Tab ");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhatHappenDropDown()))
        {
            error = "Failed to wait for what happen drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WhatHappenDropDown()))
        {
            error = "Failed to click what happen drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for what happen search text box";
            return false;
        }
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("What happend"))))
        {
            error = "Failed to wait for What happend option:" + getData("What happend");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("What happend"))))
        {
            error = "Failed to click What happend :" + getData("What happend");
            return false;
        }
        narrator.stepPassed("What happend :" + getData("What happend"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HowDidItHappenDropDown()))
        {
            error = "Failed to wait for How did it happen drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.HowDidItHappenDropDown()))
        {
            error = "Failed to click How did it happen drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for How did it happen search text box";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("How did it happen"))))
        {
            error = "Failed to wait for How did it happen option:" + getData("How did it happen");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("How did it happen"))))
        {
            error = "Failed to click How did it happen option :" + getData("How did it happen");
            return false;
        }
        narrator.stepPassed("How did it happen :" + getData("How did it happen"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.IssueNonComplianceInterventionTab()))
        {
            error = "Failed to wait for Issue Non-Compliance Intervention Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.IssueNonComplianceInterventionTab()))
        {
            error = "Failed to click Issue Non-Compliance Intervention Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Non-Compliance Intervention Tab ");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ButtonAdd()))
        {
            error = "Failed to wait for Issue Non-Compliance Intervention Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ButtonAdd()))
        {
            error = "Failed to click Issue Non-Compliance Intervention Add Button";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ProcessFlowButton2()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ProcessFlowButton2()))
        {
            error = "Failed to click the process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Non-Compliance Intervention");
        //Date of non-compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DateOfNonCompliance()))
        {
            error = "Failed to wait for Date of non-compliance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DateOfNonCompliance(), startDate))
        {
            error = "Failed to enter Date of non-compliance";
            return false;
        }
        //Reported date of non-compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReportedDateOfNonCompliance()))
        {
            error = "Failed to wait for Reported Date of non-compliance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ReportedDateOfNonCompliance(), startDate))
        {
            error = "Failed to enter Reported Date of non-compliance";
            return false;
        }

        //Reported by
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReportedByDropDown2()))
        {
            error = "Failed to wait for Reported bydrop down";
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ReportedByDropDown2()))
        {
            error = "Failed to click Reported by drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Reported bytext box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Reported by")))
        {
            error = "Failed to enter Reported by option :" + getData("Reported by");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to wait for Reported by drop down option : " + getData("Reported by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to click Reported by drop down option : " + getData("Reported by");
            return false;
        }

        narrator.stepPassed("Reported by :" + getData("Reported by"));
        

        //Person responsible for non-compliance intervention upliftment
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InterventionUpliftmentDropDown()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment drop down";
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InterventionUpliftmentDropDown()))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Person responsible for non-compliance intervention upliftment")))
        {
            error = "Failed to enter Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to wait for Reported Person responsible for non-compliance intervention upliftment down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }

        narrator.stepPassed("Person responsible for non-compliance intervention upliftment  :" + getData("Person responsible for non-compliance intervention upliftment"));

        
        //Non-compliance intervention issued to
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InterventionIssuedDropDown()))
        {
            error = "Failed to wait for Non-compliance intervention issued to drop down";
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InterventionIssuedDropDown()))
        {
            error = "Failed to click Non-compliance intervention issued to  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Non-compliance intervention issued to text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Non-compliance intervention issued to")))
        {
            error = "Failed to enter Non-compliance intervention issued to option :" + getData("Non-compliance intervention issued to");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to wait for Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to click Non-compliance intervention issued to  drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }

        narrator.stepPassed("Non-compliance intervention issued to  :" + getData("Non-compliance intervention issued to"));
        //Stoppage reference number
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageReferenceNumber()))
        {
            error = "Failed to wait for Stoppage reference number";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.StoppageReferenceNumber(), startDate))
        {
            error = "Failed to enter Stoppage reference number";
            return false;
        }

        //Type of non-compliance intervention
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeOfNonComplianceInterventionDropDown()))
        {
            error = "Failed to wait for Type of non-compliance intervention drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.TypeOfNonComplianceInterventionDropDown()))
        {
            error = "Failed to click Type of non-compliance intervention  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of non-compliance intervention text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to wait for Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to click  Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }

        narrator.stepPassed("Type of non-compliance intervention  :" + getData("Type of non-compliance intervention"));
        //Stoppage initiated by
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageInitiatedByDropDown()))
        {
            error = "Failed to wait for Stoppage initiated by drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StoppageInitiatedByDropDown()))
        {
            error = "Failed to click Stoppage initiated by  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stoppage initiated by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to wait for Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to click Stoppage initiated by  drop down option : " + getData("Stoppage initiated by");
            return false;
        }

        narrator.stepPassed("Stoppage initiated by  :" + getData("Stoppage initiated by"));
        //Clearance classification
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ClearanceClassificationdDropDown()))
        {
            error = "Failed to wait for Clearance classification drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ClearanceClassificationdDropDown()))
        {
            error = "Failed to click  Clearance classification drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Clearance classification text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Clearance classification"))))
        {
            error = "Failed to wait for Clearance classification drop down option : " + getData("Clearance classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Clearance classification"))))
        {
            error = "Failed to click Clearance classification  drop down option : " + getData("Clearance classification");
            return false;
        }

        narrator.stepPassed("Clearance classification  :" + getData("Clearance classification"));
        //Work stoppage
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to wait for Work stoppage drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to click Work stoppage drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Work stoppage text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to wait for Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to click  Work stoppage drop down option : " + getData("");
            return false;
        }

        narrator.stepPassed("Work stoppage  :" + getData("Work stoppage"));
        //Description of reason for work stoppage
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Description()))
        {
            error = "Failed to wait for Stoppage reference number";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Description(), getData("Description of reason for work stoppage")))
        {
            error = "Failed to enter Description of reason for work stoppage :" + getData("Description of reason for work stoppage");
            return false;
        }
        narrator.stepPassed("Description of reason for work stoppage :" + getData("Description of reason for work stoppage"));

        //Stop note classification
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StopClassificationDropDown()))
        {
            error = "Failed to wait for Stop note classification drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StopClassificationDropDown()))
        {
            error = "Failed to click Stop note classification  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stop note classification text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to wait for Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to click  Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }

        narrator.stepPassed("Stop note classification  :" + getData("Stop note classification"));
        //Stoppage due to
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to wait for Stoppage due to drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to click Stoppage due to  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stoppage due to text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageDueToDropDownCheckBox(getData("Stoppage due to"))))
        {
            error = "Failed to wait for Stoppage due to drop down option : " + getData("Stoppage due to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StoppageDueToDropDownCheckBox(getData("Stoppage due to"))))
        {
            error = "Failed to click Stoppage due to  drop down option : " + getData("Stoppage due to");
            return false;
        }

        narrator.stepPassed("Stoppage due to  :" + getData("Stoppage due to"));
        //Stoppage affected other areas
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageAffectedAreasDropDown()))
        {
            error = "Failed to wait for Stoppage affected other areas drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StoppageAffectedAreasDropDown()))
        {
            error = "Failed to click  Stoppage affected other areas drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stoppage affected other areas text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Stoppage affected other areas"))))
        {
            error = "Failed to wait for Stoppage affected other areas drop down option : " + getData("Stoppage affected other areas");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Stoppage affected other areas"))))
        {
            error = "Failed to click Stoppage affected other areas  drop down option : " + getData("Stoppage affected other areas");
            return false;
        }

        narrator.stepPassed("Stoppage affected other areas  :" + getData("Stoppage affected other areas"));
        //Stoppage outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to wait for Stoppage outcome drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to click Stoppage outcome  drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stoppage outcome text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to wait for Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to click Stoppage outcome  drop down option : " + getData("Stoppage outcome");
            return false;
        }

        narrator.stepPassed("Stoppage outcome :" + getData("Stoppage outcome"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        //SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
