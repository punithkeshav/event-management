/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC_EVE_01_02_Capture_A_Grievance",
        createNewBrowserInstance = false
)
public class UC_EVE_01_02_Capture_A_Grievance extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_EVE_01_02_Capture_A_Grievance()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureGrievance())
        {
            return narrator.testFailed("Failed To Capture A Grievance Due To : " + error);
        }
        return narrator.finalizeTest("Successfully Captured Grievance");
    }

    public boolean CaptureGrievance()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Verification_Additional_Detail_Tab()))
        {
            error = "Failed to wait for Verification & Additional Detail Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Verification_Additional_Detail_Tab()))
        {
            error = "Failed to click on Verification & Additional Detail Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully ");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhatHappenDropDown()))
        {
            error = "Failed to wait for what happen drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WhatHappenDropDown()))
        {
            error = "Failed to click what happen drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for what happen search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("What happend"))))
        {
            error = "Failed to click What happend :" + getData("What happend");
            return false;
        }
        narrator.stepPassed("What happend :" + getData("What happend"));

        //Activities
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActivitiesDropDown()))
        {
            error = "Failed to wait for Activities drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActivitiesDropDown()))
        {
            error = "Failed to click Activities drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Activities search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Activities"))))
        {
            error = "Failed to click Activities :" + getData("Activities");
            return false;
        }
        narrator.stepPassed("Activities :" + getData("Activities"));

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HowDidItHappenDropDown()))
        {
            error = "Failed to wait for How did it happen drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.HowDidItHappenDropDown()))
        {
            error = "Failed to click How did it happen drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for How did it happen search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("How did it happen"))))
        {
            error = "Failed to click How did it happen :" + getData("How did it happen");
            return false;
        }
        narrator.stepPassed("How did it happen :" + getData("How did it happen"));

        //Complaint anonymous
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ComplaintAnonymousDropDown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ComplaintAnonymousDropDown()))
            {
                error = "Failed to click Complaint anonymous drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Complaint anonymous search text box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Complaint anonymous"))))
            {
                error = "Failed to click Complaint anonymous :" + getData("Complaint anonymous");
                return false;
            }
            narrator.stepPassed("Complaint anonymous :" + getData("Complaint anonymous"));

        }

        if (getData("Complaint anonymous").equalsIgnoreCase("No"))
        {

            // Submitted by contractor/employee
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SubmittedByContractorDropDown()))
            {
                error = "Failed to wait for Submitted by contractor/employee drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SubmittedByContractorDropDown()))
            {
                error = "Failed to click Submitted by contractor/employee drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Submitted by contractor/employee search text box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Submitted by contractor/employee"))))
            {
                error = "Failed to click Submitted by contractor/employee :" + getData("Submitted by contractor/employee");
                return false;
            }
            narrator.stepPassed("Submitted by contractor/employee :" + getData("Submitted by contractor/employee"));

            //Submitted by
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SubmittedByDropDown()))
            {
                error = "Failed to wait for Submitted by drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SubmittedByDropDown()))
            {
                error = "Failed to click Submitted by drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Submitted by search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), "a"))
            {
                error = "Failed to wait for Submitted by option :" + getData("Submitted by");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Submitted by"))))
            {
                error = "Failed to wait for Submitted by drop down option : " + getData("Submitted by");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Submitted by"))))
            {
                error = "Failed to click Submitted by :" + getData("Submitted by");
                return false;
            }
            narrator.stepPassed("Submitted by :" + getData("Submitted by"));

            //Submitted via
//            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SubmittedViaDropDown()))
//            {
//                error = "Failed to wait for Submitted via drop down";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SubmittedViaDropDown()))
//            {
//                error = "Failed to click Submitted via drop down";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
//            {
//                error = "Failed to wait for Submitted via search text box";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Submitted via"))))
//            {
//                error = "Failed to click Submitted via :" + getData("Submitted via");
//                return false;
//            }
//            narrator.stepPassed("Submitted via :" + getData("Submitted via"));
            //Category of stakeholder involved
//            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.CategoryOfStakeholderInvolvedDropDown()))
//            {
//                error = "Failed to wait for Category of stakeholder involved drop down";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.CategoryOfStakeholderInvolvedDropDown()))
//            {
//                error = "Failed to click Category of stakeholder involved drop down";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
//            {
//                error = "Failed to wait for Category of stakeholder involved search text box";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Category of stakeholder involved")))
//            {
//                error = "Failed to enter Category of stakeholder in search text box :" + getData("Category of stakeholder involved");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Category of stakeholder involved"))))
//            {
//                error = "Failed to click Category of stakeholder involved :" + getData("Category of stakeholder involved");
//                return false;
//            }
//            narrator.stepPassed("Category of stakeholder involved :" + getData("Category of stakeholder involved"));
        }

        //Other submissions
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OtherSubmissions()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.OtherSubmissions(), getData("Other submissions")))
            {
                error = "Failed to enter Other submissions :" + getData("Other submissions");
                return false;
            }

            narrator.stepPassed("Other submissions :" + getData("Category of stakeholder involved"));
        }

        //Other stakeholders
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OtherStakeholders()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.OtherStakeholders(), getData("Other stakeholders")))
            {
                error = "Failed to enter Other stakeholders :" + getData("Other stakeholders");
                return false;
            }

            narrator.stepPassed("Other stakeholders :" + getData("Other stakeholders"));
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Captured a grievance");

        return true;
    }

}
