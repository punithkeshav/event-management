/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR13- Declare Event Ready for Investigation",
        createNewBrowserInstance = false
)
public class FR13_Declare_Event_Ready_for_Investigation extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR13_Declare_Event_Ready_for_Investigation()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Details Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Details");
    }

    public boolean CaptureDetails()
    {

        narrator.stepPassedWithScreenShot("");
        if (getData("Tick check box").equalsIgnoreCase("True"))
        {

            //check box
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.CheckBox()))
            {
                error = "Failed to wait for check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.CheckBox()))
            {
                error = "Failed to click check box";
                return false;
            }

            //Declaration comments
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DeclarationComments()))
            {
                error = "Failed to wait for Declaration comments text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DeclarationComments(), getData("Declaration comments")))
            {
                error = "Failed to enter Declaration comments :" + getData("Declaration comments");
                return false;
            }

            narrator.stepPassed("Declaration comments  :" + getData("Declaration comments"));

            //Lead investigator
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LeadInvestigator()))
            {
                error = "Failed to wait for Lead investigator drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.LeadInvestigator()))
            {
                error = "Failed to click  Lead investigator drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Type of event search text box";
                return false;
            }
            
              if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Lead investigator")))
            {
                error = "Failed to enter Lead investigator :" + getData("Lead investigator");
                return false;
            }
              
               if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Lead investigator"))))
            {
                error = "Failed to wait for Lead investigator  drop down option : " + getData("Lead investigator");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Lead investigator"))))
            {
                error = "Failed to click Lead investigator  drop down option : " + getData("Lead investigator");
                return false;
            }

            narrator.stepPassed("Lead investigator  :" + getData("Lead investigator"));

            //save
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            SeleniumDriverInstance.pause(4000);
            String saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            String record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");

        }

        //Lead investigator
        return true;
    }

}
