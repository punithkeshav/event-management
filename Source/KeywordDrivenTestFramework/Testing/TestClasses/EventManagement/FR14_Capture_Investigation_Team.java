/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR14-Capture Investigation Team",
        createNewBrowserInstance = false
)
public class FR14_Capture_Investigation_Team extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR14_Capture_Investigation_Team()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Investigation Team Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Investigation Team");
    }

    public boolean CaptureDetails()
    {

        //Event Investigation Tab

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to wait for Event Investigation Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to click Event Investigation Tab ";
            return false;
        }

        //Revised date
        if (getData("Revised date").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.RevisedDate()))
            {
                error = "Failed to wait for Revised date";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RevisedDate(), startDate))
            {
                error = "Failed to enter Revised date:" + startDate;
                return false;
            }

            narrator.stepPassedWithScreenShot("Revised date :" + startDate);

              if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.RevisedDate()))
        {
            error = "Failed to press enter";
            return false;
        }
            //Reason for revised date
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReasonForRevisedDate()))
            {
                error = "Failed to wait for Reason for revised date";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ReasonForRevisedDate(), getData("Reason for revised date")))
            {
                error = "Failed to enter Reason for revised date:" + getData("Reason for revised date");
                return false;
            }

            narrator.stepPassedWithScreenShot("Reason for revised date :" + getData("Reason for revised date"));

        }

        //Investigation Team panel
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationTeamDropDown()))
        {
            error = "Failed to wait for Investigation Team";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InvestigationTeamDropDown()))
        {
            error = "Failed to click Investigation Team";
            return false;
        }

        //Lead investigator
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationAddButton()))
        {
            error = "Failed to wait for Investigation Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InvestigationAddButton()))
        {
            error = "Failed to click  Investigation Add Button";
            return false;
        }

        //Full name
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InvestigationFullNameDropDown()))
        {
            error = "Failed to wait for Investigation Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InvestigationFullNameDropDown()))
        {
            error = "Failed to click  Investigation Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Full name")))
        {
            error = "Failed to enter Full name :" + getData("Full name");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to wait for Full name  drop down option : " + getData("Full name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to click Full name  drop down option : " + getData("Full name");
            return false;
        }

        narrator.stepPassedWithScreenShot("Full name  :" + getData("Full name"));

        //Experience/Role
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Experience_Role_DropDown()))
        {
            error = "Failed to wait for Experience Role Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Experience_Role_DropDown()))
        {
            error = "Failed to click Experience Role Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Experience/Role"))))
        {
            error = "Failed to wait for Experience/Role option : " + getData("Experience/Role");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Experience/Role"))))
        {
            error = "Failed to click Experience/Role drop down option : " + getData("Experience/Role");
            return false;
        }

        narrator.stepPassedWithScreenShot("Experience/Role  :" + getData("Experience/Role"));

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }

}
