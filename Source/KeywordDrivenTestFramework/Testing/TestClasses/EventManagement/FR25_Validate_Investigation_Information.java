/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR25 – Validate Investigation Information",
        createNewBrowserInstance = false
)
public class FR25_Validate_Investigation_Information extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR25_Validate_Investigation_Information()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Declare Event Ready for Summary and Approval Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Declare Event Ready for Summary and Approval");
    }

    public boolean CaptureDetails()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventValidationTab()))
        {
            error = "Failed to wait for Event Validation";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.EventValidationTab()))
        {
            error = "Failed to scroll to Event Validation";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventValidationTab()))
        {
            error = "Failed to click Event Validation";
            return false;
        }

        narrator.stepPassedWithScreenShot("Event Validation");

        //Validate
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ValidateDropDown()))
        {
            error = "Failed to wait for Event Validation Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ValidateDropDown()))
        {
            error = "Failed to click Event Validation Drop Down";
            return false;
        }

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Validate"))))
        {
            error = "Failed to wait for Validate option:" + getData("Validate");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Validate"))))
        {
            error = "Failed to click Validate option:" + getData("Validate");
            return false;
        }

        narrator.stepPassedWithScreenShot("Validate : " + getData("Validate"));

        if (getData("Validate").equalsIgnoreCase("No"))
        {

            //Reason for rejection
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReasonForRejectionTextArea()))
            {
                error = "Failed to wait Reason for rejection";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ReasonForRejectionTextArea(), getData("Reason for rejection")))
            {
                error = "Failed to enter Reason for rejection";
                return false;
            }
            narrator.stepPassedWithScreenShot("Reason for rejection :" + getData("Reason for rejection"));

        } else
        {
            //Validation comments
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ValidationCommentsTextArea()))
            {
                error = "Failed to wait Validation comments";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ValidationCommentsTextArea(), getData("Validation comments")))
            {
                error = "Failed to enter Validation comments";
                return false;
            }
            narrator.stepPassedWithScreenShot("Validation comments :" + getData("Validation comments"));

        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }
}
