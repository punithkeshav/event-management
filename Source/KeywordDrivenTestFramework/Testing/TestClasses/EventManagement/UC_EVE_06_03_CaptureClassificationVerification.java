/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC EVE 06 02 - Capture E-Classification Verification Main Scenario",
        createNewBrowserInstance = false
)

public class UC_EVE_06_03_CaptureClassificationVerification extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public UC_EVE_06_03_CaptureClassificationVerification()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture E-Classification Verification  Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture E-Classification Verification");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.VerifyConsequenceAdd()))
        {
            error = "Failed to scroll to Verify Consequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerifyConsequenceAdd()))
        {
            error = "Failed to wait Verify Consequence  Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerifyConsequenceAdd()))
        {
            error = "Failed to wait Verify Consequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.VerifyConsequenceAdd()))
        {
            error = "Failed to click Verify Consequence Add Button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerifyConsequenceProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.VerifyConsequenceProcessFlow()))
        {
            error = "Failed to click  process flow button";
            return false;
        }

        //Verify actual consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerifyActualConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Verify actual consequence Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.VerifyActualConsequenceTypeDropDown()))
        {
            error = "Failed to click Verify actual consequence Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Verify actual consequence 1"))))
        {
            error = "Failed to wait for Verify actual consequence :" + getData("Verify actual consequence 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Verify actual consequence 1"))))
        {
            error = "Failed to wait for Verify actual consequence Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Verify actual consequence 2"))))
        {
            error = "Failed to wait for Verify actual consequence Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Verify actual consequence 2"))))
        {
            error = "Failed to wait for Verify actual consequence Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Verify actual consequence option"))))
        {
            error = "Failed to wait for Verify actual consequence drop down option : " + getData("Verify actual consequence option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Verify actual consequence option"))))
        {
            error = "Failed to wait for Verify actual consequence drop down option : " + getData("Verify actual consequence option");
            return false;
        }

        narrator.stepPassed("Verify actual consequence :" + getData(" option"));

        //Potential consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.VerifyPotentialConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.VerifyPotentialConsequenceTypeDropDown()))
        {
            error = "Failed to click  Potential Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 1"))))
        {
            error = "Failed to wait for :" + getData("Potential Consequenc 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 1"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 2"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 2"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Potential Consequenc option"))))
        {
            error = "Failed to wait for Potential Consequenc drop down option : " + getData("Potential Consequenc option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Potential Consequenc option"))))
        {
            error = "Failed to wait for Potential Consequenc drop down option : " + getData("Potential Consequenc option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Potential Consequenc option :" + getData("Potential Consequenc option"));

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ReasonForUpdate(), getData("Reason for update")))
        {
            error = "Failed to enter '" + getData("Reason for update") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Reason for update : '" + getData("Reason for update") + "'.");

        //Add support document
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.uploadLinkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.uploadLinkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.iframeName()))
        {
            error = "Failed to locate to switch to frame.";
        }

        if (!SeleniumDriverInstance.swithToFrameByName(EventManagemant_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        narrator.stepPassedWithScreenShot("Documents uploaded");

        if (getData("Update Classifications").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.UpdateClassifications()))
            {
                error = "Failed to wait for Update classifications.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.UpdateClassifications()))
            {
                error = "Failed to click Update classifications.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Update classifications  clicked");

            pause(3000);
         if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasaFineIssuedDropDown()))
            {
                error = "Failed to wait for Was a fine issued  Drop Down";
                return false;
            }

        } else
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Verify_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Verify_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            String saved = "";

            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals(
                    "Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            String record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");
        }

        return true;
    }

}
