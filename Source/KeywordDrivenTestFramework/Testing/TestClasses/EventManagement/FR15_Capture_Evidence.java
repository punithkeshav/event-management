/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**Statement given by
Statement given by

 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR15-Capture Evidence",
        createNewBrowserInstance = false
)
public class FR15_Capture_Evidence extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR15_Capture_Evidence()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Evidence Team Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Evidence");
    }

    public boolean CaptureDetails()
    {

        //Event Investigation Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to wait for Event Investigation Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventInvestigationTab()))
        {
            error = "Failed to click Event Investigation Tab ";
            return false;
        }

        //Evidence panel
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceDropDown()))
        {
            error = "Failed to wait for Evidence";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EvidenceDropDown()))
        {
            error = "Failed to click Evidence";
            return false;
        }

        //Evidence Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceAddButton()))
        {
            error = "Failed to wait for Evidence Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EvidenceAddButton()))
        {
            error = "Failed to click  Evidence Add Button";
            return false;
        }

        pause(3000);
        //Process button 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceProcessButton()))
        {
            error = "Failed to wait for Process button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EvidenceProcessButton()))
        {
            error = "Failed to click  Process button";
            return false;
        }

        //Evidence name
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EvidenceName(), getData("Evidence name")))
        {
            error = "Failed to enter Evidence name :" + getData("Evidence name");
            return false;
        }

        narrator.stepPassed("Evidence name : " + getData("Evidence name"));
        //Date of evidence collection
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DateOfEvidenceCollection(), startDate))
        {
            error = "Failed to enter Date of evidence collection :" + startDate;
            return false;
        }

        //Evidence type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceTypeDropDown()))
        {
            error = "Failed to wait for Evidence type";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EvidenceTypeDropDown()))
        {
            error = "Failed to click  Evidence type";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Evidence type"))))
        {
            error = "Failed to wait for Evidence type drop down option : " + getData("Evidence type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Evidence type"))))
        {
            error = "Failed to click Evidence type drop down option : " + getData("Evidence type");
            return false;
        }

        narrator.stepPassedWithScreenShot("Evidence type  :" + getData("Evidence type"));

        if (getData("Evidence type").equalsIgnoreCase("Other"))
        {
            //Evidence description

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceDescription()))
            {
                error = "Failed to wait for Evidence description";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EvidenceDescription(), getData("Evidence description")))
            {
                error = "Failed to enter Evidence description :" + getData("Evidence description");
                return false;
            }

            narrator.stepPassed("Evidence name : " + getData("Evidence name"));

        }

        if (getData("Evidence type").equalsIgnoreCase("Statement"))
        {
            //Statement given by

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StatementGivenBy()))
            {
                error = "Failed to wait fo Statement given by type Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StatementGivenBy()))
            {
                error = "Failed to click Statement given by Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Statement given by text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Statement given by")))
            {
                error = "Failed to enter Statement given by option :" + getData("Statement given by");
                return false;
            }
          if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Statement given by"))))
            {
                error = "Failed to wait for Statement given by option:" + getData("Statement given by");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Statement given by"))))
            {
                error = "Failed to click Statement given by option:" + getData("Statement given by");
                return false;
            }
            narrator.stepPassedWithScreenShot("Statement given by :" + getData("Statement given by"));

            //Statement taken by
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StatementTakenBy()))
            {
                error = "Failed to wait fo Statement taken by type Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.StatementTakenBy()))
            {
                error = "Failed to click Statement taken by Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Statement taken by text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Statement taken by")))
            {
                error = "Failed to wait for Statement taken by option :" + getData("Statement taken by");
                return false;
            }
           if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Statement taken by"))))
            {
                error = "Failed to wait for Statement taken by option:" + getData("Statement taken by");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Statement taken by"))))
            {
                error = "Failed to click Statement taken by option:" + getData("Statement taken by");
                return false;
            }
            narrator.stepPassedWithScreenShot("Statement taken by :" + getData("Statement taken by"));
            //Where was statement taken?
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.StatementTaken()))
            {
                error = "Failed to wait for Where was statement taken";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.StatementTaken(), getData("Where was statement taken")))
            {
                error = "Failed to enter Where was statement taken :" + getData("Where was statement taken");
                return false;
            }

            narrator.stepPassed("Where was statement taken : " + getData("Where was statement taken"));

            //Type of statement
            
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeofStatement()))
            {
                error = "Failed to wait fo Statement taken by type Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.TypeofStatement()))
            {
                error = "Failed to click Statement taken by Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Type of statement"))))
            {
                error = "Failed to wait for Type of statement option:" + getData("Type of statement");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Type of statement"))))
            {
                error = "Failed to click Type of statement option:" + getData("Type of statement");
                return false;
            }
            narrator.stepPassedWithScreenShot("Type of statement :" + getData("Type of statement"));

        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EvidenceButton_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EvidenceButton_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        //SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        if (getData("Sensitive information").equalsIgnoreCase("true"))
        {
            //Sensitive information

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SensitiveInformation()))
            {
                error = "Failed to wait for Sensitive information";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SensitiveInformation()))
            {
                error = "Failed to click  Sensitive information";
                return false;
            }
            narrator.stepPassedWithScreenShot("Sensitive information");
        }

        return true;
    }

}
