/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR8- Capture Event Consequence – Material Losses Main Scenario",
        createNewBrowserInstance = false
)

public class FR8_Capture_Event_Consequence_Material_Losses extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR8_Capture_Event_Consequence_Material_Losses()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequence – Material Losses  Due To :" + error);
        }
        if (!Material_Losses())
        {
            return narrator.testFailed("Failed To Capture  Material Losses Due To :" + error);

        }

        return narrator.finalizeTest("Successfully Capture Event Consequence – Material Losses ");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to wait EventConsequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to click EventConsequence Add Button";
            return false;
        }

        pause(6000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Consequence type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to wait fo Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to click Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to wait for Consequenc type option:" + getData("Consequenc type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to click  Consequenc type option:" + getData("Consequenc type");
            return false;
        }

        //Potential consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to click  Potential Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Potential consequence"))))
        {
            error = "Failed to wait for Potential Consequenc:" + getData("Potential consequence");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Potential consequence"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Potential Consequenc option :" + getData("Potential consequence"));

        //Actual consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Actual Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to click Actual Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc"))))
        {
            error = "Failed to wait for Actual Consequenc :" + getData("Actual Consequenc");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc"))))
        {
            error = "Failed to wait for Actual Consequenc Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Actual consequence :" + getData("Actual Consequenc"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");
        return true;
    }

    public boolean Material_Losses()
    {
        //Property involved
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PropertyInvolvedDropDown()))
        {
            error = "Failed to wait for Property involved Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PropertyInvolvedDropDown()))
        {
            error = "Failed to click Property involved Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Property involved"))))
        {
            error = "Failed to wait for Property involved drop down option : " + getData("Property involved");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Property involved"))))
        {
            error = "Failed to click Property involved drop down option : " + getData("Property involved");
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SpecifyOtherPropertyInvolved()))
        {

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.SpecifyOtherPropertyInvolved(), getData("Specify other property involved")))
            {
                error = "Failed to  enter Specify other property involved : " + getData("Specify other property involved");
                return false;
            }

        }

        narrator.stepPassedWithScreenShot("Property involved  :" + getData("Property involved"));

        //Property owner
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PropertyOwnerDropDown()))
        {
            error = "Failed to wait for Property owner Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PropertyOwnerDropDown()))
        {
            error = "Failed to click Property owner Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Property owner"))))
        {
            error = "Failed to wait for Property owner drop down option : " + getData("Property owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Property owner"))))
        {
            error = "Failed to click Property owner drop down option : " + getData("Property owner");
            return false;
        }

        narrator.stepPassedWithScreenShot("Property owner  :" + getData("Property owner"));

        if (getData("Property owner").equalsIgnoreCase("Contract"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ContractorCompanyNameDropDown()))
            {
                error = "Failed to wait for Contractor company name Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ContractorCompanyNameDropDown()))
            {
                error = "Failed to click Contractor company name Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Type of event search text box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Contractor company name"))))
            {
                error = "Failed to click Contractor company name :" + getData("Contractor company name");
                return false;
            }
            narrator.stepPassedWithScreenShot("Contractor company name :" + getData("Contractor company name"));
        }

        //Shift duration
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ShiftDurationDropDown()))
        {
            error = "Failed to wait for Shift duration Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ShiftDurationDropDown()))
        {
            error = "Failed to click Shift duration Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Shift duration"))))
        {
            error = "Failed to wait for Shift duration drop down option : " + getData("Shift duration");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Shift duration"))))
        {
            error = "Failed to click Shift duration drop down option : " + getData("Shift duration");
            return false;
        }

        narrator.stepPassedWithScreenShot("Shift duration  :" + getData("Shift duration"));

        //Crew
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.CrewDropDown()))
        {
            error = "Failed to wait for Crew Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.CrewDropDown()))
        {
            error = "Failed to click Crew Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Crew"))))
        {
            error = "Failed to wait for Crew drop down option : " + getData("Crew");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Crew"))))
        {
            error = "Failed to click Crew  drop down option : " + getData("Crew");
            return false;
        }

        narrator.stepPassedWithScreenShot("Crew  :" + getData("Crew"));

        //Hours into shift
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HoursIntoShift()))
        {
            error = "Failed to wait for Hours into shift";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.HoursIntoShift(), getData("Hours into shift")))
        {
            error = "Failed to enter Hours into shift :" + getData("Hours into shift");
            return false;
        }

        narrator.stepPassed("Hours into shift  :" + getData("Hours into shift"));

        //Consecutive days worked
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsecutiveDaysWorked()))
        {
            error = "Failed to wait for Consecutive days worked";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ConsecutiveDaysWorked(), getData("Consecutive days worked")))
        {
            error = "Failed to enter Consecutive days worked";
            return false;
        }
        narrator.stepPassed("Consecutive days worked  :" + getData("Consecutive days worked"));
        narrator.stepPassedWithScreenShot("");

        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Illustration_Description()))
        {

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Illustration_Description(), getData("Description")))
            {
                error = "Failed to enter Illustration Description :" + getData("Description");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventUploadImage2()))
            {
                error = "Failed to wait for Upload Image";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventUploadImage2()))
            {
                error = "Failed to click Upload Image";
                return false;
            }
            pause(7000);

            String ImageUpload = System.getProperty("user.dir") + getData("Image Path");
            String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images\\" + EventManagemant_PageObjects.DVTPic();
            String path = new File(pathofImages).getAbsolutePath();
            System.out.println("path " + pathofImages);

            if (!sikuliDriverUtility.EnterText(EventManagemant_PageObjects.fileNamePic1(), path))
            {
                error = "Failed to click image 1";
                return false;
            }

            pause(3000);
            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
            pause(5000);
            narrator.stepPassedWithScreenShot("Image uploaded");
        }

        if (getData("Save and close").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close_DropDown()))
            {
                error = "Failed to click button save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close()))
            {
                error = "Failed to wait for save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save_And_Close()))
            {
                error = "Failed to click  save and close";
                return false;
            }

            narrator.stepPassedWithScreenShot("Clicked save and close");

            pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
            {
                error = "Failed to wait EventConsequence  Add Button";
                return false;
            }

        } else
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            //SeleniumDriverInstance.pause(4000);
            String saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            String record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");
        }
        return true;
    }

}
