/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC EVE 09-02 - Capture Event Consequence – Occupational Health Record Main Scenario",
        createNewBrowserInstance = false
)

public class UC_EVE_09_02_Capture_Event_Consequence_Occupational_Health_record extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public UC_EVE_09_02_Capture_Event_Consequence_Occupational_Health_record()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequence Occupational Health  Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Event Consequence Occupational Health  ");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OccupationalHealthAdd()))
        {
            error = "Failed to wait Occupational Health  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.OccupationalHealthAdd()))
        {
            error = "Failed to click Occupational Health Add Button";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OccupationalHealthProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.OccupationalHealthProcessFlow()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Full name
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OccupationalHealthFullNameDropDown()))
        {
            error = "Failed to wait fo Full name type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.OccupationalHealthFullNameDropDown()))
        {
            error = "Failed to click Full name type Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Full name")))
        {
            error = "Failed to wait for Full Name option :" + getData("Full name");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to wait for Full Name option:" + getData("Full name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to click Full Name option:" + getData("Full name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Full Name :" + getData("Full name"));

        //PIllness / disease type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.IllnessDiseaseTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.IllnessDiseaseTypeDropDown()))
        {
            error = "Failed to click  Potential Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Illness disease type"))))
        {
            error = "Failed to wait for Illness disease type :" + getData("Potential consequence");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Illness disease type"))))
        {
            error = "Failed to wait for Illness disease type Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Illness disease type :" + getData("Illness disease type"));

        //Illness description
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.IllnessDescription()))
        {
            error = "Failed to wait for Illness description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.IllnessDescription(), getData("Illness description")))
        {
            error = "Failed to enter Illness description";
            return false;
        }

        narrator.stepPassedWithScreenShot("Illness description :" + getData("Illness description"));

        //Was medical treatment provided
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasMedicalTreatmentProvidedDropDown()))
        {
            error = "Failed to wait for Was medical treatment provided Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasMedicalTreatmentProvidedDropDown()))
        {
            error = "Failed to click  Was medical treatment provided Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was medical treatment provided"))))
        {
            error = "Failed to wait for Illness disease type :" + getData("Was medical treatment provided");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was medical treatment provided"))))
        {
            error = "Failed to wait for Was medical treatment provided Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Was medical treatment provided :" + getData("Was medical treatment provided"));

        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TreatmentProvidedDescription_2()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TreatmentProvidedDescription_2(), getData("Treatment provided description")))
            {
                error = "Failed to enter Illness description";
                return false;
            }

            narrator.stepPassedWithScreenShot("Treatment provided description  :" + getData("Treatment provided description"));

        }

        //Hospitalized
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HospitalizedDropDown_2()))
        {
            error = "Failed to wait for Hospitalized Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.HospitalizedDropDown_2()))
        {
            error = "Failed to click Hospitalized Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Hospitalized"))))
        {
            error = "Failed to wait for Hospitalized option :" + getData("Hospitalized");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Hospitalized"))))
        {
            error = "Failed to wait for Hospitalized Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Hospitalized :" + getData("Hospitalized"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Occupational_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Occupational_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }

}
