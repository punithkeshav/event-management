/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR4-Capture Event Actions",
        createNewBrowserInstance = false
)
public class FR4_Capture_Event_Actions extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Event_Actions()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureActions())
        {
            return narrator.testFailed("Failed To Capture Actions Due To : " + error);
        }
        return narrator.finalizeTest("Successfully Captured Actions");
    }

    public boolean CaptureActions()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Action_Tab()))
        {
            error = "Failed to wait for Actions Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Action_Tab()))
        {
            error = "Failed to click on Actions Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked actions tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ButtonAddActions()))
        {
            error = "Failed to wait for Issue Non-Compliance Intervention Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ButtonAddActions()))
        {
            error = "Failed to click Issue Non-Compliance Intervention Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ProcessFlowActions()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ProcessFlowActions()))
        {
            error = "Failed to click the process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Actions");

        //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of action search text box";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for How did it happen option:" + getData("How did it happen");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Type of action option :" + getData("Type of action");
            return false;
        }
        narrator.stepPassed("Type of action :" + getData("Type of action"));
        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ActionDescription(), getData("Action description")))
        {
            error = "Failed to enter Action description";
            return false;
        }

        narrator.stepPassedWithScreenShot("Action description :" + getData("Action description"));

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EntityDropDown()))
        {
            error = "Failed to wait for Entity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EntityDropDown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Entity text box.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Business Unit")))
//        {
//            error = "Failed to wait for Entity option :" + getData("Business Unit");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Business unit"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Entity Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Entity Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption_1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption_1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption_1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption_1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click  Entity drop down option : " + getData("Business unit option");
            return false;
        }
        //Responsible person

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActionsResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActionsResponsiblePersonDropDown()))
        {
            error = "Failed to click Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));

        //Agency
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActionsAgencyDropDown()))
        {
            error = "Failed to wait for Agencydrop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActionsAgencyDropDown()))
        {
            error = "Failed to click Agency drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Agency search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Agency"))))
        {
            error = "Failed to click Agency :" + getData("Agency");
            return false;
        }
        narrator.stepPassed("Agency :" + getData("Agency"));
        //Action due date

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Action_due_date()))
        {
            error = "Failed to wait for Action due date text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Action_due_date(), startDate))
        {
            error = "Failed to enter Action due date :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" +startDate );
        
         if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActionsButton_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActionsButton_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Captured Actions");

        //Replicate this action to multiple users?
        return true;
    }
}
