/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR20-Capture Behaviour Analysis",
        createNewBrowserInstance = false
)
public class FR20_Capture_Behaviour_Analysis extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR20_Capture_Behaviour_Analysis()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureDetails())
        {
            return narrator.testFailed("Failed To Capture Behaviour Analysis Due To :" + error);
        }
   
        return narrator.finalizeTest("Successfully Captured Behaviour Analysis");
    }

    public boolean CaptureDetails()
    {

        //Add Button 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.BehaviourAnalysisAdd()))
        {
            error = "Failed to wait Behaviour  Analysis  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.BehaviourAnalysisAdd()))
        {
            error = "Failed to click Behaviour  Analysis Why  Button";
            return false;
        }

        // Activator
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActivatorTextArea()))
        {
            error = "Failed to wait for Activator";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ActivatorTextArea(), getData("Activator")))
        {
            error = "Failed to enter Activator";
            return false;
        }
        narrator.stepPassedWithScreenShot("Activator" + getData("Activator"));

        //Behaviour
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.BehaviourTextArea()))
        {
            error = "Failed to wait Behaviour";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.BehaviourTextArea(), getData("Behaviour")))
        {
            error = "Failed to enter Behaviour";
            return false;
        }
        narrator.stepPassedWithScreenShot("Behaviour" + getData("Behaviour"));

        // Consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsequenceTextArea()))
        {
            error = "Failed to wait Consequence";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ConsequenceTextArea(), getData("Consequence")))
        {
            error = "Failed to enter Consequence";
            return false;
        }
        narrator.stepPassedWithScreenShot("Consequence" + getData("Consequence"));

        //Behaviour classification
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.BehaviourClassificationDropDown()))
        {
            error = "Failed to wait Behaviour classification";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.BehaviourClassificationDropDown()))
        {
            error = "Failed to click Behaviour classification";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Behaviour classification")))
        {
            error = "Failed to enter Behaviour classification :" + getData("Behaviour classification");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Behaviour classification"))))
        {
            error = "Failed to wait for Immediate or Later option:" + getData("Behaviour classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Behaviour classification"))))
        {
            error = "Failed to click Behaviour classification option:" + getData("Behaviour classification");
            return false;
        }

        narrator.stepPassedWithScreenShot("Behaviour classification : " + getData("Behaviour classification"));

        //Immediate or Later
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ImmediateOrLaterDropDown()))
        {
            error = "Failed to wait Immediate or Later Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ImmediateOrLaterDropDown()))
        {
            error = "Failed to click Immediate or Later Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Immediate or Later"))))
        {
            error = "Failed to wait for Immediate or Later option:" + getData("Immediate or Later");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Immediate or Later"))))
        {
            error = "Failed to click Immediate or Later option:" + getData("Immediate or Later");
            return false;
        }

        narrator.stepPassedWithScreenShot("Immediate or Later : " + getData("Immediate or Later"));

        //Certain Or Uncertain
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.CertainOrUncertainDropDown()))
        {
            error = "Failed to wait Certain Or Uncertain Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.CertainOrUncertainDropDown()))
        {
            error = "Failed to click Certain Or Uncertain Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Certain Or Uncertain"))))
        {
            error = "Failed to wait for Certain Or Uncertain option:" + getData("Control analysis");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Certain Or Uncertain"))))
        {
            error = "Failed to click  Certain Or Uncertain option:" + getData("Certain Or Uncertain");
            return false;
        }

        narrator.stepPassedWithScreenShot("Certain Or Uncertain : " + getData("Certain Or Uncertain"));

        //Positive or Negative
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PositiveOrNegative()))
        {
            error = "Failed to wait Positive or Negative";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PositiveOrNegative()))
        {
            error = "Failed to click Positive or Negative";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Positive or Negative"))))
        {
            error = "Failed to wait for Positive or Negative option:" + getData("Positive or Negative");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Positive or Negative"))))
        {
            error = "Failed to click  Positive or Negative option:" + getData("Positive or Negative");
            return false;
        }

        narrator.stepPassedWithScreenShot("Positive or Negative : " + getData("Positive or Negative"));

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        return true;
    }
}
