/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR7 Capture Event Consequence – Legal and Regulatory Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_Capture_Event_Consequence_Legal_Regulatory extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_Capture_Event_Consequence_Legal_Regulatory()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequence – Legal and Regulatory Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Event Consequence – Legal and Regulatory");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to wait EventConsequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to click EventConsequence Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Consequence type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to wait fo Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to click Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to wait for Consequenc type option:" + getData("Consequenc type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to click  Consequenc type option:" + getData("Consequenc type");
            return false;
        }
   
        //Potential consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to click  Potential Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text5(getData("Potential consequence"))))
        {
            error = "Failed to wait for Potential Consequenc:" + getData("Potential consequence");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text5(getData("Potential consequence"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Potential Consequenc option :" + getData("Potential consequence"));
        
        
             //Actual consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Actual Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to click Actual Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text5(getData("Actual Consequenc"))))
        {
            error = "Failed to wait for Actual Consequenc :" + getData("Actual Consequenc");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text5(getData("Actual Consequenc"))))
        {
            error = "Failed to wait for Actual Consequenc Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Actual consequence :" + getData("Actual Consequenc"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        //Was a fine issued
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasaFineIssuedDropDown()))
        {
            error = "Failed to wait for Was a fine issued  Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasaFineIssuedDropDown()))
        {
            error = "Failed to click  Was a fine issued  Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was a fine issued"))))
        {
            error = "Failed to wait for Was a fine issued :" + getData("Was a fine issued");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was a fine issued"))))
        {
            error = "Failed to wait for  Was a fine issued  drop down";
            return false;
        }

        narrator.stepPassed("Was a fine issued :" + getData("Was a fine issued"));

        return true;
    }

}
