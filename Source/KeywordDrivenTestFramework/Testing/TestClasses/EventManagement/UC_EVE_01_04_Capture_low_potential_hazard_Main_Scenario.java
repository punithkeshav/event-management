/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "UC_EVE_01_04_Capture_low_potential_hazard Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class UC_EVE_01_04_Capture_low_potential_hazard_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public UC_EVE_01_04_Capture_low_potential_hazard_Main_Scenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Event Due To :" + error);
        }

        if (getData("Check declaration").equalsIgnoreCase("True"))
        {
            if (!Check_declaration())
            {
                return narrator.testFailed("Failed To Click Check Box Due To :" + error);
            }

        }

        if (getData("Save And Close").equalsIgnoreCase("True"))
        {
            if (!SaveAndClose())
            {
                return narrator.testFailed("Failed To Click Save And Close Due To :" + error);

            }
        }

        return narrator.finalizeTest("Successfully Captured An Event");
    }

    public boolean Capture_An_Event()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventManagemantTab()))
        {
            error = "Failed to wait for Event Managemant Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventManagemantTab()))
        {
            error = "Failed to click Event Managemant Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Event Managemant");

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.AddButton()))
        {
            error = "Failed to wait for the add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.AddButton()))
        {
            error = "Failed to click the add button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeofEventDropDown()))
        {
            error = "Failed to wait for Type of event down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.TypeofEventDropDown()))
        {
            error = "Failed to click Type of event drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Type of Event"))))
        {
            error = "Failed to click Type of event :" + getData("Type of Event");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of Event :" + getData("Type of Event"));

        //Event title 
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventTitle()))
        {
            error = "Failed to wait for  Event title text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventTitle(), getData("Event title")))
        {
            error = "Failed to enter Event title :" + getData("Event title");
            return false;
        }

        narrator.stepPassedWithScreenShot("Event title :" + getData("Event title"));

        //Event description
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventDescription()))
        {
            error = "Failed to wait for Event description text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EventDescription(), getData("Event description")))
        {
            error = "Failed to enter Event description :" + getData("Event description");
            return false;
        }

        narrator.stepPassed("Event description :" + getData("Event description"));

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Business Unit")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EntityexpandButton()))
//        {
//            error = "Failed to wait to expand Business Unit";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EntityexpandButton()))
//        {
//            error = "Failed to expand Entity";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));
        //Functional location
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to wait for Functional location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to click  Functional location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional location text box.";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Functional location  option")))
        {
            error = "Failed to enter Functional location option:" + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(7000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to wait for :" + getData("Functional location 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text(getData("Functional location  option"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Functional location  option"))))
        {
            error = "Failed to click  Functional location drop down option : " + getData("Functional location  option");
            return false;
        }

        //Specific location
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SpecificLocation()))
        {
            error = "Failed to wait for Specific location text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.SpecificLocation(), getData("Specific location")))
        {
            error = "Failed to wait for Specific location option :" + getData("Specific location");
            return false;
        }
        narrator.stepPassedWithScreenShot("Specific location :" + getData("Specific location"));

        //Pin to map
        if (getData("Pin To Map").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PinToMap()))
            {
                error = "Failed to wait for Pin to map";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PinToMap()))
            {
                error = "Failed to click Pin to map";
                return false;
            }

        }

        narrator.stepPassedWithScreenShot("Pin To Map");
        //Link to projects
        if (getData("Link to projects").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LinkToProjects()))
            {
                error = "Failed To Wait For Link To Projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.LinkToProjects()))
            {
                error = "Failed To Click On Link To Projects";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To Wait For Link To Projects Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To click Link To Projects Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text(getData("Project"))))
            {
                error = "Failed to wait for Project :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text(getData("Project"))))
            {
                error = "Failed to click Project :" + getData("Project");
                return false;
            }
            narrator.stepPassed("Project :" + getData("Project"));
        }
        narrator.stepPassedWithScreenShot("Link to projects");

        //Date of event
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DateOfEvent()))
        {
            error = "Failed to wait for Date of event";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DateOfEvent(), startDate))
        {
            error = "Failed to enter Date of event";
            return false;
        }

        //Date reported
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.DateReported()))
        {
            error = "Failed to wait for Date reported";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.DateReported(), startDate))
        {
            error = "Failed to enter Date reported";
            return false;
        }

        //Time of event
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TimeOfEvent()))
        {
            error = "Failed to wait for Time of event";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TimeOfEvent(), startDate))
        {
            error = "Failed to enter Time of event";
            return false;
        }

        //Time reported
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TimeReported()))
        {
            error = "Failed to wait for Time reported";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TimeReported(), startDate))
        {
            error = "Failed to enter Time reported";
            return false;
        }
        //Immediate action taken
        if (getData("Type of Event").equalsIgnoreCase("Grievance"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ImmediateActionTaken()))
            {
                error = "Failed to wait for Immediate action taken";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ImmediateActionTaken(), getData("Immediate action taken")))
            {
                error = "Failed to enter Immediate action taken";
                return false;
            }
            narrator.stepPassedWithScreenShot("Immediate action taken : " + getData("Immediate action taken"));

        }

        if (getData("Type of Event").equalsIgnoreCase("Hazard"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequencesDropDown()))
            {
                error = "Failed to wait for Potential consequences drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequencesDropDown()))
            {
                error = "Failed to clickPotential consequences drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Potential consequences text box.";
                return false;
            }

            pause(4000);
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin(getData("Potential consequences option"))))
            {
                error = "Failed to click Potential consequences drop down option :" + getData("Potential consequences option");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin(getData("Potential consequences option"))))
            {
                error = "Failed to click Potential consequences drop down option :" + getData("Potential consequences option");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin2(getData("Potential consequences level"))))
            {
                error = "Failed to click Potential consequences level drop down option :" + getData("Potential consequences level");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownOptin2(getData("Potential consequences level"))))
            {
                error = "Failed to click Potential consequences level drop down option :" + getData("Potential consequences level");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequencesDropDownClose()))
            {
                error = "Failed to close Potential consequences drop down";
                return false;
            }

            // What happened to cause the hazard (Agency)
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhatHappenedToCauseTheHazardDropDown()))
            {
                error = "Failed to wait for What happened to cause the hazard (Agency) drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WhatHappenedToCauseTheHazardDropDown()))
            {
                error = "Failed to click What happened to cause the hazard (Agency) drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for What happened to cause the hazard (Agency) text box.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("What happened to cause the hazard (Agency)"))))
            {
                error = "Failed to wait for What happened to cause the hazard (Agency) type :" + getData("What happened to cause the hazard (Agency)");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("What happened to cause the hazard (Agency)"))))
            {
                error = "Failed to click What happened to cause the hazard (Agency) type :" + getData("What happened to cause the hazard (Agency)");
                return false;
            }
            narrator.stepPassedWithScreenShot("What happened to cause the hazard (Agency) : " + getData("What happened to cause the hazard (Agency)"));

            // Hazard type
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HazardTypeDropDown()))
            {
                error = "Failed to wait for Hazard type drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.HazardTypeDropDown()))
            {
                error = "Failed to click Hazard type drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Hazard type text box.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Hazard type"))))
            {
                error = "Failed to wait for Hazard type :" + getData("Hazard type");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Hazard type"))))
            {
                error = "Failed to click Hazard type :" + getData("Hazard type");
                return false;
            }
            narrator.stepPassed("Hazard type : " + getData("Hazard type"));

            //Has the risk been controlled
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.RiskControlledDropDown()))
            {
                error = "Failed to wait for Has the risk been controlled drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.RiskControlledDropDown()))
            {
                error = "Failed to click Has the risk been controlled drop down";
                return false;
            }

            if (getData("Has the risk been controlled").equalsIgnoreCase("Yes"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Has the risk been controlled"))))
                {
                    error = "Failed to wait for Has the risk been controlled type :" + getData("Has the risk been controlled");
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Has the risk been controlled"))))
                {
                    error = "Failed to click Has the risk been controlled type :" + getData("Has the risk been controlled");
                    return false;
                }

                narrator.stepPassedWithScreenShot("Has the risk been controlled : " + getData("Has the risk been controlled"));

                //What was done to control the hazard
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WhatWasDoneToControlTheHazard()))
                {
                    error = "Failed to wait for Has the risk been controlled text box";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.WhatWasDoneToControlTheHazard(), getData("What was done to control the hazard")))
                {
                    error = "Failed to enter  What was done to control the hazard :" + getData("What was done to control the hazard");
                    return false;
                }
                narrator.stepPassedWithScreenShot("What was done to control the hazard : " + getData("What was done to control the hazard"));

            } else if (getData("Has the risk been controlled").equalsIgnoreCase("No"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Has the risk been controlled"))))
                {
                    error = "Failed to wait for Has the risk been controlled type :" + getData("Has the risk been controlled");
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Has the risk been controlled"))))
                {
                    error = "Failed to click Has the risk been controlled type :" + getData("Has the risk been controlled");
                    return false;
                }

                narrator.stepPassedWithScreenShot("Has the risk been controlled : " + getData("Has the risk been controlled"));

                //What was done in the interim
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InterimDropDown()))
                {
                    error = "Failed to wait for /What was done in the interimdrop down";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InterimDropDown()))
                {
                    error = "Failed to click /What was done in the interim drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("What was done in the interim"))))
                {
                    error = "Failed to wait for What was done in the interim option :" + getData("What was done in the interim");
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("What was done in the interim"))))
                {
                    error = "Failed to click What was done in the interim option :" + getData("What was done in the interim");
                    return false;
                }

                narrator.stepPassedWithScreenShot("What was done in the interim opyion : " + getData("What was done in the interim"));

                if (getData("What was done in the interim").equalsIgnoreCase("Other"))
                {
                    //Other description

                    if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OtherDescription()))
                    {
                        error = "Failed to wait for Other description :" + getData("Other description");
                        return false;
                    }

                    if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.OtherDescription(), getData("Other description")))
                    {
                        error = "Failed to enter Other description :" + getData("Other description");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Enter Other description :" + getData("Other description"));
                }
            }//end of if else
        }//end of if 

        //Reported by
        if (getData("Reported By Check").equalsIgnoreCase("True"))
        {
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReportedByCheckBox()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ReportedByCheckBox()))
                {
                    error = "Failed to click Reported by check box";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ReportedByDropDown()))
                {
                    error = "Failed to wait for Reported bydrop down";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ReportedByDropDown()))
                {
                    error = "Failed to click Reported by drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to wait for Reported bytext box.";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Reported by")))
                {
                    error = "Failed to enter Reported by option :" + getData("Responsible person");
                    return false;
                }
                if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to press enter";
                    return false;
                }
                pause(5000);
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Reported by"))))
                {
                    error = "Failed to wait for Reported by drop down option : " + getData("Reported by");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Reported by"))))
                {
                    error = "Failed to click Reported by drop down option : " + getData("Reported by");
                    return false;
                }

                narrator.stepPassedWithScreenShot("Reported by :" + getData("Reported by"));

            }

        }

        //Responsible supervisor
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ResponsibleSupervisorDropDown()))
        {
            error = "Failed to wait for Responsible supervisor drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ResponsibleSupervisorDropDown()))
        {
            error = "Failed to click Responsible supervisor drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible supervisor text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Responsible supervisor")))
        {
            error = "Failed to enter Responsible supervisor option :" + getData("Responsible supervisor");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Responsible supervisor"))))
        {
            error = "Failed to wait for Responsible supervisor drop down option : " + getData("Responsible supervisor");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Responsible supervisor"))))
        {
            error = "Failed to click Responsible supervisor drop down option : " + getData("Responsible supervisor");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible supervisor :" + getData("Responsible supervisor"));

        if (getData("Key person involved Check").equalsIgnoreCase("True"))
        {
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.KeyPersonInvolved()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.KeyPersonInvolved()))
                {
                    error = "Failed to click Key person involved check box";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.KeyPersonInvolvedDropDown()))
                {
                    error = "Failed to wait for Key person involved drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.KeyPersonInvolvedDropDown()))
                {
                    error = "Failed to click Key person involved drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to wait for Key person involved  text box.";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Key person involved option")))
                {
                    error = "Failed to enter Key person involved option :" + getData("Key person involved option");
                    return false;
                }
                if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to press enter";
                    return false;
                }
                pause(60000);
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Key person involved option"))))
                {
                    error = "Failed to wait for Key person involved : " + getData("Key person involved option") + " " + "After 60 seconds wait";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Key person involved option"))))
                {
                    error = "Failed to click Key person involved";
                    return false;
                }
            }

        }

        if (getData("External parties involved Check").equalsIgnoreCase("True"))
        {
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ExternalPartiesInvolved()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ExternalPartiesInvolved()))
                {
                    error = "Failed to click External parties involved check box";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ExternalPartiesInvolvedDropDown()))
                {
                    error = "Failed to wait for External parties involved drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ExternalPartiesInvolvedDropDown()))
                {
                    error = "Failed to click External parties involvedKey person involved drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to wait for External parties involved  text box.";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Key person involved option")))
                {
                    error = "Failed to enter External parties involved :" + getData("Key person involved option");
                    return false;
                }
                if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
                {
                    error = "Failed to press enter";
                    return false;
                }

                 pause(60000);
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Key person involved option"))))
                {
                    error = "Failed to wait for External parties involved : "+getData("Key person involved option")+ " " +"After 60 seconds wait";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Key person involved option"))))
                {
                    error = "Failed to click External parties involved";
                    return false;
                }

            }

        }
        if (getData("Was equipment involved Check").equalsIgnoreCase("True"))
        {
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasEquipmentInvolved()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasEquipmentInvolved()))
                {
                    error = "Failed to click Was equipment involved check box";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasEquipmentInvolvedDropDown()))
                {
                    error = "Failed to wait for Was Equipment Involved drop down";
                    return false;
                }

                pause(2000);
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasEquipmentInvolvedDropDown()))
                {
                    error = "Failed to click Was Equipment Involved drop down";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
                {
                    error = "Failed to wait for Was Equipment Involved text box";
                    return false;
                }
                pause(6000);

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch2(), getData("Was equipment involved option 1")))
                {
                    error = "Failed to enter  Team Name :" + getData("Was equipment involved option 1");
                    return false;
                }

                if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch2()))
                {
                    error = "Failed to press enter";
                    return false;
                }

                pause(2000);

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasEquipmentInvolvedOption(getData("Was equipment involved option 1"))))
                {
                    error = "Failed to wait for Was equipment involved option :" + getData("Was equipment involved option 1");
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasEquipmentInvolvedOption(getData("Was equipment involved option 1"))))
                {
                    error = "Failed to click Was equipment involved option  drop down :" + getData("Was equipment involved option 1");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasEquipmentInvolvedCheckBox(" | Do Not Delete")))
                {
                    error = "Failed to wait for Was equipment involved option :" + getData("Was equipment involved option 1");
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasEquipmentInvolvedCheckBox("| Do Not Delete")))
                {
                    error = "Failed to click Was equipment involved option  drop down :" + getData("Was equipment involved option 1");
                    return false;
                }

            }

        }

        narrator.stepPassedWithScreenShot("Check boxes");

        if (getData("Upload Image").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.UploadImage()))
            {
                error = "Failed to page down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.UploadImage1()))
            {
                error = "Failed to wait for Upload Image";
                return false;
            }

            if (!SeleniumDriverInstance.doubleClickElementbyXpath(EventManagemant_PageObjects.UploadImage1()))
            {
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(EventManagemant_PageObjects.UploadImage1()))
                {
                    error = "Failed to click Upload Image";
                    return false;
                }
            }
            pause(7000);

            String ImageUpload = System.getProperty("user.dir") + getData("Image Path");
            //Path path = Paths.get("C:","IsoMetrixModules","Event Management","images");

            String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images\\" + EventManagemant_PageObjects.DVTPic();

            String path = new File(pathofImages).getAbsolutePath();
            System.out.println("path " + pathofImages);

            if (!sikuliDriverUtility.EnterText(EventManagemant_PageObjects.fileNamePic1(), path))
            {
                error = "Failed to click image 1";
                return false;
            }

            pause(3000);
            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
            pause(5000);
            narrator.stepPassedWithScreenShot("Image uploaded");
        }

        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ValidatorDropDown()))
        {
            //Validator

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ValidatorDropDown()))
            {
                error = "Failed to click Validator drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Validator  text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Validator")))
            {
                error = "Failed to enter Validator  option :" + getData("Validator");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text3(getData("Validator"))))
            {
                error = "Failed to wait for Validator  drop down option : " + getData("Validator");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text3(getData("Validator"))))
            {
                error = "Failed to click Validator  drop down option : " + getData("Validator");
                return false;
            }

            narrator.stepPassed("Validator  :" + getData("Validator"));

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(5000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.", "");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved", acionRecord);

        return true;
    }

    public boolean Check_declaration()
    {
        //Check declaration
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Declaration_checkbox()))
        {
            error = "Failed to wait for declaration checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Declaration_checkbox()))
        {
            error = "Failed to click declaration checkbox";
            return false;
        }

        narrator.stepPassedWithScreenShot(endDate, date);
        narrator.stepPassedWithScreenShot("Successfully clicked declaration checkbox");
        pause(3000);

        return true;

    }

    public boolean SaveAndClose()
    {
        //Check declaration
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.SaveAndClose()))
        {
            error = "Failed to wait for Save and close";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.SaveAndClose()))
        {
            error = "Failed to click Save and close";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Save and close");
        pause(3000);

        return true;

    }
}
