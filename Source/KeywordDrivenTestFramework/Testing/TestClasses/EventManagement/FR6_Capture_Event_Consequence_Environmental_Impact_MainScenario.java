/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6 - Capture Event Consequence – Environmental Impact Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_Capture_Event_Consequence_Environmental_Impact_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR6_Capture_Event_Consequence_Environmental_Impact_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequenc Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Event Consequence – Safety Injury ");
    }

    public boolean EventConsequence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to wait EventConsequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to click EventConsequence Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Consequence type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to wait fo Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to click Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to wait for Consequenc type option:" + getData("Consequenc type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text5(getData("Consequenc type"))))
        {
            error = "Failed to click  Consequenc type option:" + getData("Consequenc type");
            return false;
        }

        //Actual consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Actual Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to click Actual Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Actual Consequenc 1"))))
        {
            error = "Failed to wait for Actual Consequenc :" + getData("Actual Consequenc 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Actual Consequenc 1"))))
        {
            error = "Failed to wait for Actual Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Actual Consequenc 2"))))
        {
            error = "Failed to wait for Actual Consequenct Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Actual Consequenc 2"))))
        {
            error = "Failed to wait for Actual Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc option"))))
        {
            error = "Failed to wait for Actual Consequenc drop down option : " + getData("Actual Consequenc option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc option"))))
        {
            error = "Failed to wait for Actual Consequenc drop down option : " + getData("Actual Consequenc option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Actual consequence :" + getData("Actual Consequenc option"));

        //Potential consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to click  Potential Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 1"))))
        {
            error = "Failed to wait for Potential Consequenc:" + getData("Potential Consequenc 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 1"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 2"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Potential Consequenc 2"))))
        {
            error = "Failed to wait for Potential Consequenc Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Potential Consequenc option"))))
        {
            error = "Failed to wait for Potential Consequenc drop down option : " + getData("Potential Consequenc option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Potential Consequenc option"))))
        {
            error = "Failed to wait for Potential Consequenc drop down option : " + getData("Potential Consequenc option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Potential Consequenc option :" + getData("Potential Consequenc option"));

        //Accountable person for verification of classification
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Accountable_Person_Verification_DropDown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Accountable_Person_Verification_DropDown()))
            {
                error = "Failed to click Accountable person for verification of classification drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Responsible supervisor text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), getData("Accountable person for verification of classification")))
            {
                error = "Failed to enter Accountable person for verification of classification option :" + getData("Accountable person for verification of classification");
                return false;
            }
           if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Accountable_person_for_verification(getData("Accountable person for verification of classification"))))
            {
                error = "Failed to wait for Accountable person for verification of classification drop down option : " + getData("Accountable person for verification of classification");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Accountable_person_for_verification(getData("Accountable person for verification of classification"))))
            {
                error = "Failed to click Accountable person for verification of classification drop down option : " + getData("Accountable person for verification of classification");
                return false;
            }

            narrator.stepPassedWithScreenShot("Accountable person for verification of classification :" + getData("Accountable person for verification of classification"));

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        if (getData("View Environmental Consequence").equalsIgnoreCase("True"))
        {
            //View Environmental Consequence Level
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.View_Environmental_Consequence()))
            {
                error = "Failed to wait for View Environmental Consequence Level";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.View_Environmental_Consequence()))
            {
                error = "Failed to click View Environmental Consequence Level";
                return false;
            }
            narrator.stepPassedWithScreenShot("View Environmental Consequence Level clicked");
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch to tab";
                return false;
            }
            pause(3000);
            narrator.stepPassedWithScreenShot("View Environmental Consequence Level clicked");

        }

        if (getData("Execute").equalsIgnoreCase("True"))
        {

            //Primary media impacted
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PrimaryMediaImpactedDropDown()))
            {
                error = "Failed to wait for Primary media impacted  Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PrimaryMediaImpactedDropDown()))
            {
                error = "Failed to click  Primary media impacted  Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Primary media impacted"))))
            {
                error = "Failed to wait for Primary media impacted :" + getData("Primary media impacted");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Primary media impacted"))))
            {
                error = "Failed to wait for  Primary media impacted  drop down option";
                return false;
            }

            narrator.stepPassedWithScreenShot("Primary media impacted :" + getData("Primary media impacted"));

            //Environmental Hazard(s)
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EnvironmentalHazardDropDown()))
            {
                error = "Failed to wait for  Environmental Hazard(s) Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EnvironmentalHazardDropDown()))
            {
                error = "Failed to click Environmental Hazard(s)  Drop Down";
                return false;
            }
            pause(3000);

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Was Environmental Hazard(s) text box";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EnvironmentalHazardSelectAll()))
            {
                error = "Failed to click Environmental Hazard(s)select all";
                return false;
            }

            //Radioactive
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.RadioactiveTypeDropDown()))
            {
                error = "Failed to wait for  Radioactive Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.RadioactiveTypeDropDown()))
            {
                error = "Failed to click Radioactive  Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Radioactive"))))
            {
                error = "Failed to wait for Radioactive:" + getData("Radioactive");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Radioactive"))))
            {
                error = "Failed to wait for  Radioactive  drop down";
                return false;
            }

            narrator.stepPassedWithScreenShot("Radioactive :" + getData("Radioactive"));

            //Was a fine issued
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasaFineIssuedDropDown()))
            {
                error = "Failed to wait for Was a fine issued  Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasaFineIssuedDropDown()))
            {
                error = "Failed to click  Was a fine issued  Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was a fine issued"))))
            {
                error = "Failed to wait for Was a fine issued :" + getData("Was a fine issued");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was a fine issued"))))
            {
                error = "Failed to wait for  Was a fine issued  drop down";
                return false;
            }

            narrator.stepPassedWithScreenShot("Was a fine issued :" + getData("Was a fine issued"));

            //Off-site impact
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.OffSiteImpactDropDown()))
            {
                error = "Failed to wait for Off-site impact  Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.OffSiteImpactDropDown()))
            {
                error = "Failed to click Off-site impact   Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Off-site impact"))))
            {
                error = "Failed to wait for Off-site impact :" + getData("Off-site impact");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Off-site impact"))))
            {
                error = "Failed to wait for  Off-site impact  drop down";
                return false;
            }

            narrator.stepPassedWithScreenShot("Off-site impact :" + getData("Off-site impact"));

            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Describe_scale_of_impact()))
            {

                //Describe scale of impact
                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Describe_scale_of_impact(), getData("Describe scale of impact")))
                {
                    error = "Failed to enter Describe scale of impact :" + getData("Describe scale of impact");
                    return false;
                }
                narrator.stepPassed("Describe scale of impact :" + getData("Describe scale of impact"));
                //Describe the severity of receiving environment
                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Describe_severity(), getData("Describe the severity of receiving environment")))
                {
                    error = "Failed to enter Describe the severity of receiving environment :" + getData("Describe the severity of receiving environment");
                    return false;
                }
                narrator.stepPassed("Describe the severity of receiving environment :" + getData("Describe the severity of receiving environment"));

            }
            ///////////////////////////////////////////////////////////

            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Illustration_Description()))
            {

                if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.Illustration_Description(), getData("Description")))
                {
                    error = "Failed to enter Illustration Description :" + getData("Description");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventUploadImage()))
                {
                    error = "Failed to wait for Upload Image";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventUploadImage()))
                {
                    error = "Failed to click Upload Image";
                    return false;
                }
                pause(7000);

                String ImageUpload = System.getProperty("user.dir") + getData("Image Path");
                String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images\\" + EventManagemant_PageObjects.DVTPic();
                String path = new File(pathofImages).getAbsolutePath();
                System.out.println("path " + pathofImages);

                if (!sikuliDriverUtility.EnterText(EventManagemant_PageObjects.fileNamePic1(), path))
                {
                    error = "Failed to click image 1";
                    return false;
                }

                pause(3000);
                if (!SeleniumDriverInstance.pressEnter())
                {
                    error = "Failed to press enter";
                    return false;
                }
                pause(5000);
                narrator.stepPassedWithScreenShot("Image uploaded");         
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");

        }

        return true;

    }
}
