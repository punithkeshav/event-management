/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EventManagement;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EventManagement.EventManagemant_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses.InjuredPersons;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5 Capture Event Consequence Safety Injury Main Scenario",
        createNewBrowserInstance = false
)
public class FR5_Capture_Event_Consequence_Safety_Injury extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR5_Capture_Event_Consequence_Safety_Injury()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!EventConsequence())
        {
            return narrator.testFailed("Failed To Capture Event Consequenc Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Event Consequence – Safety Injury ");
    }

    public boolean EventConsequence()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to wait EventConsequence  Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceAdd()))
        {
            error = "Failed to click EventConsequence Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.EventConsequenceProcessFlow()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Consequence type
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to wait fo Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ConsequenceTypeDropDown()))
        {
            error = "Failed to click Consequenc type Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Consequenc type"))))
        {
            error = "Failed to wait for Consequenc type option:" + getData("Consequenc type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Consequenc type"))))
        {
            error = "Failed to click  Consequenc type option:" + getData("Consequenc type");
            return false;
        }

        //Actual consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Actual Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ActualConsequenceTypeDropDown()))
        {
            error = "Failed to click Actual Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc"))))
        {
            error = "Failed to wait for Actual Consequenc  drop down option : " + getData("Actual Consequenc");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Actual Consequenc"))))
        {
            error = "Failed to click  Actual Consequenc  drop down option : " + getData("Actual Consequenc");
            return false;
        }
        narrator.stepPassedWithScreenShot("Actual consequence :" + getData("Actual Consequenc"));

        //Potential consequence
        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to wait for Potential Consequenc Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.PotentialConsequenceTypeDropDown()))
        {
            error = "Failed to click Capture Event Consequenc Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Potential consequence"))))
        {
            error = "Failed to wait for Potential consequence drop down option : " + getData("Potential consequence");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Potential consequence"))))
        {
            error = "Failed to click Potential consequence drop down option : " + getData("Potential consequence");
            return false;
        }
        narrator.stepPassedWithScreenShot("Potential consequence :" + getData("Potential consequence"));

        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        //SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EventManagemant_PageObjects.setRecord_Number(record[2]);
        String record_ = EventManagemant_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record Saved");

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            //Full name
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.FullNameDropDown()))
            {
                error = "Failed to wait for Full name Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.FullNameDropDown()))
            {
                error = "Failed to click Full name Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Full name text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TypeSearch(), "a"))
            {
                error = "Failed to wait for Full name text box.";
                return false;
            }
          if (!SeleniumDriverInstance.pressEnter_2(EventManagemant_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text3(getData("Full name"))))
            {
                error = "Failed to wait for Full name :" + getData("Full name");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text3(getData("Full name"))))
            {
                error = "Failed to click Full name  :" + getData("Full name");
                return false;
            }
            narrator.stepPassed("Full name : " + getData("Full name"));

            //Was treatment provided
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.WasTreatmentProvidedDropDown()))
            {
                error = "Failed to wait for Was treatment provided Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.WasTreatmentProvidedDropDown()))
            {
                error = "Failed to click Was treatment provided Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Was treatment provided"))))
            {
                error = "Failed to wait for Was treatment provided drop down option : " + getData("Was treatment provided");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Was treatment provided"))))
            {
                error = "Failed to click Was treatment provided drop down option : " + getData("Was treatment provided");
                return false;
            }

            narrator.stepPassed("Was treatment provided   :" + getData("Was treatment provided"));

            //Hospitalized
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HospitalizedDropDown()))
            {
                error = "Failed to wait for Hospitalized Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.HospitalizedDropDown()))
            {
                error = "Failed to click Hospitalized Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Hospitalized"))))
            {
                error = "Failed to wait for Hospitalized drop down option : " + getData("Hospitalized");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Hospitalized"))))
            {
                error = "Failed to click Hospitalized drop down option : " + getData("Hospitalized");
                return false;
            }
            narrator.stepPassedWithScreenShot("Hospitalized   :" + getData("Hospitalized"));

            //Treatment provided description
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.TreatmentProvidedDescription()))
            {
                error = "Failed to wait for Treatment provided description";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.TreatmentProvidedDescription(), getData("Treatment provided description")))
            {
                error = "Failed to enter Treatment provided description";
                return false;
            }
            narrator.stepPassed("Treatment provided description   :" + getData("Treatment provided description"));

            //Shift duration
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ShiftDurationDropDown()))
            {
                error = "Failed to wait for Shift duration Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.ShiftDurationDropDown()))
            {
                error = "Failed to click Shift duration Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Shift duration"))))
            {
                error = "Failed to wait for Shift duration drop down option : " + getData("Shift duration");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Shift duration"))))
            {
                error = "Failed to click Shift duration drop down option : " + getData("Shift duration");
                return false;
            }

            narrator.stepPassedWithScreenShot("Shift duration  :" + getData("Shift duration"));

            //Crew
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.CrewDropDown()))
            {
                error = "Failed to wait for Crew Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.CrewDropDown()))
            {
                error = "Failed to click Crew Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Crew"))))
            {
                error = "Failed to wait for Crew drop down option : " + getData("Crew");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Crew"))))
            {
                error = "Failed to click Crew  drop down option : " + getData("Crew");
                return false;
            }

            narrator.stepPassedWithScreenShot("Crew  :" + getData("Crew"));

            //Hours into shift
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.HoursIntoShift()))
            {
                error = "Failed to wait for Hours into shift";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.HoursIntoShift(), getData("Hours into shift")))
            {
                error = "Failed to enter Hours into shift :" + getData("Hours into shift");
                return false;
            }

            narrator.stepPassedWithScreenShot("Hours into shift  :" + getData("Hours into shift"));


            //Consecutive days worked
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.ConsecutiveDaysWorked()))
            {
                error = "Failed to wait for Consecutive days worked";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.ConsecutiveDaysWorked(), getData("Consecutive days worked")))
            {
                error = "Failed to enter Consecutive days worked";
                return false;
            }
            narrator.stepPassed("Consecutive days worked  :" + getData("Consecutive days worked"));

            // Injury description
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InjuryDescription()))
            {
                error = "Failed to wait for Injury description Drop Down";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.InjuryDescription(), getData("Injury description")))
            {
                error = "Failed to enter Injury description";
                return false;
            }

            narrator.stepPassedWithScreenShot("Injury description  :" + getData("Injury description"));

            if (getData("Injury").equalsIgnoreCase("True"))
            {

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InjuryAddButton()))
                {
                    error = "Failed to wait for Injury add button";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InjuryAddButton()))
                {
                    error = "Failed to click Injury add button";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.InjuryBodyPart()))
                {
                    error = "Failed to wait for Injury Body Part";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.InjuryBodyPart()))
                {
                    error = "Failed to click Injury Body Part";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Injury Body Part"))))
                {
                    error = "Failed to wait for Injury Body Part drop down option : " + getData("Injury Body Part");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Injury Body Part"))))
                {
                    error = "Failed to click Injury Body Part  drop down option : " + getData("Injury Body Part");
                    return false;
                }

                narrator.stepPassedWithScreenShot("Injury Body Part  :" + getData("Injury Body Part"));
                
                
             

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.NatureOfInjuryDropDown()))
                {
                    error = "Failed to wait for Nature of injury";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.NatureOfInjuryDropDown()))
                {
                    error = "Failed to click Nature of injury";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Text2(getData("Nature of injury"))))
                {
                    error = "Failed to wait for Nature of injury drop down option : " + getData("Nature of injury");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Text2(getData("Nature of injury"))))
                {
                    error = "Failed to click Nature of injury  drop down option : " + getData("Nature of injury");
                    return false;
                }

                narrator.stepPassedWithScreenShot("Nature of injury  :" + getData("Nature of injury"));

            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            //SeleniumDriverInstance.pause(4000);
            saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");

        }//end of execute

        if (getData("Optional Scenario").equalsIgnoreCase("True"))
        {
            //Estimated days lost from
            if (!SeleniumDriverInstance.scrollToElement(EventManagemant_PageObjects.EstimatedDaysLostFrom()))
            {
                error = "Failed to wait for Estimated days lost from";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.EstimatedDaysLostFrom()))
            {
                error = "Failed to wait for Estimated days lost from";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedDaysLostFrom(), startDate))
            {
                error = "Failed to enter Estimated days lost from";
                return false;
            }

            //Real days lost from
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealDaysLostFrom(), startDate))
            {
                error = "Failed to enter Real days lost from";
                return false;
            }
            //Estimated restricted days lost from
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedRestrictedDaysLostFrom(), startDate))
            {
                error = "Failed to enter Estimated restricted days lost from";
                return false;
            }
            //Real restricted days lost 
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealRestrictedDaysLost(), startDate))
            {
                error = "Failed to enter Real restricted days lost ";
                return false;
            }
            //Estimated days lost
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedDaysLost(), startDate))
            {
                error = "Failed to enter Estimated days lost";
                return false;
            }
            //Estimated restricted days lost
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedRestrictedDaysLost(), startDate))
            {
                error = "Failed to enter Estimated restricted days lost";
                return false;
            }

            //Estimated days lost to
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedDaysLostTo(), startDate))
            {
                error = "Failed to enter Estimated days lost to";
                return false;
            }
            //Real days lost to
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealDaysLostTo(), startDate))
            {
                error = "Failed to enter Real days lost to";
                return false;
            }

            //Estimated restricted days lost to
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.EstimatedRestrictedDaysLostTo(), startDate))
            {
                error = "Failed to enter Estimated restricted days lost to";
                return false;
            }

            //Real restricted days lost to
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealRestrictedDaysLostTo(), startDate))
            {
                error = "Failed to enter Real restricted days lost to";
                return false;
            }

            //Real days lost to
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealDaysLost(), startDate))
            {
                error = "Failed to enter Real days lost to";
                return false;
            }
            //Real restricted days lost
            if (!SeleniumDriverInstance.enterTextByXpath(EventManagemant_PageObjects.RealRestrictedDaysLostTo(), startDate))
            {
                error = "Failed to enter Estimated restricted days lost to";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.Consequenc_Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            //SeleniumDriverInstance.pause(4000);
            saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EventManagemant_PageObjects.getActionRecord());
            record = acionRecord.split(" ");
            EventManagemant_PageObjects.setRecord_Number(record[2]);
            record_ = EventManagemant_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record Saved");

        }

        return true;
    }
}
