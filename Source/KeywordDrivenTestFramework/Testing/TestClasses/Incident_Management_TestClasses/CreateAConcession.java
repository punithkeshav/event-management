/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add a Concessions",
        createNewBrowserInstance = false
)

public class CreateAConcession extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber2;
    String concessionRecordNumber;
    String concessionActionRecordNumber;

    public CreateAConcession()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        recordNumber2 = "";
    }

    public TestResult executeTest()
    {
        if (!CheckConcessionEndDate())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Completed Capturing Concessions automatically go overdue");
    }

    public boolean CheckConcessionEndDate()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
        {
            error = "Failed to wait for Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
        {
            error = "Failed to click Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
        {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
        {
            error = "Failed to click Quality concession expand button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Add_Buttonxpath()))
        {
            error = "Failed to wait for concession Add button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Add_Buttonxpath()))
        {
            error = "Failed to click concession Add button.";
            return false;
        }

        /*
         handle the save mask
        
         */
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Description_TextAreaxpath()))
        {
            error = "Failed to wait for concession Description Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_Description_TextAreaxpath(), testData.getData("Description")))
        {
            error = "Failed to EnterText into concession Description Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Impact_TextAreaxpath()))
        {
            error = "Failed to wait for concession Impact Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_Impact_TextAreaxpath(), testData.getData("Impact")))
        {
            error = "Failed to EnterText into concession Impact Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath()))
        {
            error = "Failed to wait for All Parties Accfected CheckBox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath()))
        {
            error = "Failed to click All Parties Accfected CheckBox.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_CorrectiveAction_TextAreaxpath()))
        {
            error = "Failed to wait for concession Corrective Action Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_CorrectiveAction_TextAreaxpath(), testData.getData("Action")))
        {
            error = "Failed to EnterText into concession Corrective Action Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath()))
        {
            error = "Failed to wait for Responsible Person Select dropdawon list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath()))
        {
            error = "Failed to click for Responsible Person Select dropdawon list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_TypeSearchxpath(), testData.getData("ResponsiblePerson")))
//        {
//            error = "Failed to EnterText into Responsible Person type search.";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.responsiblePerson(testData.getData("ResponsiblePerson"))))
        {
            error = "Failed to click Responsible Person: " + testData.getData("ResponsiblePerson");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_StartDate_InputAreaxpath()))
        {
            error = "Failed to wait for concession Start date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_StartDate_InputAreaxpath(), testData.getData("ConcessionStartDate")))
        {
            error = "Failed to EnterText into concession Start date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_EndDate_InputAreaxpath()))
        {
            error = "Failed to wait for concession End date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_EndDate_InputAreaxpath(), testData.getData("ConcessionEndDate")))
        {
            error = "Failed to EnterText into concession End date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Priority_SelectFieldxpath()))
        {
            error = "Failed to wait for Priority Select dropdown list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Priority_SelectFieldxpath()))
        {
            error = "Failed to click Priority Select dropdown list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.responsiblePerson(), testData.getData("Priority")))
//        {
//            error = "Failed to EnterText into Priority type search.";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.responsiblePerson(testData.getData("Priority"))))
        {
            error = "Failed to click Priority: " + testData.getData("ResponsiblePerson");
            return false;
        }

        pause(300);
        narrator.stepPassedWithScreenShot("Successfully Entered Concession Details.");

        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.chevron_Down_ButtonXPath()))
        {
            error = "Failed to wait for the Save & Close button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.chevron_Down_ButtonXPath()))
        {
            error = "Failed to click on the Save & Close button.";
            return false;
        }
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.save_And_Close_XPath()))
        {
            error = "Failed to wait for Save and Close button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.save_And_Close_XPath()))
        {
            error = "Failed to click the Save and Close button.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait2(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        return true;
    }

}
