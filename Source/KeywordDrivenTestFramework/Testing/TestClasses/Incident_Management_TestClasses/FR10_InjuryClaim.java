/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Injury claim",
        createNewBrowserInstance = false
)

public class FR10_InjuryClaim extends BaseClass {

    String error = "";
    String date;

    public FR10_InjuryClaim() {

        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!injuryClaim()) {
            return narrator.testFailed("Failed fill Injured Persons - Injury claim " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Injury claim ");
    }

    public boolean injuryClaim() {
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuredPersonsSaveXpath())) {
            error = "Failed to wait for injury Claim save ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuredPersonsSaveXpath())) {
            error = "Failed to click injury Claim save ";
            return false;
        }
       
        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveActiveInjuredPersonXpath(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveInjuredPersonXpath(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryClaimTab())) {
            error = "Failed to wait for injury Claim Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryClaimTab())) {
            error = "Failed to click injury Claim Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryClaimAdd())) {
            error = "Failed to wait for injury Claim add ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.injuryClaimAdd())) {
            error = "Failed to click injury Claim add ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        String unit = getData("Business unit");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(unit))) {
            error = "Failed to wait for business unit - " + unit;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(unit))) {
            error = "Failed to click business unit - " + unit;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.specificLoca())) {
            error = "Failed to enter injury Claim - Specific location ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.specificLoca(), getData("Specific location"))) {
            error = "Failed to enter injury Claim - Specific location ";
            return false;
        }

       
        return true;
    }

}
