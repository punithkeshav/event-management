/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "View Incident Action Summary",
        createNewBrowserInstance = false
)
public class FR29_View_Incident_Action_Summary extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR29_View_Incident_Action_Summary() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!ViewIncidentActionSummary()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Viewing Incident Action Summary - Main Scenario");
    }

    public boolean ViewIncidentActionSummary() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
            error = "Failed to wait for 4. Incident Sign Off Tab..";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to wait for 4. Incident Sign Off Tab..";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to click 4. Incident Sign Off Tab.";
                return false;
            }
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummary_Panelxpath())) {
            error = "Failed to wait for Action Summary Panel arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummary_Panelxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummary_Panelxpath())) {
                error = "Failed to wait for Action Summary Panel arrow.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummary_Panelxpath())) {
                error = "Failed to click Action Summary Panel arrow.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Action Records Available.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummaryRecord_xpath())) {
            error = "Failed to wait for Action Summary Record.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummaryRecord_xpath())) {
            error = "Failed to click Action Summary Record.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_Process_buttonxpath())) {
            error = "Failed to wait for Process button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.IncidentSignOff_Process_buttonxpath())) {
            error = "Failed to click Process button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummaryRecordNumber_xpath())) {
            error = "Failed to wait for Incident Record number.";
            return false;
        }

        String retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.IncidentSignOff_ActionSummaryRecordNumber_xpath());
        narrator.stepPassed("Incident Action opened: " + retrieveMessage);
        pause(3000);

        return true;
    }

}
