/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Equipment and Tools Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Equipment_And_Tools_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Equipment_And_Tools_AlternateScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        
        if (!equipment())
        {
            return narrator.testFailed("Failed to - " + error);
        }

        return narrator.finalizeTest("Successfully captured the details for Equipment and Tools");
    }

    
    public boolean equipment()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath()))
        {
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath()))
        {
            error = "Failed to click Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentAddXpath()))
        {
            error = "Failed to click Equipment and Tools Add button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        SeleniumDriverInstance.pause(15000);
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetTypeDropDownXpath()))
        {
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(getData("Asset Type"))))
        {
            error = "Failed to click Equipment and Tools Asset Type option - " + getData("Asset Type");
            return false;
        }

        String[] asset = getData("Asset").split(",");
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetDropDownXpath()))
        {
            error = "Failed to click asset dropdown";
            return false;
        }

        for (int k = 0; k < asset.length; k++)
        {

            if (k + 1 == asset.length)
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionXpath(asset[k])))
                {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("SuccessFully asset : " + asset[k]);
            } else
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveDropdownXpath(asset[k])))
                {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("Successfully selected asset type : " + asset[k]);
            }
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.equipmentDescXpath(), getData("Equipment description")))
        {
            error = "Failed to enter Equipment and Tools - Equipment description - " + getData("Equipment description");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.descrOfDamageXpath(), getData("Description of damage")))
        {
            error = "Failed to enter Equipment and Tools - Description of damage - " + getData("Description of damage");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.damageCostXpath(), getData("Damage cost")))
        {
            error = "Failed to enter Equipment and Tools - Damage cost - " + getData("Damage cost");
            return false;
        }
        SeleniumDriverInstance.pause(500);
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.equipmentDescXpath()))
        {
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorEmTypeDropdownpath()))
        {
            error = "Failed to click Equipment and Tools - Operator employment type ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator employment type"))))
        {
            error = "Failed to select Equipment and Tools - Operator employment type - " + getData("Operator employment type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorDropdownpath()))
        {
            error = "Failed to click Equipment and Tools - Operator  ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator"))))
        {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator"))))
        {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.motorVehiTabXpath()))
        {
            error = "Failed to click Equipment and Tools - Motor Vehicle Accident Information tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the Motor Vechile Accident Information panel is not displayed");

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveEquipToolsXpath()))
        {
            error = "Failed to click  Equipment and Tools  save button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Equipment and Tools details");

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        SeleniumDriverInstance.pause(2000);
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
//        {
//            error = "Failed to click on the Save And Continue button";
//            return false;
//        } else
//        {
//            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());
//
//            if (text.equals(" "))
//            {
//                error = "String cannot be empty";
//                return false;
//            } else if (text.equals(testData.getData("Record Saved")))
//            {
//                narrator.stepPassedWithScreenShot("Successfully saved the Equipment & Tools record: " + text);
//            }
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.closeEquipToolXpath()))
        {
            error = "Failed to click  Equipment and Tools  close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByJavascript(VerificationAndAdditionalPageObject.closeEquipToolXpath()))
        {
            error = "Failed to click  Equipment and Tools  close button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20))
        {
            error = "Failed to click save Equipment and Tools details";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20))
        {
            error = "Failed to save Equipment and Tools details";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath()))
        {
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath()))
        {
            error = "Failed to click Equipment and Tools tab ";
            return false;
        }
        return true;
    }
}
