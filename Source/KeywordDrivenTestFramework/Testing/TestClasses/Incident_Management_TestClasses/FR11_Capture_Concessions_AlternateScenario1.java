/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Capture Concessions Alternate Scenario 1",
        createNewBrowserInstance = false
)
public class FR11_Capture_Concessions_AlternateScenario1 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    String recordNumber2;
    String concessionRecordNumber;
    String concessionActionRecordNumber;

    public FR11_Capture_Concessions_AlternateScenario1() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {

        if (!CaptureConcessions()) {
            return narrator.testFailed("Failed due - " + error);
        }

        if (!AddConcessionsActions()) {
            return narrator.testFailed("Failed due - " + error);
        }

        if (!ValidateConcessionsActions()) {
            return narrator.testFailed("Failed due - " + error);
        }

        if (!ValidateConcessions()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Capturing Concessions Alternate Scenario 1");
    }

    public boolean CaptureConcessions() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to wait for Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to click Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to click Quality concession expand button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Add_Buttonxpath())) {
            error = "Failed to wait for concession Add button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Add_Buttonxpath())) {
            error = "Failed to click concession Add button.";
            return false;
        }

        /*
         handle the save mask
        
         */
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Description_TextAreaxpath())) {
            error = "Failed to wait for concession Description Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_Description_TextAreaxpath(), testData.getData("Description"))) {
            error = "Failed to EnterText into concession Description Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Impact_TextAreaxpath())) {
            error = "Failed to wait for concession Impact Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_Impact_TextAreaxpath(), testData.getData("Impact"))) {
            error = "Failed to EnterText into concession Impact Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath())) {
            error = "Failed to wait for All Parties Accfected CheckBox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath())) {
            error = "Failed to click All Parties Accfected CheckBox.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_CorrectiveAction_TextAreaxpath())) {
            error = "Failed to wait for concession Corrective Action Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_CorrectiveAction_TextAreaxpath(), testData.getData("Action"))) {
            error = "Failed to EnterText into concession Corrective Action Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to wait for Responsible Person Select dropdawon list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to click for Responsible Person Select dropdawon list.";
            return false;
        }
//        
//        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_TypeSearchxpath())) {
//            error = "Failed to wait for Responsible Person type search.";
//            return false;
//        }

//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_TypeSearchxpath(), testData.getData("ResponsiblePerson"))) {
//            error = "Failed to EnterText into Responsible Person type search.";
//            return false;
//        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_xpath(testData.getData("ResponsiblePerson")))) {
            error = "Failed to click Responsible Person: " + testData.getData("ResponsiblePerson");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_ResponsiblePerson_xpath(testData.getData("ResponsiblePerson")))) {
            error = "Failed to click Responsible Person: " + testData.getData("ResponsiblePerson");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_StartDate_InputAreaxpath())) {
            error = "Failed to wait for concession Start date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_StartDate_InputAreaxpath(), testData.getData("ConcessionStartDate"))) {
            error = "Failed to EnterText into concession Start date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_EndDate_InputAreaxpath())) {
            error = "Failed to wait for concession End date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_EndDate_InputAreaxpath(), testData.getData("ConcessionEndDate"))) {
            error = "Failed to EnterText into concession End date Input Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Priority_SelectFieldxpath())) {
            error = "Failed to wait for Priority Select dropdown list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Priority_SelectFieldxpath())) {
            error = "Failed to click Priority Select dropdown list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessions_Priority_TypeSearchxpath(), testData.getData("Priority"))) {
//            error = "Failed to EnterText into Priority type search.";
//            return false;
//        }
//        pause(300);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Priority_xpath(testData.getData("Priority")))) {
            error = "Failed to wait for Priority: " + testData.getData("Priority");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Priority_xpath(testData.getData("Priority")))) {
            error = "Failed to click Priority: " + testData.getData("Priority");
            return false;
        }

        pause(300);
        narrator.stepPassedWithScreenShot("Successfully Entered Concession Details.");
        narrator.stepPassed("All affected parties agreed to the proposed corrective action and the duration of this concession is not ticked");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_SaveToContinue_Buttonxpath())) {
            error = "Failed to wait for Save To Continue Button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_SaveToContinue_Buttonxpath())) {
            error = "Failed to click Save To Continue Button.";
            return false;
        }

        pause(5000);
        String retrieveRecordNo = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.retrieveConcessionRecordNumberXPath());
        narrator.stepPassed("Successfully retrieved and stored Concession: " + retrieveRecordNo);
        recordNumber2 = retrieveRecordNo.substring(10);

        String retrieveConcessionStatus = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.Concession_Status_XPath());
        narrator.stepPassed("Successfully retrieved and stored Concession Status: " + retrieveConcessionStatus);
        if (!retrieveConcessionStatus.equalsIgnoreCase("Logged")) {
            error = "Failed to Validate Concession Status is Logged.";
        } else {
            narrator.stepPassedWithScreenShot("Successfully saved Concession status: " + retrieveConcessionStatus);
        }

        return true;
    }

    private boolean AddConcessionsActions() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_Add_Buttonxpath())) {
            error = "Failed to wait for Concession Action ADD Button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_Add_Buttonxpath())) {
            error = "Failed to click Concession Action ADD  Button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_Description_Textfieldxpath())) {
            error = "Failed to wait for Concession Action ADD Button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessionsAction_Description_Textfieldxpath(), testData.getData("ActionDescription"))) {
            error = "Failed to Enter Text onto Concession Action Description text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_DepartmentResponsible_Buttonxpath())) {
            error = "Failed to wait for Concession Action Department Responsible select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_DepartmentResponsible_Buttonxpath())) {
            error = "Failed to click Concession Action Department Responsible select field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_DepartmentResponsible_xpath(testData.getData("ActionDepartmentResponsible")))) {
            error = "Failed to wait for Concession Action Department Responsible: " + testData.getData("ActionDepartmentResponsible");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_DepartmentResponsible_xpath(testData.getData("ActionDepartmentResponsible")))) {
            error = "Failed to click Concession Action Department Responsible: " + testData.getData("ActionDepartmentResponsible");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to wait for Concession Action Responsible Person Select dropdown list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to click Concession Action Responsible Person Select dropdown list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessionsAction_ResponsiblePerson_TypeSearchxpath(), testData.getData("ActionResponsiblePerson"))) {
//            error = "Failed to EnterText into Concession Action Responsible Person type search.";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_ResponsiblePerson_xpath(testData.getData("ActionResponsiblePerson")))) {
            error = "Failed to wait for Concession Action Responsible Person: " + testData.getData("ActionResponsiblePerson");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_ResponsiblePerson_xpath(testData.getData("ActionResponsiblePerson")))) {
            error = "Failed to click Concession Action Responsible Person: " + testData.getData("ActionResponsiblePerson");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_ActionDueDate_Textfieldxpath())) {
            error = "Failed to wait for Concession Action Due Date Text Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.concessionsAction_ActionDueDate_Textfieldxpath(), startDate)) {
            error = "Failed to Enter Text onto Concession Action Due Date text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_Save_Buttonxpath())) {
            error = "Failed to wait for Concession Action Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_Save_Buttonxpath())) {
            error = "Failed to click on Concession Action Save button";
            return false;
        }
        pause(5000);

        String retrieveconcessionRecordNumber = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.retrievedConcessionActionRecordNumberXPath());
        narrator.stepPassed("Successfully retrieved and stored Concession Actions: " + retrieveconcessionRecordNumber);
        concessionActionRecordNumber = retrieveconcessionRecordNumber.substring(10);

        narrator.stepPassedWithScreenShot("Successfully captured Concession Action details.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_Save_DropDownxpath())) {
            error = "Failed to wait for Concession Action Save And Continue dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_Save_DropDownxpath())) {
            error = "Failed to click on Concession Action Save And Continue dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_SaveAndClose_Buttonxpath())) {
            error = "Failed to wait for Concession Action Save And Close button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_SaveAndClose_Buttonxpath())) {
            error = "Failed to click on Concession Action Save And Close button";
            return false;
        }
        pause(5000);

        return true;
    }

    private boolean ValidateConcessionsActions() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_RefreshRecords_Buttonxpath())) {
            error = "Failed to wait for Concession Action refresh button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_RefreshRecords_Buttonxpath())) {

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_RefreshRecords_Buttonxpath())) {
                error = "Failed to wait for Concession Action refresh button";
                return false;
            }
            pause(8000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_RefreshRecords_Buttonxpath())) {
                error = "Failed to click on Concession Action refresh button";
                return false;
            }
        }

        pause(8000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_OpenSsavedConcessionRecord_SelectField(concessionActionRecordNumber))) {
            error = "Failed to wait for Concession Action Record: #" + concessionActionRecordNumber;
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_OpenSsavedConcessionRecord_SelectField(concessionActionRecordNumber))) {
            error = "Failed to click on Concession Action Record: #" + concessionActionRecordNumber;
            return false;
        }

        pause(5000);
        narrator.stepPassedWithScreenShot("Sucessfully Validated Concession Action Record: #" + concessionActionRecordNumber);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessionsAction_Save_DropDownxpath())) {
            error = "Failed to wait for Concession Action Save dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_Save_DropDownxpath())) {
            error = "Failed to click Concession Action Save dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessionsAction_SaveAndClose_Buttonxpath())) {
            error = "Failed to click on Concession Action Save And Close button";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_Save_DropDownMorexpath())) {
            error = "Failed to wait for Concession Action Save dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_Save_DropDownMorexpath())) {
            error = "Failed to click Concession Action Save dropdown button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_SaveAndClose_Buttonxpath())) {
            error = "Failed to click Concession Action Save and close button";
            return false;
        }
        pause(8000);

        return true;
    }

    private boolean ValidateConcessions() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_VerifAndAddiDetails_Tabxpath())) {
            error = "Failed to wait for Verification and Additional Details Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_VerifAndAddiDetails_Tabxpath())) {
            error = "Failed to click Verification and Additional Details Tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to wait for Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to click Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to click Quality concession expand button.";
            return false;
        }

        pause(6000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_OpenRecord_Selectxpath(recordNumber2))) {
            error = "Failed to wait for Concession Record: #" + recordNumber2;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_OpenRecord_Selectxpath(recordNumber2))) {
            error = "Failed to click Concession Record: #" + recordNumber2;
            return false;
        }

        pause(15000);

        return true;
    }
}
