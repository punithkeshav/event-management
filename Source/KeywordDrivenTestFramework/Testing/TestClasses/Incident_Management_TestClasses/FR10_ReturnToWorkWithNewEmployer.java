/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Return to Work with new employer",
        createNewBrowserInstance = false
)

public class FR10_ReturnToWorkWithNewEmployer extends BaseClass {

    String error = "";

    public FR10_ReturnToWorkWithNewEmployer() {

    }

    public TestResult executeTest() {
        if (!returnToWork()) {
            return narrator.testFailed("Failed fill Have you returned to work with a new employer " + error);
        }

        return narrator.finalizeTest("Completed Have you returned to work with a new employer ");
    }

    public boolean returnToWork() {

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnWorkDetailsOpenTab(), 2)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
                error = "Failed to click Treatment and Return To Work Details   Tab ";
                return false;
            }
        }

        String newEmployes = getData("Return to work new Employer");

        if (newEmployes.equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnNewEmployersdropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to work with a new employer? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(newEmployes))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to work with a new employer? " + newEmployes;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.newEmployerNameXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - Please provide the name of the new employer  ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.newEmployerContactXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - Please provide the contact number of the new employer  ";
                return false;
            }

           

            narrator.stepPassedWithScreenShot("Successfully filled - Have you returned to work with a new employer? and fields are displayed is displayed.");

        } else if (newEmployes.equalsIgnoreCase("No")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnNewEmployersdropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to work with a new employer? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(newEmployes))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to work with a new employer? " + newEmployes;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.newEmployerNameXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - Please provide the name of the new employer  ";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.newEmployerContactXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - Please provide the contact number of the new employer  ";
                return false;
            }

            
            narrator.stepPassedWithScreenShot("Successfully filled T Have you returned to work with a new employer? no fields are triggered.");
        }

        return true;
    }

}
