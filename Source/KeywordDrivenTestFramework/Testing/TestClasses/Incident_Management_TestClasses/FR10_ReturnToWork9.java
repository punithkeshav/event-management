/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Treatment and Return to Work Details 9",
        createNewBrowserInstance = false
)

public class FR10_ReturnToWork9 extends BaseClass {

    String error = "";
    String date;

    public FR10_ReturnToWork9() {
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!returnToWork()) {
            return narrator.testFailed("Failed fill have you returned to your same employer " + error);
        }

        return narrator.finalizeTest("Completed have you returned to your same employer ");
    }

    public boolean returnToWork() {
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnWorkDetailsOpenTab(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
                error = "Failed to wait for Treatment and Return To Work Details   Tab ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
                error = "Failed to click Treatment and Return To Work Details   Tab ";
                return false;
            }
        }

        if(getData("Return to work").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.doctorTreatmentXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Who is your nominated treating doctor?";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.doctorTreatmentXpath(), getData("nominated treating doctor"))) {
                error = "Failed to enter - Treatment and Return To Work Details - Who is your nominated treating doctor?";
                return false;
            }
            
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.numOfTreatmentXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Number of your nominated treating doctor   ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.numOfTreatmentXpath(), getData("Number of nominated treating doctor"))) {
                error = "Failed to enter - Treatment and Return To Work Details - Number of your nominated treating doctor   ";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.nameOfClinicXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details -  name (including Clinics or Hospitals) that have treated your injury";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameOfClinicXpath(), getData("Place Treated Injury"))) {
            error = "Failed to enter - Treatment and Return To Work Details -  name (including Clinics or Hospitals) that have treated your injury";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.contactDetailsXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - contact details ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.contactDetailsXpath(), getData("Contact Details"))) {
            error = "Failed to enter - Treatment and Return To Work Details - contact details ";
            return false;
        }
        

        String additionalDetails = getData("Return to work");
        if (additionalDetails.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.sameEmployerDropdownXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.sameEmployerDropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + additionalDetails;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnDateXpath(), 2)) {
                error = "Failed to wait for - Treatment and Return To Work Details - What was the date? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.returnDateXpath(), date)) {
                error = "Failed to enter - Treatment and Return To Work Details - What was the date? ";
                return false;
            }
            
            String time = new SimpleDateFormat("HH:mm").format(new Date());
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.hourWorked())) {
                error = "Failed to wait for - Treatment and Return To Work Details - How many hours are you working? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.hourWorked(), time)) {
                error = "Failed to enter - Treatment and Return To Work Details - How many hours are you working? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.dutiesdropdownXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - What duties are you doing? ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.dutiesdropdownXpath())) {
                error = "Failed to click - Treatment and Return To Work Details - What duties are you doing? dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionXpath(getData("duties")))) {
                error = "Failed to wait for - Treatment and Return To Work Details - What duties are you doing?";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionXpath(getData("duties")))) {
                error = "Failed to click - Treatment and Return To Work Details - What duties are you doing?";
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.returnDateXpath())) {
                error = "Failed to diplay - Treatment and Return To Work Details - What was the date?";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled  Treatment and Return To Work Details and What duties are you doing? and How many hours are you working? fields are displayed is displayed.");

        } else if (additionalDetails.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.sameEmployerDropdownXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.sameEmployerDropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(additionalDetails))) {
                error = "Failed to wait for Treatment and Return To Work Details - Have you returned to your same employer " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(additionalDetails))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + additionalDetails;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnDateXpath(), 2)) {
                error = "Display - Treatment and Return To Work Details - What was the date? ";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.dutiesdropdownXpath(), 2)) {
                error = "Display - Treatment and Return To Work Details - What duties are you doing? ";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnDateXpath(), 2)) {
                error = "Diplay - Treatment and Return To Work Details - What was the date?";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Treatment and Return To Work Details no fields are triggered.");
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.returnClaimsDateXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - When did / will you give your employer this claim form? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.returnClaimsDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer this claim form? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.DateOfMedCertDateXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - When did / will you give your employer the first medical certificate? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.DateOfMedCertDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer the first medical certificate? ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.formToYouremployerddropdownXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.formToYouremployerddropdownXpath())) {
            error = "Failed to enter - Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.dutiesOption(getData("Delivery")))) {
            error = "Failed to wait for Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown" + getData("Delivery");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.dutiesOption(getData("Delivery")))) {
            error = "Failed to click Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown" + getData("Delivery");
            return false;
        }

        return true;
    }

}
