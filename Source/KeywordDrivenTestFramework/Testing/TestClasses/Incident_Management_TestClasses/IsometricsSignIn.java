/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import org.openqa.selenium.By;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To Isometrix",
        createNewBrowserInstance = true
)
public class IsometricsSignIn extends BaseClass {

    String error = "";

    public IsometricsSignIn()
    {
        

    }

    public TestResult executeTest() {

        if (!NavigateToIsometricsSignInPage()) {
            return narrator.testFailed("Failed to navigate to Isometrix Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToIsomoterics()) {
            return narrator.testFailed("Failed to sign into the Isometrix Home Page :"+error);
        }
        
        if (!IsoVersion()) {
            return narrator.testFailed("Failed to check IsoMetriX version.:"+error);
        }

        if (!IsomotericsCheckSolutionBranch()) {
            return narrator.testFailed("Failed to check IsoMetriX Solution Branch.:"+error);
        }
        
        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page :"+error);
    }

    public boolean NavigateToIsometricsSignInPage() {

        if (!SeleniumDriverInstance.navigateTo(getData("URL"))) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToIsomoterics() {

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Username(), testData.getData("Username"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Password(), testData.getData("Password"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.LoginBtn())) {
            error = "Failed to click sign in button.";
            return false;
        }

        pause(10000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.userProjectXpath(), 10)) {
            error = "Failed to wait user name"
                    ;
            return false;
        }

        userName = SeleniumDriverInstance.retrieveTextByXpath(IsometricsPOCPageObjects.userProjectXpath());

        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");

        return true;

    }

    private boolean IsomotericsCheckSolutionBranch() {
        if(!testData.getData("Username").equals("BIuser")){
            if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                error = "Failed to switch to frame ";
                return false;
            }
            return true;
        }else{
            if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                error = "Failed to switch to frame ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.homepage_SolutionsBranch_Btn(), 2000)) {
                error = "Failed to wait for Solution Branch button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.homepage_SolutionsBranch_Btn())) {
                error = "Failed to click Solution Branch button.";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.homepage_SolutionBranchCode_Btn(), 2000)) {
                error = "Failed to wait for Solution Branch button.";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.homepage_SolutionBranchCode_Btn())) {
                error = "Failed to click Solution Branch button.";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.sb_text(), 2000)){
                error = "Failed to wait for Solution Branch.";
                return false;
            }
            pause(2000);
            String extractedText = SeleniumDriverInstance.retrieveTextByXpath(IsometricsPOCPageObjects.sb_text());

            narrator.stepPassedWithScreenShot("Successfully extracted Solution Branch: " + extractedText + ".");

            if (!SeleniumDriverInstance.waitForElementsByXpath(IsometricsPOCPageObjects.backtohomepage_Btn())) {
                error = "Failed to wait for Solution Branch button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.backtohomepage_Btn())) {
                error = "Failed to click Solution Branch button.";
                return false;
            }
            return true;
        }
    }
    
    public boolean IsoVersion(){
        if(!SeleniumDriverInstance.switchToDefaultContent()){
            error = "Failed to switch to side menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.isoSideMenu())){
            error = "Failed to wait for Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.isoSideMenu())){
            error = "Failed to click Side Menu.";
            return false;
        }
        
        String version = SeleniumDriverInstance.retrieveTextByXpath(IsometricsPOCPageObjects.version());
        
        narrator.stepPassedWithScreenShot("Successfully extracted IsoMetrix Version: " + version + ".");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.isoSideMenu())){
            error = "Failed to wait for Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.isoSideMenu())){
            error = "Failed to click Side Menu.";
            return false;
        }
        
        return true;
    }

}
