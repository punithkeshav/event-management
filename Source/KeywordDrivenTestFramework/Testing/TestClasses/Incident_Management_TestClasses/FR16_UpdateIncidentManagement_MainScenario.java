/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Update Incident Management Main",
        createNewBrowserInstance = false
)
public class FR16_UpdateIncidentManagement_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR16_UpdateIncidentManagement_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        

    }

    public TestResult executeTest() {
        if (!UpdateIncidentManagement()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Completed Updating Incident Management - Main Scenario");
    }

    public boolean UpdateIncidentManagement() {

        String button = getData("Submit Step 2");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.flowProcess_ButtonXpath())) {
            error = "Failed to wait incident management flow process button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.flowProcess_ButtonXpath())) {
            error = "Failed to click incident management flow process button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
            error = "Failed to wait 2.Verification And Additional Details tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification And Additional Details tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Tab_ButtonXpath())) {
            error = "Failed to wait for Risk And Impact Assessment tab button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Tab_ButtonXpath())) {
            error = "Failed to click Risk And Impact Assessment tab button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to wait for I declare that I am authorised to ... check box.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to click I declare that I am authorised to ... check box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath())) {
            error = "Failed to wait for Declaration Comment text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath(), testData.getData("DeclarationComments"))) {
            error = "Failed to enter text into Declaration Comment text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to wait for Lead inverstigator Select dropdawon list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to click for Lead inverstigator Select dropdawon list.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.anyActiveOptionDropdownXpath(getData("LeadInvestigator")))) {
            error = "Failed to wait for EnterText into Lead inverstigator type search.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.anyActiveOptionDropdownXpath(getData("LeadInvestigator")))) {
            error = "Failed to EnterText into Lead inverstigator type search.";
            return false;
        }

        if (button.equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
                error = "Failed to wait for Save and continue to Step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
                error = "Failed to click Save and continue to Step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
                error = "Failed to wait for Save dropdown button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
                error = "Failed to click Save Save dropdown button.";
                return false;
            }

        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath())) {
                error = "Failed to wait for Save and continue to Step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath())) {
                error = "Failed to click Save and continue to Step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.activeUnderInvesgationXpath(), 300000)) {
                error = "Failed to wait 3.Under Investigation phase";
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(MainScenario_PageObjects.investigationTab())) {
                error = "Failed to scroll to 3.Under Investigation tab";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTab())) {
                error = "Failed to click 3.Under Investigation tab";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTab())) {
                error = "Failed to click second 3.Under Investigation tab";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
                error = "Failed to click on the investigation details tab ";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationDueDateXPath(), date)) {
                error = "Failed to enter investigation due date";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.investigationScope(), getData("Investigation Scope"))) {
                error = "Failed to enter investigation scope";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Captured Risk and Impact Assessment.");
            narrator.stepPassed("Declaration comment: " + testData.getData("DeclarationComments"));
            narrator.stepPassed("lead investigator: " + testData.getData("LeadInvestigator"));

        }

        return true;
    }

}
