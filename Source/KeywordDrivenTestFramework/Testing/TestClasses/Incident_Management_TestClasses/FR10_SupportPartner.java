/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Do you support a partner",
        createNewBrowserInstance = false
)

public class FR10_SupportPartner extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR10_SupportPartner() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!supportPartner()) {
            return narrator.testFailed("Failed fill Do you support a partner " + error);
        }

        return narrator.finalizeTest("Completed Do you support a partner ");
    }

    public boolean supportPartner() {

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workersTabCheckOpenXpath(), 2)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workersTabXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details  ";
                return false;
            }
        }




        String support = getData("Do you support a partner");

        if (support.equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.partnerDropDownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(support))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + support;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.averageGrossXpath(), 2)) {
                error = "Failed to display injury Claim - Worker's Personal Details - What were their average gross weekly earnings over 3 months? ";
                return false;
            }
            
             if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.averageGrossXpath(), getData("average gross"))) {
                error = "Failed to enter injury Claim - Worker's Personal Details - What were their average gross weekly earnings over 3 months? ";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Do you support a partner and 1 fields are displayed.");

        } else if (support.equalsIgnoreCase("No")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.partnerDropDownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(support))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? " + support;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.averageGrossXpath(), 2)) {
                error = "Worker's Personal Details - What were their average gross weekly earnings over 3 months? fields are triggered.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Do you support a partner? and No fields are triggered.");
        }

        return true;
    }

}
