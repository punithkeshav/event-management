/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "View Related Risk Assessment",
        createNewBrowserInstance = false
)
public class FR22_View_Related_Risk_Assessment_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR22_View_Related_Risk_Assessment_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!ViewingRelatedRiskAssessment()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Viewing Related Risk Assessment - Main Scenario");
    }

    public boolean ViewingRelatedRiskAssessment() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Save_textFieldXpath())) {
            error = "Failed to wait for Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Save_textFieldXpath())) {
            error = "Failed to click Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_DueDate_textFieldXpath())) {
            error = "Failed to wait for Investigation due date text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_DueDate_textFieldXpath(), testData.getData("InvestigationDueDate"))) {
            error = "Failed to EnterText: " + testData.getData("InvestigationDueDate") + ", into Investigation due date text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to wait for Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to click Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath(), testData.getData("InvestigationScope"))) {
            error = "Failed to EnterText: " + testData.getData("InvestigationScope") + ", into Investigation Scope text field.";
            return false;
        }
        narrator.stepPassed("Investigation Due Date: " + testData.getData("InvestigationDueDate"));
        narrator.stepPassed("Investigation Scope: " + testData.getData("InvestigationScope"));

        pause(500);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationDetail_Tabxpath())) {
            error = "Failed to wait for Investigation Detail Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationDetail_Tabxpath())) {
            error = "Failed to click Investigation Detail Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationDetail_Tabxpath())) {
            error = "Failed to wait for Investigation Detail Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationDetail_Tabxpath())) {
            error = "Failed to click Investigation Detail Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ProcessOrActivity_Layoutxpath())) {
            error = "Failed to wait for Process Or Activity layout.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ProcessOrActivity_Layoutxpath())) {
            error = "Failed to click Process Or Activity layout.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ProcessOrActivity_TickAllxpath())) {
            error = "Failed to wait for Process Or Activity layout Tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ProcessOrActivity_TickAllxpath())) {
            error = "Failed to click Process Or Activity layout Tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RiskSource_Layoutxpath())) {
            error = "Failed to wait for Risk Source layout.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RiskSource_Layoutxpath())) {
            error = "Failed to click for Risk Source layout.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RiskSource_TickAllxpath())) {
            error = "Failed to wait for Risk Source layout tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RiskSource_TickAllxpath())) {
            error = "Failed to click Risk Source layout tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Risk_Layoutxpath())) {
            error = "Failed to wait for Risk layout.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_Risk_Layoutxpath())) {
            error = "Failed to click Risk layout.";
            return false;
        }
        pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Risk_TickAllxpath())) {
            error = "Failed to wait for Risk layout tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_Risk_TickAllxpath())) {
            error = "Failed to click Risk layout tick all checkbox.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationType_Layoutxpath())) {
            error = "Failed to wait for Investigation Type layout.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationType_Layoutxpath())) {
            error = "Failed to click Investigation Type layout.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationType_TickAllxpath(testData.getData("InvestigationType")))) {
            error = "Failed to wait for Investigation Type: " + testData.getData("InvestigationType");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_InvestigationType_TickAllxpath(testData.getData("InvestigationType")))) {
            error = "Failed to click Investigation Type: " + testData.getData("InvestigationType");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Refresh_Buttonxpath())) {
            error = "Failed to wait for Investigation Refresh button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_Refresh_Buttonxpath())) {
            error = "Failed to click Investigation Refresh button.";
            return false;
        }
        
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath(), 300000)) {
            error = "Failed to wait for Related Risk panel.";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
            error = "Failed to click Related Risk panel.";
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
//            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
//                error = "Failed to wait for Related Risk panel.";
//                return false;
//            }
//            pause(10000);
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
//                error = "Failed to click Related Risk panel.";
//                return false;
//            }
//        }

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Openxpath(), 300000)) {
            error = "Failed to wait for Available Related Risk.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Related Risk Panel.");

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_Openxpath())) {
            error = "Failed to click Available Related Risk.";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Risk to open.";
            return false;
        }

        String openedRelatedRisk = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedRisk_OpenedRecordxpath());
        narrator.stepPassed("Risk Assessment opened: " + openedRelatedRisk);

        return true;
    }

}
