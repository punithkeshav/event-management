/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Logging an Incident",
        createNewBrowserInstance = false
)
public class LoggingAnIncident extends BaseClass
{

    String error = "";
    String recordNumber;

    public LoggingAnIncident()
    {
        recordNumber = "";
    }

    public TestResult executeTest()
    {

        if (!loggingAnIncident())
        {
            return narrator.testFailed("Failed to logging An Incident due - " + error);
        }

        return narrator.finalizeTest("Successfully logging An Incident");
    }

    public boolean loggingAnIncident()
    {

        SikuliDriverUtility sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame ";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame ";
//        }

        if(!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.environmentalHealth())){
            error = "Failed to wait for Environmental Health Safety";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealth()))
        {
            error = "Failed to click Environmental Health Safety";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath()))
        {
            error = "Failed to wait for Incident Managment";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath()))
        {
            error = "Failed to click Incident Managment";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60))
            {
                error = " save took long - reached the time out ";
                return false;
            }
        }
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn(), 5000))
        {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.AddNewBtn()))
        {
            error = "Failed to click add button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.incidenttitleXpath()))
        {
            error = "Failed to wait for Incident title";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.incidenttitleXpath(), getData("Incident title")))
        {
            error = "Failed to enter  Incident title";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.IncidentDescription()))
        {
            error = "Failed to wait for Incident Description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.IncidentDescription(), getData("Incident Description")))
        {
            error = "Failed to enter  Incident Description";
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentOccured(), 300000))
        {
            error = "Failed to wait for Section where incident occurred dropdown";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentOccured()))
        {
            error = "Failed to click Section where incident occurred dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.clickGlobalCompanyXpath()))
        {
            error = "Failed to wait for global Company dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.clickGlobalCompanyXpath()))
        {
            error = "Failed to click global Company dropdown";
            return false;
        }

        String occured = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.occuredText())).getText();

        narrator.stepPassed("Successfully selected an item for the Section where incident occurred dropdown: " + occured);
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.specificLocationXpath()))
        {
            error = "Failed to wait for Specific Location ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.specificLocationXpath(), getData("Specific Location")))
        {
            error = "Failed to enter to Specific Location ";
            return false;
        }

        narrator.stepPassed("Successfully entered the Specific location: " + getData("Specific Location"));
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.ProjectLink()))
        {
            error = "Failed to wait for Project Link";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.ProjectLink()))
        {
            error = "Failed to click Project Link";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.Project()))
        {
            error = "Failed to wait for project dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Project()))
        {
            error = "Failed to click project dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.anyClickOptionXpath(getData("Project"))))
        {
            error = "Failed to wait for project " + getData("Project");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Project"))))
        {
            error = "Failed to click project " + getData("Project");
            return false;
        }

        narrator.stepPassed("Successfully selected Project :" + SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.projectText())).getText());

//        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectMap())) {
//            error = "Failed to click Map";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.ScrollDownWithWheel(IncidentPageObjects.mapPic(), 12)) {
//            error = "Failed to zoom in the map ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.ScrollUpWithWheel(IncidentPageObjects.africaPic(), 4)) {
//            error = "Failed to zoom in Africa ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.ScrollUpWithWheel(IncidentPageObjects.joziPic(), 6)) {
//            error = "Failed to zoom in Johannesburg ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.ScrollUpWithWheel(IncidentPageObjects.joziPic2(), 6)) {
//            error = "Failed to zoom in fourways ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.markIconPic())) {
//            error = "Failed to click mark in fourways ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.markPic())) {
//            error = "Failed to click mark in fourways ";
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Marked location ", IncidentPageObjects.mapPicXpath());
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectMap())) {
//            error = "Failed to click Map close";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.RiskDisciple()))
        {
            error = "Failed to wait for Impack type dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.RiskDisciple()))
        {
            error = "Failed to click Impack type dropdown ";
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.incidenttypeAll(), 5000))
        {
            error = "Failed to wait for selected all Impack type  ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.incidenttypeAll()))
        {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        String impactType = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.impactTypeText())).getText();
        if (impactType.equalsIgnoreCase(""))
        {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        narrator.stepPassed("Successfully selected Impact type :" + impactType);

        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.incidentTypeXpath()))
        {
            error = "Failed to wait for incident type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeXpath()))
        {
            error = "Failed to click incident type dropdown";
            return false;
        }

        pause(2000);
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsIncidentPageObjects.incedentTypeWait())){
            error = "Failed to wait for Incident type data present.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.incedentTypeWait())){
            error = "Failed to wait for Incident type data.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeSelectAllXpath()))
        {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();

        startDate = sdf.format(cal.getTime());
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.dateOcXpath(), startDate))
        {
            error = "Failed to enter  Date of occurrence";
            return false;
        }

        String time = new SimpleDateFormat("HH:mm").format(new Date());
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.timeXpath()))
        {
            error = "Failed to wait for Time of occurrence";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.timeXpath(), time))
        {
            error = "Failed to enter  Time of occurrence";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.reportedDateXpath()))
        {
            error = "Failed to wait for Reported date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedDateXpath(), startDate))
        {
            error = "Failed to enter  Reported date";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.reportedTimeXpath()))
        {
            error = "Failed to wait for Reported time";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedTimeXpath(), time))
        {
            error = "Failed to enter  Reported time";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.shiftXpath()))
        {
            error = "Failed to wait for shift dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.shiftXpath()))
        {
            error = "Failed to click shift dropdown";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.anyClickOptionXpath(getData("Shift")), 5000))
        {
            error = "Failed to wait for shift ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Shift"))))
        {
            error = "Failed to click shift ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.partInvolvedXpath()))
        {
            error = "Failed to wait for External parties involved check box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.partInvolvedXpath()))
        {
            error = "Failed to click External parties involved check box";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("External parties"))))
        {
            error = "Failed to wait for External parties ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("External parties"))))
        {
            error = "Failed to click External parties ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.ActionTaken()))
        {
            error = "Failed to wait for Immediate action taken";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.ActionTaken(), getData("Immediate action taken")))
        {
            error = "Failed to enter  Immediate action taken";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.Reported()))
        {
            error = "Failed to wait for Reported by check box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.Reported()))
        {
            error = "Failed to click Reported by check box";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.SuperVisor()))
        {
            error = "Failed to wait for Reported by dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.SuperVisor()))
        {
            error = "Failed to click Reported by dropdown";
            return false;
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.sighOffActionXpath(getData("Reported by"))))
//        {
//            error = "Failed to wait for Reported by projecte User";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Reported by"))))
//        {
//            error = "Failed to click Reported by projecte User";
//            return false;
//        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.addMoreImagesRow1())) {
//            error = "Failed to click Add more images 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.addMoreImagesRow2())) {
//            error = "Failed to click Add more images 2";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.imagePic1())) {
//            error = "Failed to click image 1";
//            return false;
//        }
//
//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//
//        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path)) {
//            error = "Failed to click image 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectIdentificationType()) {
//            error = "Failed to click Add more images ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvtPic())) {
//            error = "Failed to click dvt ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//            error = "Failed to click open ";
//            return false;
//        }
//
//        //image 2
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.image2Pic())) {
//            error = "Failed to click image 2";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvt2Pic())) {
//            error = "Failed to click dvt ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//            error = "Failed to click open ";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Add images ");
//        //image 3
//        if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.scrollXpath())) {
//            error = "Failed to scroll to Incident Owner";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.image3Pic())) {
//            error = "Failed to click image 2";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvt3Pic())) {
//            error = "Failed to click dvt ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//            error = "Failed to click open ";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.IncidentOwnerXpath()))
        {
            error = "Failed to wait for to Incident Owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.IncidentOwnerXpath()))
        {
            error = "Failed to click to Incident Owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.sighOffActionXpath(getData("Incident Owner"))))
        {
            error = "Failed to wait for Incident Owner";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Incident Owner"))))
        {
            error = "Failed to select Incident Owner";
            return false;
        }

        narrator.stepPassedWithScreenShot("Add image 3 and entered incident owner");
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveStep2()))
        {
            error = "Failed to wait for to Save and continue to Step 2";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.saveStep2()))
        {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }
        pause(2000);
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400))
            {
                error = "Website too long to load wait reached the time out";
                return false;
            }
        }

        SeleniumDriverInstance.pause(1000);
        recordNumber = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.GetIncidentRecordNumberXPath()).substring(9);
        
        narrator.stepPassed("Successfully selected incident Type  :" + SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.incidentTypeTextXpath())).getText());

        return true;
    }
}
