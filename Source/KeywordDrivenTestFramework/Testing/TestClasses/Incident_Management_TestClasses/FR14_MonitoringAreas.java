/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MonitoringAirPageObjects;
import java.util.Set;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR14 Monitoring",
        createNewBrowserInstance = false
)

public class FR14_MonitoringAreas extends BaseClass {

    String error = "";
    String monitorAreas = "";

    public FR14_MonitoringAreas() {

    }

    public TestResult executeTest() {
        if (!monitorArea()) {
            return narrator.testFailed("Failed fill Monitoring areas - " + monitorAreas + " " + error);
        }

        return narrator.finalizeTest("Succesfully open a new window of " + monitorAreas + " ");
    }

    public boolean monitorArea() {

        monitorAreas = getData("Monitor Areas");
        String addButton = "Add " + monitorAreas + " Monitoring Record";

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.EnvironmentTabXPath())) {
            error = "Failed to click on the Environment tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.monitorReqquiredCheckbox())) {
            error = "Failed to click on Monitoring required checkbox";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.monitorPanelXpath(), 2)) {
            error = "Failed to diplay on Monitoring Panel";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully displayed : Monitoring panel and Monitoring areas checkbox list ");

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaCheckbox(monitorAreas), 3)) {
            error = "Failed to display on Monitoring Areas : " + monitorAreas;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaCheckbox(monitorAreas))) {
            error = "Failed to click on Monitoring Areas : " + monitorAreas;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.monitoringAreasTapXpath(monitorAreas), 3)) {
            error = "Failed to diplays " + monitorAreas + " tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.monitoringAreasTapXpath(monitorAreas))) {
            error = "Failed to click " + monitorAreas + " tab";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(MonitoringAirPageObjects.monitoringAreasTapXpath("Monitoring - " + monitorAreas))) {
            error = "Failed to diplays Monitoring - " + monitorAreas + " viewing grid ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.monitoringAreasTapXpath("Monitoring - " + monitorAreas), 3)) {
            error = "Failed to diplays Monitoring - " + monitorAreas + " viewing grid ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully displayed Air tab and Monitoring – Air viewing grid ");

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.monitoringAreasTapXpath(addButton))) {
            error = "Failed to click " + addButton + " button";
            return false;
        }

        SeleniumDriverInstance.switchToDefaultContent();
        Set<String> set = SeleniumDriverInstance.Driver.getWindowHandles();
        String[] win = set.stream().toArray(String[]::new);
        SeleniumDriverInstance.Driver.switchTo().window(win[1]);

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame of " + monitorAreas;
            return false;
        }

//        if (monitorAreas.equalsIgnoreCase("Air")) {
//            FR14_MonitoringAreasAir areasAir = new FR14_MonitoringAreasAir();
//            return areasAir.monitorAreaAir();
//
//        }else if(monitorAreas.equalsIgnoreCase("Water")){
//            FR15_MonitoringAreasWater areasWater = new FR15_MonitoringAreasWater();
//            return areasWater.monitorAreaWater();
//         }

        return true;
    }

}
