/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Return To Work Management Alternate Scenario 6",
        createNewBrowserInstance = false
)

public class FR8_Capture_Return_To_Work_Management_AlternateScenario6 extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    
    public FR8_Capture_Return_To_Work_Management_AlternateScenario6()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }
    
    public TestResult executeTest()
    {
        if (!ReturnToWorkManagement())
        {
            return narrator.testFailed("Failed to validate that 'Management who referred the person?' is displayed and 'Link to medical test record' field is displayed - " + error);
        }
        if (!DoctorsNotesDetails())
        {
            return narrator.testFailed("Failed to enter Doctor's notes details - " + error);
        }
        if (!ValidateRecordIsSaved())
        {
            return narrator.testFailed("Failed to validate that the record is saved - " + error);
        }
        
        return narrator.finalizeTest("Successfully saved an Incident Management record");
    }
    
    public boolean ReturnToWorkManagement()
    {
        
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ReturnToWorkManagementXPath(), 500))
        {
            error = "Failed to scroll to the Return To Work Mangement heading ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkManagementAddButtonXPath()))
        {
            error = "Failed to click the Add button ";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.StatusDropdownXPath()))
        {
            error = "Failed to click the Status Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Status"))))
        {
            error = "Failed to select the Reportable to item: " + testData.getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath()))
        {
            error = "Failed to click on the dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Origin Of Case"))))
        {
            error = "Failed to click the Origin Of Case Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ExternalCaseDropdownXPath()))
        {
            error = "Failed to click the External Case Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("External Case"))))
        {
            error = "Failed to select the following item item: " + getData("External Case");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.ManagementWhoReferredThePersonLabelXPath()))
        {
            error = "Failed to validate that 'Management who referred the person' field is displayed";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ManagementWhoReferredThePersonDropdownXPath()))
        {
            error = "Failed to click 'Management who referred the person' dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Management"))))
        {
            error = "Failed to select element from dropdown - " + getData("Management");
            return false;
        }
        narrator.stepPassed("'Management who referred the person?' is displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.LinkToMedicalTestRecordFieldXPath()))
        {
            error = "Failed to validate that the Link Case To Medical Test Record field is displayed";
            return false;
        }
        narrator.stepPassed("'Link to medical test record' field is displayed");
        narrator.stepPassedWithScreenShot("Successfully selected " + getData("Origin Of Case") + " from the Origin Of Case dropdown");
        return true;
    }
    
    public boolean DoctorsNotesDetails()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.MedicalPractionerDropdownXPath()))
        {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Medical Practionar"))))
        {
            error = "Failed to select the following item: " + getData("Medical Practionar");
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DoctorsNotesTextAreaXPath(), getData("Doctors Notes")))
        {
            error = "Failed to fill in the Doctor's Notes text area";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateIssuedXPath(), date))
        {
            error = "Failed to enter text into the Date Issued text field";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FitnessForWorkDropdownXPath()))
        {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Fitness"))))
        {
            error = "Failed to select the following item: " + getData("Fitness");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateFromXPath(), date))
        {
            error = "Failed to enter text into the Date From text field ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(MainScenario_PageObjects.RestrictionsTextAreaHiddenXPath()))
        {
            error = "Failed to wait for the Restrictions text area";
            return false;
        }
        narrator.stepPassed("'Restrictions' field is not displayed");
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.DateToHiddenXPath()))
        {
            error = "Failed to wait for the Date To text field";
            return false;
        }
        narrator.stepPassed("'Date to' field is not displayed");
       
        narrator.stepPassedWithScreenShot("Successfully validated that the Restrictions field are not displayed and that the Date To field are not displayed");
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ChronicIllnessPanelXPath()))
        {
            error = "Failed to move to the 'Chronic Illness' panel ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.ChronicIllnessPanelXPath()))
        {
            error = "Failed to validate that 'Chronic Illness' panel is displayed";
            return false;
        }
        narrator.stepPassed("'Chronic Illness' panel is displayed");
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveFormButtonXPath()))
        {
            error = "Failed to click on the Save button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.CloseForm2ButtonXPath()))
        {
            error = "Failed to click on the close form Button ";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        return true;
    }
    
    public boolean ValidateRecordIsSaved()
    {
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
        {
            error = "Failed to wait for the Verification And Additional Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
        {
            error = "Failed to click on the Verification And Additional Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WaitForItemXPath(getData("Full Name"))))
        {
            error = "Failed to verify that the Incident Management record was added";
            return false;
        }
        narrator.stepPassedWithScreenShot("Record is saved - " + getData("Full Name"));
        return true;
    }
    
}
